-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 10, 2016 at 01:49 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agasidbase2015`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE IF NOT EXISTS `agenda` (
  `id_agenda` int(11) NOT NULL AUTO_INCREMENT,
  `id_klasifikasi` int(11) NOT NULL,
  `inc_agenda` int(11) NOT NULL,
  `no_agenda` varchar(50) NOT NULL,
  `no_srt_masuk` varchar(200) NOT NULL,
  `tgl_penerimaan` datetime NOT NULL,
  `tgl_srt_masuk` date NOT NULL,
  `hal_surat` text NOT NULL,
  `tujuan` varchar(200) NOT NULL,
  `nama_pengirim` varchar(200) NOT NULL,
  `alamat_pengirim` text NOT NULL,
  `isi_ringkas` varchar(200) NOT NULL,
  `tembusan` varchar(200) DEFAULT NULL,
  `salinan` text NOT NULL,
  `keamanan` varchar(200) NOT NULL,
  `lampiran` varchar(200) DEFAULT NULL,
  `keterangan` text,
  `filename` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_agenda`),
  KEY `id_klasifikasi` (`id_klasifikasi`),
  KEY `id_klasifikasi_2` (`id_klasifikasi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `disposisi`
--

CREATE TABLE IF NOT EXISTS `disposisi` (
  `id_disposisi` int(11) NOT NULL AUTO_INCREMENT,
  `id_agenda` int(11) NOT NULL,
  `tujuan_utama` int(11) NOT NULL,
  `tanggapan_utama` text NOT NULL,
  `tujuan_pansek` int(11) NOT NULL,
  `tanggapan_pansek` text NOT NULL,
  `tujuan_wapansek` int(11) NOT NULL,
  `tanggapan_wapansek` text NOT NULL,
  `tujuan_panmud` int(11) NOT NULL,
  `tanggapan_panmud` text NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_utama` datetime NOT NULL,
  `tgl_pansek` datetime NOT NULL,
  `tgl_wapansek` datetime NOT NULL,
  `tgl_panmud` datetime NOT NULL,
  `tgl_staff` datetime NOT NULL,
  `note` text NOT NULL,
  PRIMARY KEY (`id_disposisi`),
  KEY `id_agenda` (`id_agenda`,`tujuan_utama`,`tujuan_pansek`,`tujuan_panmud`,`id_pegawai`),
  KEY `id_agenda_2` (`id_agenda`),
  KEY `id_pegawai` (`id_pegawai`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_jabatan`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `jabatan`
--

INSERT INTO jabatan
  (id_jabatan, jabatan)
VALUES
  (1, 'Ketua'),
  (2, 'Wakil Ketua'),
  (3, 'Panitera'),
  (4, 'Sekretaris'),
  (5, 'Wakil Panitera'),
  (6, 'Kepala Bagian Umum'),
  (7, 'Panitera Muda Pidana'),
  (8, 'Panitera Muda Perdata'),
  (9, 'Panitera Muda Hukum'),
  (10, 'Panitera Muda Tipikor'),
  (11, 'Panitera Muda Niaga'),
  (12, 'Panitera Muda PHI'),
  (13, 'Kasub Bagian Umum dan Keuangan'),
  (14, 'Kasub Bagian Kepegawaian, Organisasi dan Tata Laksana'),
  (15, 'Kasub Bagian Teknologi Informasi, Perencanaan, dan Pelaporan'),
  (16, 'Koordinator Delegasi'),
  (17, 'Staff Pidana'),
  (18, 'Staff Perdata'),
  (19, 'Staff Hukum'),
  (20, 'Staff Tipikor'),
  (21, 'Staff Niaga'),
  (22, 'Staff Bagian Umum dan Keuangan'),
  (23, 'Staff PHI'),
  (24, 'Staff Bagian Kepegawaian, Organisasi, dan Tata Laksana'),
  (25, 'Staff Bagian Teknologi Informasi, Perencanaan, dan Pelaporan'),
  (26, 'Staff Bagian Berkas Perkara'),
  (27, 'Staff Delegasi'),
  (99, 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `klasifikasi`
--

CREATE TABLE IF NOT EXISTS `klasifikasi` (
  `id_klasifikasi` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  PRIMARY KEY (`id_klasifikasi`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `klasifikasi`
--

INSERT INTO `klasifikasi` (`id_klasifikasi`, `kode`, `jenis`) VALUES
(1, 'Del.Pid', 'Delegasi Pidana'),
(2, 'Del.Pdt', 'Delegasi Perdata'),
(3, 'PMB', 'Permintaan Biaya'),
(4, 'BB', 'Berkas Perkara'),
(5, 'A', 'Umum');

-- --------------------------------------------------------

--
-- Table structure for table `koordinasi`
--

CREATE TABLE IF NOT EXISTS `koordinasi` (
  `id_koordinasi` int(11) NOT NULL AUTO_INCREMENT,
  `kepala` int(11) NOT NULL,
  `staff` int(11) NOT NULL,
  PRIMARY KEY (`id_koordinasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `koordinasi`
--

INSERT INTO koordinasi
  (kepala, staff)
VALUES
  (7, 17),
  (7, 27),
  (8, 18),
  (8, 27),
  (9, 19),
  (9, 27),
  (10, 20),
  (10, 27),
  (11, 21),
  (11, 27),
  (12, 23),
  (12, 27),
  (13, 22),
  (13, 27),
  (14, 27),
  (15, 25),
  (15, 27),
  (16, 17),
  (16, 18),
  (16, 19),
  (16, 20),
  (16, 21),
  (16, 22),
  (16, 23),
  (16, 24),
  (16, 25),
  (16, 26),
  (16, 27);

-- --------------------------------------------------------

--
-- Table structure for table `log_admin`
--

CREATE TABLE IF NOT EXISTS `log_admin` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `ket` text NOT NULL,
  `exetime` datetime NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `id_jabatan` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `vpassword` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telepon` varchar(100) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=suspend;1=aktif',
  `path` text NOT NULL,
  PRIMARY KEY (`id_pegawai`),
  KEY `jabatan` (`id_jabatan`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `id_jabatan`, `nama`, `username`, `password`, `vpassword`, `email`, `telepon`, `status`, `path`) VALUES
(1, 99, 'Administrator', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 'agasiadmin@gmail.com', '08123456789', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `surat_keluar`
--

CREATE TABLE IF NOT EXISTS `surat_keluar` (
  `id_srt_keluar` int(11) NOT NULL AUTO_INCREMENT,
  `id_agenda` int(11) NOT NULL,
  `no_srt_keluar` varchar(100) NOT NULL,
  `id_klasifikasi` int(11) NOT NULL,
  `tgl_srt_keluar` date NOT NULL,
  `perihal` varchar(200) NOT NULL,
  `tujuan` varchar(200) NOT NULL,
  `lampiran` varchar(200) NOT NULL,
  `ket_srt` text NOT NULL,
  `filename` varchar(100) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_srt_keluar`),
  KEY `id_agenda` (`id_agenda`,`id_klasifikasi`,`id_pegawai`),
  KEY `id_klasifikasi` (`id_klasifikasi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trash_agenda`
--

CREATE TABLE IF NOT EXISTS `trash_agenda` (
  `id_agenda` int(11) NOT NULL DEFAULT '0',
  `id_klasifikasi` int(11) NOT NULL,
  `inc_agenda` int(11) NOT NULL,
  `no_agenda` varchar(50) CHARACTER SET latin1 NOT NULL,
  `no_srt_masuk` varchar(200) CHARACTER SET latin1 NOT NULL,
  `tgl_penerimaan` datetime NOT NULL,
  `tgl_srt_masuk` date NOT NULL,
  `hal_surat` text CHARACTER SET latin1 NOT NULL,
  `tujuan` varchar(200) CHARACTER SET latin1 NOT NULL,
  `nama_pengirim` varchar(200) CHARACTER SET latin1 NOT NULL,
  `alamat_pengirim` text CHARACTER SET latin1 NOT NULL,
  `isi_ringkas` varchar(200) CHARACTER SET latin1 NOT NULL,
  `tembusan` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `salinan` text CHARACTER SET latin1 NOT NULL,
  `keamanan` varchar(200) CHARACTER SET latin1 NOT NULL,
  `lampiran` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1,
  `filename` varchar(200) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trash_disposisi`
--

CREATE TABLE IF NOT EXISTS `trash_disposisi` (
  `id_disposisi` int(11) NOT NULL DEFAULT '0',
  `id_agenda` int(11) NOT NULL,
  `tujuan_utama` int(11) NOT NULL,
  `tanggapan_utama` text CHARACTER SET latin1 NOT NULL,
  `tujuan_pansek` int(11) NOT NULL,
  `tanggapan_pansek` text CHARACTER SET latin1 NOT NULL,
  `tujuan_wapansek` int(11) NOT NULL,
  `tanggapan_wapansek` text CHARACTER SET latin1 NOT NULL,
  `tujuan_panmud` int(11) NOT NULL,
  `tanggapan_panmud` text CHARACTER SET latin1 NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_utama` datetime NOT NULL,
  `tgl_pansek` datetime NOT NULL,
  `tgl_wapansek` datetime NOT NULL,
  `tgl_panmud` datetime NOT NULL,
  `tgl_staff` datetime NOT NULL,
  `note` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trash_keluar`
--

CREATE TABLE IF NOT EXISTS `trash_keluar` (
  `id_srt_keluar` int(11) NOT NULL DEFAULT '0',
  `id_agenda` int(11) NOT NULL,
  `no_srt_keluar` varchar(100) CHARACTER SET latin1 NOT NULL,
  `id_klasifikasi` int(11) NOT NULL,
  `tgl_srt_keluar` date NOT NULL,
  `perihal` varchar(200) CHARACTER SET latin1 NOT NULL,
  `tujuan` varchar(200) CHARACTER SET latin1 NOT NULL,
  `lampiran` varchar(200) CHARACTER SET latin1 NOT NULL,
  `ket_srt` text CHARACTER SET latin1 NOT NULL,
  `filename` varchar(100) CHARACTER SET latin1 NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
