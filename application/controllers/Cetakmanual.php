<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cetakmanual extends CI_Controller {
    
     public function index(){
         $this->load->library('m_pdf');
          $data['the_content']='mPDF and CodeIgniter are cool!';
          $html=$this->load->view('cetak_disp_manual', $data, true); 
          $pdfFilePath = "namafilepdf.pdf";
          $pdf = $this->m_pdf->load();
          $pdf->WriteHTML($html);
          $pdf->Output($pdfFilePath, "I");
     }
 
}