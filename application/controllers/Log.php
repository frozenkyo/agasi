<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {

        public function __construct() {
            parent::__construct();
            $this->load->helper('tgl_indonesia');
            $this->load->model('logsess');
            $this->load->model('masuksess');
            $this->load->model('dashboardsess');
            $this->load->library('session');
        }

	public function index()
	{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $hakakses = $session_data['privilege'];
                $idpegawai= $session_data['id_pegawai'];
                if(($hakakses=="1")||($hakakses=="2")){ //ketua wakil
                    $data['disposisi']= $this->logsess->cekDisposisiKetua($hakakses);
                }else if($hakakses=="3" || $hakakses=="4"){ //pansek
                    $data['disposisi']= $this->logsess->cekDisposisiPansek($hakakses);
                }else if($hakakses=="5" || $hakakses=="6"){ //wakil
                    $data['disposisi']= $this->logsess->cekDisposisiWaPansek($hakakses);
                }else if(($hakakses>="7")&&($hakakses<="16")){ //panmud kabag koor
                    $data['disposisi']= $this->logsess->cekDisposisiPanmud($hakakses);
                }else if(($hakakses>="17")&&($hakakses<="35")){ //staff
                    $data['disposisi']= $this->logsess->cekDisposisiStaff($idpegawai);
                }
                $data['menu']="log";
                $data['submenu']="";
                $this->load->view('element/header',$data);
                $this->load->view('logdisposisi',$data);
                $this->load->view('element/footer');
            }else{
                redirect(base_url().'login/out');
            }
	}
}
