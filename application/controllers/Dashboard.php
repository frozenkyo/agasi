<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

        public function __construct() {
            parent::__construct();
            $this->load->helper('tgl_indonesia');
            $this->load->model('dashboardsess');
            $this->load->library('session');
        }

	public function index()
	{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['jabatan'] = $session_data['jabatan'];
                $data['pegawai'] = $session_data['id_pegawai'];
                $pegawai = $session_data['id_pegawai'];
                $hakakses = $session_data['privilege'];
                $data['masuk']=$this->dashboardsess->jmlMasuk();
                $data['keluar']=$this->dashboardsess->jmlKeluar();
                $data['selesai']=$this->dashboardsess->jmlSelesai();
                $data['surat']=$this->dashboardsess->jmlMasuk()+$this->dashboardsess->jmlKeluar();
                $data['message'] = $this->session->flashdata('status');
//                echo $hakakses;
                if($hakakses=="22"){ //umum
//                    echo $hakakses;
                    if($this->input->post('submit')){
                        $id_disposisi = $this->input->post('disposisi',TRUE);
                        $tes=$this->dashboardsess->updateReff($id_disposisi);
                        if($tes){
                            $this->session->set_flashdata('status', 'success');
                            redirect(base_url().'dashboard');
                        }else{
                            $this->session->set_flashdata('status', 'error');
                            redirect(base_url().'dashboard');
                        }
                    }else{
                        $data['disposisi']= $this->dashboardsess->cekDisposisi2($pegawai);
                        $data['menu']="dashboard";
                        $data['submenu']="";
                        $this->load->view('element/header',$data);
                        $this->load->view('main',$data);
                        $this->load->view('element/footer');
                    }
                }else if(($hakakses=="1")||($hakakses=="2")){ //ketua wakil
                    $data['disposisi']= $this->dashboardsess->cekDisposisiKetua($hakakses);
                    $data['menu']="dashboard";
                    $data['submenu']="";
                    $this->load->view('element/header',$data);
                    $this->load->view('main_kw',$data);
                    $this->load->view('element/footer');
                }else if($hakakses=="3" || $hakakses=="4"){ //pansek
                    $data['disposisi']= $this->dashboardsess->cekDisposisiPansek($hakakses);
                    $data['menu']="dashboard";
                    $data['submenu']="";
                    $this->load->view('element/header',$data);
                    $this->load->view('main_kw',$data);
                    $this->load->view('element/footer');
                }else if($hakakses=="5"||$hakakses=="6"){ // wakil
                    $data['disposisi']= $this->dashboardsess->cekDisposisiWaPansek($hakakses);
                    $data['menu']="dashboard";
                    $data['submenu']="";
                    $this->load->view('element/header',$data);
                    $this->load->view('main_kw',$data);
                    $this->load->view('element/footer');
                }else if(($hakakses>="7")&&($hakakses<="16")){ //panmud kabag koor
                    $data['disposisi']= $this->dashboardsess->cekDisposisiPanmud($hakakses);
                    $data['menu']="dashboard";
                    $data['submenu']="";
                    $this->load->view('element/header',$data);
                    $this->load->view('main_kw',$data);
                    $this->load->view('element/footer');
                }else if((($hakakses>="17")&&($hakakses<="21"))||($hakakses>="23" && $hakakses<="35")){ //staff
                    if($this->input->post('submit')){
                        $id_disposisi = $this->input->post('disposisi',TRUE);
                        $tes=$this->dashboardsess->updateReffStaff($id_disposisi);
                        if($tes){
                            $this->session->set_flashdata('status', 'success');
                            redirect(base_url().'dashboard');
                        }else{
                            $this->session->set_flashdata('status', 'error');
                            redirect(base_url().'dashboard');
                        }
                    }else{
                        $data['disposisi']= $this->dashboardsess->cekDisposisi2($pegawai);
                        $data['menu']="dashboard";
                        $data['submenu']="";
                        // var_dump($data);
                        $this->load->view('element/header',$data);
                        $this->load->view('main',$data);
                        $this->load->view('element/footer');
                    }
                }else if($hakakses=="99"){
                    $data['menu']="dashboard";
                    $data['submenu']="";
                    $this->load->view('element/header',$data);
                    $this->load->view('admin/main',$data);
                    $this->load->view('element/footer');
                }
            }else{
                redirect(base_url().'login/out');
            }
	}

    public function direct()
	{
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $hakakses = $session_data['privilege'];
            $data['hakakses'] = $session_data['privilege'];
            $data['message'] = $this->session->flashdata('status');
            if($hakakses=="22"){ //umum
                redirect(base_url().'dashboard');
            }else if(($hakakses=="1")||($hakakses=="2")){ //ketua wakil
                if($this->input->post('submit')){
                    $id_disposisi = $this->input->post('referensi',TRUE);
                    $tanggapan = $this->input->post('tanggapan',TRUE);
                    $arahan = $this->input->post('tujuandisp',TRUE);
                    if($arahan=="3" || $arahan == "4"){
                        $tes=$this->dashboardsess->disposisiKePansek($id_disposisi,$tanggapan,$arahan);
                    }else if($arahan=="5" || $arahan=="6"){
                        $tes=$this->dashboardsess->disposisiKeWaPansek($id_disposisi,$tanggapan,$arahan);
                    }else if($arahan>="7"){
                        $tes=$this->dashboardsess->disposisiKePanmud($id_disposisi,$tanggapan,$arahan);
                    }
                    if($tes){
                        $this->session->set_flashdata('status', 'success');
                        redirect(base_url().'dashboard');
                    }else{
                        $this->session->set_flashdata('status', 'error');
                        redirect(base_url().'dashboard');
                    }
                }else{
                    if ($this->uri->segment(3) === NULL){
                    redirect(base_url().'dashboard');
                    }else{
                        $data['referensi']=$this->uri->segment(3);
                        $idagenda=$this->dashboardsess->disposisiToAgenda($this->uri->segment(3));
                        $data['disposisi']= $this->dashboardsess->viewAgenda($idagenda[0]->id_agenda);
                        $data['menu']="dashboard";
                        $data['submenu']="";
                        $this->load->view('element/header',$data);
                        $this->load->view('directing',$data);
                        $this->load->view('element/footer');
                    }
                }
            }else if($hakakses=="3" || $hakakses=="4"){ //pansek
                if($this->input->post('submit')){
                    $id_disposisi = $this->input->post('referensi',TRUE);
                    $tanggapan = $this->input->post('tanggapan',TRUE);
                    $arahan = $this->input->post('tujuandisp',TRUE);
                    if($hakakses=="5" || $arahan=="6"){
                        $tes=$this->dashboardsess->disposisiPansekKeWapansek($id_disposisi,$tanggapan,$arahan);
                    }else if($arahan>="7" && $arahan<="16"){
                        $tes=$this->dashboardsess->disposisiPansekKePanmud($id_disposisi,$tanggapan,$arahan);
                    }else{
//                            echo "ok";
                        $newArahan=  str_replace("A", "", $arahan);
                        $tes=$this->dashboardsess->disposisiPansekKeStaff($id_disposisi,$tanggapan,$newArahan);
                    }
                    if($tes){
                        $this->session->set_flashdata('status', 'success');
                        redirect(base_url().'dashboard');
                    }else{
                        $this->session->set_flashdata('status', 'error');
                        redirect(base_url().'dashboard');
                    }
                }else{
                    if ($this->uri->segment(3) === NULL){
                    redirect(base_url().'dashboard');
                    }else{
                        $data['referensi']=$this->uri->segment(3);
                        $idagenda=$this->dashboardsess->disposisiToAgenda($this->uri->segment(3));
                        $data['disposisi']= $this->dashboardsess->viewAgenda($idagenda[0]->id_agenda);
                        $data['staff']= $this->dashboardsess->listStaff();
                        $data['menu']="dashboard";
                        $data['submenu']="";
                        $this->load->view('element/header',$data);
                        $this->load->view('directing',$data);
                        $this->load->view('element/footer');
                    }
                }
            }else if($hakakses=="5" || $hakakses=="6"){ //wakil pansek
                if($this->input->post('submit')){
                    $id_disposisi = $this->input->post('referensi',TRUE);
                    $tanggapan = $this->input->post('tanggapan',TRUE);
                    $arahan = $this->input->post('tujuandisp',TRUE);
                    if(($arahan>="7")&&($arahan<="16")){
                        $tes=$this->dashboardsess->disposisiWaPansekKePanmud($id_disposisi,$tanggapan,$arahan);
                    }else{
//                            echo "ok";
                        $newArahan=  str_replace("A", "", $arahan);
                        $tes=$this->dashboardsess->disposisiWaPansekKeStaff($id_disposisi,$tanggapan,$newArahan);
                    }
                    if($tes){
                        $this->session->set_flashdata('status', 'success');
                        redirect(base_url().'dashboard');
                    }else{
                        $this->session->set_flashdata('status', 'error');
                        redirect(base_url().'dashboard');
                    }
                }else{
                    if ($this->uri->segment(3) === NULL){
                    redirect(base_url().'dashboard');
                    }else{
                        $data['referensi']=$this->uri->segment(3);
                        $idagenda=$this->dashboardsess->disposisiToAgenda($this->uri->segment(3));
                        $data['disposisi']= $this->dashboardsess->viewAgenda($idagenda[0]->id_agenda);
                        $data['staff']= $this->dashboardsess->listStaff();
                        $data['menu']="dashboard";
                        $data['submenu']="";
                        $this->load->view('element/header',$data);
                        $this->load->view('directing',$data);
                        $this->load->view('element/footer');
                    }
                }
            }else if(($hakakses>="7")&&($hakakses<="16")){ //panmud kabag koor
                if($this->input->post('submit')){
                    $id_disposisi = $this->input->post('referensi',TRUE);
                    $tanggapan = $this->input->post('tanggapan',TRUE);
                    $arahan = $this->input->post('tujuandisp',TRUE);
                    $tes=$this->dashboardsess->disposisiPanmudKeStaff($id_disposisi,$tanggapan,$arahan);
                    if($tes){
                        $this->session->set_flashdata('status', 'success');
                        redirect(base_url().'dashboard');
                    }else{
                        $this->session->set_flashdata('status', 'error');
                        redirect(base_url().'dashboard');
                    }
                }else{
                    if ($this->uri->segment(3) === NULL){
                    redirect(base_url().'dashboard');
                    }else{
                        $data['referensi']=$this->uri->segment(3);
                        $idagenda=$this->dashboardsess->disposisiToAgenda($this->uri->segment(3));
                        $data['disposisi']= $this->dashboardsess->viewAgenda($idagenda[0]->id_agenda);
                        $staff = $this->dashboardsess->cekStaff($hakakses);
                        if($hakakses=="16"){
                            $data['staff']= $this->dashboardsess->listStaff();
                        }else{
                            $data['staff']= $this->dashboardsess->listStaffKhusus($staff[0]->staff);
                        }
                        $data['menu']="dashboard";
                        $data['submenu']="";
                        $this->load->view('element/header',$data);
                        $this->load->view('directing',$data);
                        $this->load->view('element/footer');
                    }
                }
            }else if((($hakakses>="17") && ($hakakses<="21")) || ($hakakses>="23" && $hakakses<="35")){ //staff
                redirect(base_url().'dashboard');
            }
        }else{
            redirect(base_url().'login/out');
        }
	}

        public function review(){
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['hakakses'] = $session_data['privilege'];
                if($this->input->post('submit')){
                    $id_disposisi = $this->input->post('disposisi',TRUE);
                    $status= $this->input->post('status',TRUE);
                        $tes=$this->dashboardsess->updateReffSelesai($id_disposisi);
                    if($tes){
                        $this->session->set_flashdata('status', 'success');
                        redirect(base_url().'dashboard');
                    }else{
                        $this->session->set_flashdata('status', 'error');
                        redirect(base_url().'dashboard');
                    }
                }else{
                    $data['menu']="dashboard";
                    $data['submenu']="";
                    if ($this->uri->segment(3) === NULL){
                        redirect(base_url().'dashboard');
                    }else{
                        $data['referensi']=$this->uri->segment(3);
                        $idagenda=$this->dashboardsess->disposisiToAgenda($this->uri->segment(3));
                        $data['disp']= $this->dashboardsess->viewDisposisi($this->uri->segment(3));
                        $data['disposisi']= $this->dashboardsess->viewAgenda($idagenda[0]->id_agenda);
                    }
                    $this->load->view('element/header',$data);
                    $this->load->view('reviewsurat',$data);
                    $this->load->view('element/footer');
                }
            }else{
                redirect(base_url().'login/out');
            }
        }

        public function reject(){
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                if($this->input->post('submit')){
                    $id_disposisi = $this->input->post('disposisi',TRUE);
                    $note= $this->input->post('note',TRUE);
                    $tes=$this->dashboardsess->updateReffPending($id_disposisi,$note);
                    if($tes){
                        $this->session->set_flashdata('status', 'success');
                        redirect(base_url().'dashboard');
                    }else{
                        $this->session->set_flashdata('status', 'error');
                        redirect(base_url().'dashboard');
                    }
                }else{
                    redirect(base_url().'login/out');
                }
            }else{
                redirect(base_url().'login/out');
            }
        }
}
