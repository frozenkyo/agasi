<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluar extends CI_Controller {

        public function __construct() {
            parent::__construct();

            $this->load->model('keluarsess');
            $this->load->library('form_validation');
            $this->load->library('session');
            $this->load->helper('tgl_indonesia');
        }

	public function index()
	{
            redirect('keluar/maillist');
	}

        public function maillist(){
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $hakakses = $session_data['privilege'];
                $data['hakakses'] = $session_data['privilege'];
//                if(($hakakses>="15")&&($hakakses<="21")){
                    $data['menu']="keluar";
                    $data['submenu']="klist";
                    $data['content']= $this->keluarsess->daftarSurat();
                    $data['message'] = $this->session->flashdata('status');
                    $this->load->view('element/header',$data);
                    if(($hakakses=="22")||($hakakses=="99")){
                        $this->load->view('keluarlist',$data);
                    }else{
                        $this->load->view('keluarlistviewonly',$data);
                    }
                    $this->load->view('element/footer');
//                }else{
//                    redirect('/login/out');
//                }
            }else{
                redirect('/login/out');
            }
        }

        public function newmail(){
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
//                echo $session_data['id_pegawai'];
                $hakakses = $session_data['privilege'];
                if(($hakakses>="17")&&($hakakses<="35")){
                    if ($this->uri->segment(3) === NULL){
                        $data['referensi']="";
                    }else{
                        $data['referensi']=$this->uri->segment(3);
                        $data['datareff']=$this->keluarsess->daftarSuratMasukReff($this->uri->segment(3));
                    }
                    $date = DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d H:i:s"));
                    $yearNow=$date->format("Y");
                    $cekNoAgenda= $this->keluarsess->cekNoAgenda($yearNow);
                    $data['next']=($cekNoAgenda[0]->id_srt_keluar+1);
//                    $data['klasifikasi']=  $this->keluarsess->daftarKlasifikasi();
                    $data['suratmasuk']=  $this->keluarsess->daftarSuratMasuk();
                    $data['menu']="keluar";
                    $data['submenu']="kwrite";
                    if($this->input->post('submit')){
//                        $this->form_validation->set_rules('klasifikasi', 'Klasifikasi', 'trim');
                        $this->form_validation->set_rules('tgl_surat', 'Tanggal Surat', 'trim');
                        $this->form_validation->set_rules('surat1', 'Surat 1', 'trim');
                        $this->form_validation->set_rules('surat2', 'Surat 2', 'trim');
                        $this->form_validation->set_rules('surat3', 'Surat 3', 'trim');
                        $this->form_validation->set_rules('surat4', 'Surat 4', 'trim');
                        $this->form_validation->set_rules('perihal', 'Perihal', 'trim');
                        $this->form_validation->set_rules('tujuan', 'Tujuan', 'trim');
                        $this->form_validation->set_rules('lampiran', 'Lampiran', 'trim');
                        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');
                        $this->form_validation->set_rules('referensi', 'Referensi', 'trim');


                        if ($this->form_validation->run() == FALSE) {
                            $this->session->set_flashdata('status', 'fail');
                            redirect(base_url().'keluar/newmail');
                        }else{
//                            $klasifikasi = $this->input->post('klasifikasi',TRUE);
                            $tgl_surat = $this->input->post('tgl_surat',TRUE);
                            $no1 = $this->input->post('surat1',TRUE);
                            $no2 = $this->input->post('surat2',TRUE);
                            $no3 = $this->input->post('surat3',TRUE);
                            $no4 = $this->input->post('surat4',TRUE);
                            $agenda = $this->input->post('agenda',TRUE);
                            $perihal = $this->input->post('perihal',TRUE);
                            $tujuan = $this->input->post('tujuan',TRUE);
                            $lampiran = $this->input->post('lampiran',TRUE);
                            $keterangan = $this->input->post('keterangan',TRUE);
                            $referensi = $this->input->post('referensi',TRUE);
                            $reffmasuk = $this->input->post('reff',TRUE);

//                            echo $reffmasuk;

                            $ceksurat=  $this->keluarsess->cekSurat($tgl_surat,$perihal);
                            if($ceksurat){
                                $this->session->set_flashdata('status', 'duplicate');
                                redirect(base_url().'keluar/newmail');
                            }else{
                                $id_pegawai = $session_data['id_pegawai'];
                                $no_surat=$no1."/".$agenda."/".$no2."/".$no3."/".$no4;
                                $newmail= $this->keluarsess->newMail($tgl_surat,$no_surat,$perihal,$tujuan,$lampiran,$keterangan,$referensi,$id_pegawai);
                                if($newmail){
                                    if($reffmasuk!=""){
                                        $this->keluarsess->updateReff($reffmasuk);
                                    }
                                    $config = array(
                                    'upload_path' => "./outgoing/",
                                    'allowed_types' => "pdf",
                                    'overwrite' => TRUE,
                                    'max_size' => "0"
                                    );
                                    $this->load->library('upload', $config);
                                    if($this->upload->do_upload("fileupload")){
                                        $data_upload = $this->upload->data();
                                        $file_name = $data_upload["file_name"];
                                        rename("./outgoing/".$file_name, "./outgoing/".$tgl_surat."-".md5($perihal).$data_upload['file_ext']);
                                        $newPath="outgoing/".$tgl_surat."-".md5($perihal).$data_upload['file_ext'];
                                        $this->keluarsess->updateUpload($newmail,$newPath);
                                        $this->session->set_flashdata('status', 'success');
                                        redirect(base_url().'keluar/newmail');
                                    }else{
//                                        echo $this->upload->display_errors();
                                        $this->session->set_flashdata('status', 'errorupload');
                                        redirect(base_url().'keluar/newmail');
                                    }
                                }else{
                                    $this->session->set_flashdata('status', 'error');
                                    redirect(base_url().'keluar/newmail');
                                }
                            }
                        }
                    }else{
                        $data['message'] = $this->session->flashdata('status');
                        $this->load->view('element/header',$data);
                        $this->load->view('newkeluar',$data);
                        $this->load->view('element/footer');
                    }
                }else{
                    redirect(base_url().'login/out');
                }
            }else{
                redirect(base_url().'login/out');
            }
        }

        public function reupload(){
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['menu']="keluar";
                $data['submenu']="klist";
                if($this->input->post('submit')){
                    $tgl_surat = $this->input->post('tgl_surat',TRUE);
                    $perihal = $this->input->post('perihal',TRUE);
                    $newmail = $this->input->post('id_srt_keluar',TRUE);

                    $config = array(
                    'upload_path' => "./outgoing/",
                    'allowed_types' => "pdf",
                    'overwrite' => TRUE,
                    'max_size' => "0"
                    );
                    $this->load->library('upload', $config);
                    if($this->upload->do_upload("fileupload")){
                        $data_upload = $this->upload->data();
                        $file_name = $data_upload["file_name"];
                        rename("./outgoing/".$file_name, "./outgoing/".$tgl_surat."-".md5($perihal).$data_upload['file_ext']);
                        $newPath="outgoing/".$tgl_surat."-".md5($perihal).$data_upload['file_ext'];
                        $this->keluarsess->updateUpload($newmail,$newPath);
                        $this->session->set_flashdata('status', 'success');
                        redirect(base_url().'keluar/maillist');
                    }else{
//                        echo $this->upload->display_errors();
                        $this->session->set_flashdata('status', 'errorupload');
                        redirect(base_url().'keluar/maillist');
                    }
                }else{
                    if ($this->uri->segment(3) === NULL){
                        $data['content']="";
                    }else{
                        $data['content'] = $this->keluarsess->dataSurat($this->uri->segment(3));
                    }
                    $data['message'] = $this->session->flashdata('status');
                    $this->load->view('element/header',$data);
                    $this->load->view('reupload',$data);
                    $this->load->view('element/footer');
                }
            }else{
                redirect(base_url('login/out'));
            }
        }

        public function delkeluar()
	{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                if($this->input->post('submit')){
                    $id_srt_keluar=  $this->input->post('id_srt_keluar');

                    $delete=  $this->keluarsess->copyKeluar($id_srt_keluar);
                    if($delete){
                        $this->keluarsess->delKeluar($id_srt_keluar);

                        $message="Admin telah menghapus Surat Keluar nomor ".$id_srt_keluar;
                        $this->keluarsess->logAdmin($message);

                        $this->session->set_flashdata('status', 'hapus');
                        redirect(base_url('keluar/maillist'));
                    }else{
                        $this->session->set_flashdata('status', 'error');
                        redirect(base_url('keluar/maillist'));
                    }
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect(base_url('keluar/maillist'));
                }
            }else{
                redirect(base_url('admin/out'));
            }
	}
}
