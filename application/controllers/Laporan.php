<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

        public function __construct() {
            parent::__construct();
            $this->load->helper('tgl_indonesia');
            $this->load->model('logsess');
            $this->load->model('masuksess');
            $this->load->model('dashboardsess');
            $this->load->model('laporansess');
            $this->load->model('klasifikasi');
            $this->load->library('session');
        }

	public function index()
	{
            redirect('/laporan/klasifikasi');
	}

    /*public function klasifikasi(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="laporan";
            $data['submenu']="lklas";
            if($this->input->post('submit')){
                $tahun = $this->input->post('tahun',TRUE);
                if($tahun){
                    $data['tahun'] = $this->input->post('tahun',TRUE);

                    $data['pidsisa']= $this->laporansess->cekSisaLalu(($tahun-1),'1');
                    $data['pdtsisa']= $this->laporansess->cekSisaLalu(($tahun-1),'2');
                    $data['pmbsisa']= $this->laporansess->cekSisaLalu(($tahun-1),'3');
                    $data['bbsisa']= $this->laporansess->cekSisaLalu(($tahun-1),'4');
                    $data['asisa']= $this->laporansess->cekSisaLalu(($tahun-1),'5');
                    $data['pidmsk']= $this->laporansess->cekSuratMasuk(($tahun),'1');
                    $data['pdtmsk']= $this->laporansess->cekSuratMasuk(($tahun),'2');
                    $data['pmbmsk']= $this->laporansess->cekSuratMasuk(($tahun),'3');
                    $data['bbmsk']= $this->laporansess->cekSuratMasuk(($tahun),'4');
                    $data['amsk']= $this->laporansess->cekSuratMasuk(($tahun),'5');
                    $data['pidsls']= $this->laporansess->cekTerselesaikan(($tahun),'1');
                    $data['pdtsls']= $this->laporansess->cekTerselesaikan(($tahun),'2');
                    $data['pmbsls']= $this->laporansess->cekTerselesaikan(($tahun),'3');
                    $data['bbsls']= $this->laporansess->cekTerselesaikan(($tahun),'4');
                    $data['asls']= $this->laporansess->cekTerselesaikan(($tahun),'5');
                    $data['pidtgk']= $this->laporansess->cekTunggakan(($tahun),'1');
                    $data['pdttgk']= $this->laporansess->cekTunggakan(($tahun),'2');
                    $data['pmbtgk']= $this->laporansess->cekTunggakan(($tahun),'3');
                    $data['bbtgk']= $this->laporansess->cekTunggakan(($tahun),'4');
                    $data['atgk']= $this->laporansess->cekTunggakan(($tahun),'5');
                    $this->load->view('element/header',$data);
                    $this->load->view('klasifikasilist',$data);
                    $this->load->view('element/footer');
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect('/laporan/klasifikasi');
                }
            }else{
                $data['list']=$this->laporansess->listTahun();
                $data['message'] = $this->session->flashdata('status');
                $data['tanggal'] = $this->session->flashdata('tgl');
                $this->load->view('element/header',$data);
                $this->load->view('klasifikasi',$data);
                $this->load->view('element/footer');
            }
        }else{
            redirect('/login/out');
        }
    }*/

    public function klasifikasi(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="laporan";
            $data['submenu']="lklas";
            if($this->input->post('submit')){
                $tahun = $this->input->post('tahun', TRUE);
                if($tahun){
                    $data['tahun'] = $this->input->post('tahun',TRUE);

                    $klasifikasi = $this->klasifikasi->get();

                    foreach ($klasifikasi as $key => $kls) {
                        $klasifikasi[$key]->sisa = $this->laporansess->cekSisaLalu(($tahun-1), $kls->id_klasifikasi)[0]->totagenda;
                        $klasifikasi[$key]->masuk = $this->laporansess->cekSuratMasuk(($tahun), $kls->id_klasifikasi)[0]->totmasuk;
                        $klasifikasi[$key]->selesai = $this->laporansess->cekTerselesaikan(($tahun), $kls->id_klasifikasi)[0]->totselesai;
                        $klasifikasi[$key]->tunggakan = $this->laporansess->cekTunggakan(($tahun), $kls->id_klasifikasi)[0]->tottunggakan;
                    }

                    // var_dump($klasifikasi); exit();
                    $data['klasifikasi'] = $klasifikasi;
                    $this->load->view('element/header',$data);
                    $this->load->view('klasifikasilist',$data);
                    $this->load->view('element/footer');
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect(base_url('laporan/klasifikasi'));
                }
            }else{
                $data['list']=$this->laporansess->listTahun();
                $data['message'] = $this->session->flashdata('status');
                $data['tanggal'] = $this->session->flashdata('tgl');
                $this->load->view('element/header',$data);
                $this->load->view('klasifikasi',$data);
                $this->load->view('element/footer');
            }
        }else{
            redirect(base_url('login/out'));
        }
    }

    public function pelaksana(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="laporan";
            $data['submenu']="lpelaksana";
            if($this->input->post('submit')){
                $tahun = $this->input->post('tahun',TRUE);
                if($tahun){
                    $data['tahun'] = $this->input->post('tahun',TRUE);
                    $data['staff'] = $this->laporansess->listStaff();
                    $this->load->view('element/header',$data);
                    $this->load->view('pelaksanalist',$data);
                    $this->load->view('element/footer');
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect('/laporan/pelaksana');
                }
            }else{
                $data['list']=$this->laporansess->listTahun();
                $data['message'] = $this->session->flashdata('status');
                $data['tanggal'] = $this->session->flashdata('tgl');
                $this->load->view('element/header',$data);
                $this->load->view('pelaksana',$data);
                $this->load->view('element/footer');
            }
        }else{
            redirect('/login/out');
        }
    }

    public function pelaksana2(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="laporan";
            $data['submenu']="lpelaksana";
            if($this->input->post('submit')){
                $tahun = $this->input->post('tahun',TRUE);
                if($tahun){
                    $data['tahun'] = $this->input->post('tahun',TRUE);
                    // $data['staff'] = $this->laporansess->listStaff();
                    $staff = $this->laporansess->listStaff();
                    $klasifikasi = $this->klasifikasi->get();

                    foreach ($staff as $key => $stff) {
                        $staff[$key]->kerja = array();
                        $kerja = array();
                        $totalTerima = 0;
                        $totalSelesai = 0;
                        foreach ($klasifikasi as $klskey => $kls) {
                            $terima = intval($klasifikasi[$klskey]->terima = $this->laporansess->cekTerima($tahun, $kls->id_klasifikasi, $stff->id_pegawai)[0]->totterima);
                            $selesai = intval($klasifikasi[$klskey]->selesai = $this->laporansess->cekSelesai($tahun, $kls->id_klasifikasi, $stff->id_pegawai)[0]->totselesai);

                            $kerja[] = array(
                                    'kode'      => $kls->kode,
                                    'terima'    => $terima,
                                    'selesai'   => $selesai
                                );
                            $totalTerima += $terima;
                            $totalSelesai += $selesai;
                        }
                        $kerja[] = array(
                                'kode'      => 'Total',
                                'terima'    => $totalTerima,
                                'selesai'   => $totalSelesai
                            );
                        $staff[$key]->kerja = $kerja;
                    }

                    // var_dump($staff); exit();
                    $data['staff'] = $staff;
                    $data['klasifikasi'] = $klasifikasi;

                    $this->load->view('element/header',$data);
                    $this->load->view('pelaksanalist2',$data);
                    $this->load->view('element/footer');
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect(base_url('laporan/pelaksana2'));
                }
            }else{
                $data['list']=$this->laporansess->listTahun();
                $data['message'] = $this->session->flashdata('status');
                $data['tanggal'] = $this->session->flashdata('tgl');
                $this->load->view('element/header',$data);
                $this->load->view('pelaksana',$data);
                $this->load->view('element/footer');
            }
        }else{
            redirect(base_url('login/out'));
        }
    }
}
