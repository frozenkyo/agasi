<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {

     public function __construct() {
        parent::__construct();

        $this->load->model('cetaksess');
        $this->load->model('laporansess');
        $this->load->model('klasifikasi');
        $this->load->library('session');
        $this->load->helper('tgl_indonesia');
     }

     public function index(){
         redirect(base_url()."dashboard");
     }

     public function single(){
         if($this->uri->segment(3)==NULL){
            redirect(base_url()."dashboard");
         }else{
            $this->load->library('m_pdf');
            $data['content']= $this->cetaksess->getDisposisi($this->uri->segment(3));
            // var_dump($data);exit();
            $html=$this->load->view('cetak_disp', $data, true);
            // var_dump($data);
            $pdfFilePath = "Disposisi Elektronik-".$data['content'][0]->no_agenda.".pdf";
            $pdf = $this->m_pdf->load();
            // $pdf->showImageErrors = true;
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "I");
         }
     }

     public function multipages(){
         if($this->uri->segment(3)==NULL){
            redirect(base_url()."dashboard");
         }else{
            $this->load->library('m_pdf');
            $data['contents']= $this->cetaksess->getDisposisiDate($this->uri->segment(3));
            // var_dump($data['contents']);exit();
            $html=$this->load->view('cetak_disp_massal', $data, true);
            $time = strtotime($this->uri->segment(3));
            $tes=tgl_indo2(date("Y-m-d", $time));

            $pdfFilePath = "Disposisi Elektronik Massal-".$tes.".pdf";
            $pdf = $this->m_pdf->load();
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "I");
         }
     }

     public function singlemanual(){
         if($this->uri->segment(3)==NULL){
            redirect(base_url()."dashboard");
         }else{
            $this->load->library('m_pdf');
            $data['content']= $this->cetaksess->getDisposisiManual($this->uri->segment(3));
            $html=$this->load->view('cetak_disp_manual', $data, true);
            $pdfFilePath = "Disposisi Manual-".$data['content'][0]->no_agenda.".pdf";
            $pdf = $this->m_pdf->load();
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "I");
         }
     }

     public function multipagesmanual(){
         if($this->uri->segment(3)==NULL){
            redirect(base_url()."dashboard");
         }else{
            $this->load->library('m_pdf');
            $data['content']= $this->cetaksess->getDisposisiDateManual($this->uri->segment(3));
            $html=$this->load->view('cetak_disp_manual_massal', $data, true);
            $time = strtotime($this->uri->segment(3));
            $tes=tgl_indo2(date("Y-m-d", $time));
            $pdfFilePath = "Disposisi Manual Massal-".$tes.".pdf";
            $pdf = $this->m_pdf->load();
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "I");
         }
     }

     /*public function lapklas(){
         if($this->uri->segment(3)==NULL){
            redirect(base_url()."dashboard");
         }else{
            $this->load->library('m_pdf');
            $data['pidsisa']= $this->laporansess->cekSisaLalu(($this->uri->segment(3)-1),'1');
            $data['pdtsisa']= $this->laporansess->cekSisaLalu(($this->uri->segment(3)-1),'2');
            $data['pmbsisa']= $this->laporansess->cekSisaLalu(($this->uri->segment(3)-1),'3');
            $data['bbsisa']= $this->laporansess->cekSisaLalu(($this->uri->segment(3)-1),'4');
            $data['asisa']= $this->laporansess->cekSisaLalu(($this->uri->segment(3)-1),'5');
            $data['pidmsk']= $this->laporansess->cekSuratMasuk(($this->uri->segment(3)),'1');
            $data['pdtmsk']= $this->laporansess->cekSuratMasuk(($this->uri->segment(3)),'2');
            $data['pmbmsk']= $this->laporansess->cekSuratMasuk(($this->uri->segment(3)),'3');
            $data['bbmsk']= $this->laporansess->cekSuratMasuk(($this->uri->segment(3)),'4');
            $data['amsk']= $this->laporansess->cekSuratMasuk(($this->uri->segment(3)),'5');
            $data['pidsls']= $this->laporansess->cekTerselesaikan(($this->uri->segment(3)),'1');
            $data['pdtsls']= $this->laporansess->cekTerselesaikan(($this->uri->segment(3)),'2');
            $data['pmbsls']= $this->laporansess->cekTerselesaikan(($this->uri->segment(3)),'3');
            $data['bbsls']= $this->laporansess->cekTerselesaikan(($this->uri->segment(3)),'4');
            $data['asls']= $this->laporansess->cekTerselesaikan(($this->uri->segment(3)),'5');
            $data['pidtgk']= $this->laporansess->cekTunggakan(($this->uri->segment(3)),'1');
            $data['pdttgk']= $this->laporansess->cekTunggakan(($this->uri->segment(3)),'2');
            $data['pmbtgk']= $this->laporansess->cekTunggakan(($this->uri->segment(3)),'3');
            $data['bbtgk']= $this->laporansess->cekTunggakan(($this->uri->segment(3)),'4');
            $data['atgk']= $this->laporansess->cekTunggakan(($this->uri->segment(3)),'5');

            $data['maks']=$this->laporansess->terimaMax($this->uri->segment(3));

            $data['tahun']=  $this->uri->segment(3);
            $html=$this->load->view('cetak_lapklas', $data, true);
            $pdfFilePath = "Laporan Surat Berdasar Klasifikasi Tahun ".$this->uri->segment(3)." - ".tgl_indo2(date("Y-m-d")).".pdf";
            $pdf = $this->m_pdf->load();
            $pdf->AddPage('L','','','','',10,10,10,10,6,3);
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "I");
         }
     }*/

     public function lapklas(){
         if($this->uri->segment(3)==NULL){
            redirect(base_url()."dashboard");
         }else{
            $this->load->library('m_pdf');
            $tahun = $this->uri->segment(3);

            $klasifikasi = $this->klasifikasi->get();

            foreach ($klasifikasi as $key => $kls) {
                $klasifikasi[$key]->sisa = $this->laporansess->cekSisaLalu(($tahun-1), $kls->id_klasifikasi)[0]->totagenda;
                $klasifikasi[$key]->masuk = $this->laporansess->cekSuratMasuk(($tahun), $kls->id_klasifikasi)[0]->totmasuk;
                $klasifikasi[$key]->selesai = $this->laporansess->cekTerselesaikan(($tahun), $kls->id_klasifikasi)[0]->totselesai;
                $klasifikasi[$key]->tunggakan = $this->laporansess->cekTunggakan(($tahun), $kls->id_klasifikasi)[0]->tottunggakan;
            }

            $data['maks']=$this->laporansess->terimaMax($this->uri->segment(3));
            $data['klasifikasi'] = $klasifikasi;

            $data['tahun']=  $tahun;
            $html=$this->load->view('cetak_lapklas2', $data, true);
            $pdfFilePath = "Laporan Surat Berdasar Klasifikasi Tahun ".$this->uri->segment(3)." - ".tgl_indo2(date("Y-m-d")).".pdf";
            $pdf = $this->m_pdf->load();
            $pdf->AddPage('L','','','','',10,10,10,10,6,3);
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "I");
         }
     }

     public function lappelaksana(){
         if($this->uri->segment(3)==NULL){
            redirect(base_url()."dashboard");
         }else{
            $this->load->library('m_pdf');
            $data['staff'] = $this->laporansess->listStaff();

            $data['maks']=$this->laporansess->terimaMax($this->uri->segment(3));

            $data['tahun']=  $this->uri->segment(3);
            $html=$this->load->view('cetak_lappelaksana', $data, true);
            $pdfFilePath = "Laporan Surat Berdasar Pelaksana Tahun ".$this->uri->segment(3)." - ".tgl_indo2(date("Y-m-d")).".pdf";
            $pdf = $this->m_pdf->load();
            $pdf->AddPage('L','','','','',10,10,10,10,6,3);
            $pdf->WriteHTML($html);
            $pdf->Output($pdfFilePath, "I");
         }
     }
}