<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klasifikasi extends CI_Controller {
        
        public function __construct() {
            parent::__construct();
            $this->load->helper('tgl_indonesia');
            $this->load->model('klasifikasisess');
            $this->load->library('session');
        }
        
	public function index()
	{
            redirect("/klasifikasi/klasifikasilist");
	}
        
        public function klasifikasilist()
	{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['menu']="klasifikasi";
                $data['submenu']="klaslist";
                $data['content']=  $this->klasifikasisess->listKlasifikasi();
                $data['message'] = $this->session->flashdata('status');
                $this->load->view('element/header',$data);
                $this->load->view('admin/klasifikasilist',$data);
                $this->load->view('element/footer');
            }else{
                redirect('/admin/out');
            }
	}
        
        public function newklasifikasi()
	{
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                $data['menu']="klasifikasi";
                $data['submenu']="klasadd";
                if($this->input->post('submit')){
                    $jenis=  $this->input->post('nama');
                    $kode=  $this->input->post('kode');
                    
                    $simpan=  $this->klasifikasisess->addKlas($jenis,$kode);
                    if($simpan){
                        $this->session->set_flashdata('status', 'success');
                        redirect('/klasifikasi/newklasifikasi');  
                    }else{
                        $this->session->set_flashdata('status', 'error');
                        redirect('/klasifikasi/newklasifikasi');  
                    }
                }else{
                    $data['message'] = $this->session->flashdata('status');
                    $this->load->view('element/header',$data);
                    $this->load->view('admin/newklasifikasi',$data);
                    $this->load->view('element/footer');
                }
            }else{
                redirect('/admin/out');
            }
	}
        
        
        public function delklas(){
            if($this->session->userdata('logged_in')){
                $session_data = $this->session->userdata('logged_in');
                $data['username'] = $session_data['username'];
                if($this->input->post('submit')){
                    $id_klasifikasi=  $this->input->post('id_klasifikasi');
                    
                    $cekdelete =  $this->klasifikasisess->cekKlas($id_klasifikasi);
                    if($cekdelete){
                        $this->session->set_flashdata('status', 'error');
                        redirect('/klasifikasi/klasifikasilist');
                    }else{
                        $hapus=$this->klasifikasisess->delKlas($id_klasifikasi);
                        if($hapus){
                            $this->session->set_flashdata('status', 'hapus');
                            redirect('/klasifikasi/klasifikasilist');
                        }else{
                            $this->session->set_flashdata('status', 'error');
                            redirect('/klasifikasi/klasifikasilist');
                        }
                    }
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect('/klasifikasi/klasifikasilist');
                }
            }else{
                redirect('/admin/out');
            }
        }
}
