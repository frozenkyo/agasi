<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masuk extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('masuksess');
        $this->load->model('dashboardsess');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('tgl_indonesia');
    }

	public function index()
	{
            redirect(base_url().'masuk/maillist');
	}

    public function maillist(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $hakakses = $session_data['privilege'];
            $data['hakakses'] = $session_data['privilege'];
            if(($hakakses>="1")&&($hakakses<="99")){
                $data['menu']="masuk";
                $data['submenu']="mlist";
                $data['content']= $this->masuksess->daftarSurat();
                $data['message'] = $this->session->flashdata('status');
                $this->load->view('element/header',$data);
                if(($hakakses=="22")||($hakakses=="99")){
                    $this->load->view('masuklist',$data);
                }else{
                    $this->load->view('masuklistviewonly',$data);
                }
                $this->load->view('element/footer');
            }else{
                redirect(base_url().'login/out');
            }
        }else{
            redirect(base_url().'login/out');
        }
    }

    public function newmail(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $hakakses = $session_data['privilege'];
            if($hakakses=="22"){
                $data['menu']="masuk";
                $data['submenu']="mwrite";
                $data['klasifikasi']=  $this->masuksess->daftarKlasifikasi();
                if($this->input->post('submit')){
                    $this->form_validation->set_rules('tgl_terima', 'Tanggal Terima', 'trim');
                    $this->form_validation->set_rules('perihal', 'Perihal', 'trim');
                    $this->form_validation->set_rules('tujuan', 'Tujuan', 'trim');
                    $this->form_validation->set_rules('nama', 'Nama', 'trim');
                    $this->form_validation->set_rules('alamat', 'Alamat', 'trim');
                    $this->form_validation->set_rules('ringkasan', 'Ringkasan', 'trim');
                    $this->form_validation->set_rules('tembusan', 'Tembusan', 'trim');
                    $this->form_validation->set_rules('salinan', 'Salinan', 'trim');
                    $this->form_validation->set_rules('no_surat', 'Nomor Surat', 'trim');
                    $this->form_validation->set_rules('tgl_surat', 'Tanggal Surat', 'trim');
                    $this->form_validation->set_rules('lampiran', 'Lampiran', 'trim');
                    $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');
                    $this->form_validation->set_rules('klasifikasi', 'Klasifikasi', 'trim');


                    if ($this->form_validation->run() == FALSE) {
                        $this->session->set_flashdata('status', 'fail');
                        redirect(base_url().'masuk/newmail');
                    }else{
                        $tgl_terima = $this->input->post('tgl_terima',TRUE);
                        $perihal = $this->input->post('perihal',TRUE);
                        $tujuan = $this->input->post('tujuan',TRUE);
                        $nama = $this->input->post('nama',TRUE);
                        $alamat = $this->input->post('alamat',TRUE);
                        $ringkasan = $this->input->post('ringkasan',TRUE);
                        $tembusan = $this->input->post('tembusan',TRUE);
                        $salinan = $this->input->post('salinan',TRUE);
                        $tgl_surat = $this->input->post('tgl_surat',TRUE);
                        $no_surat = $this->input->post('no_surat',TRUE);
                        $lampiran = $this->input->post('lampiran',TRUE);
                        $keterangan = $this->input->post('keterangan',TRUE);
                        $klasifikasi = $this->input->post('klasifikasi',TRUE);
                        $keamanan = $this->input->post('keamanan',TRUE);
                        $tujuandisposisi = $this->input->post('tujuandisp',TRUE);

                        $ceksurat=  $this->masuksess->cekSurat($tgl_terima,$perihal);
                        if($ceksurat){
                            $this->session->set_flashdata('status', 'duplicate');
                            redirect(base_url().'masuk/newmail');
                        }else{
                            $date = DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d H:i:s"));
                            $yearNow=$date->format("Y");
                            $cekNoAgenda= $this->masuksess->cekNoAgenda($yearNow);
                            $nextAgenda=($cekNoAgenda[0]->inc_agenda+1);
                            $klasCode= $this->masuksess->kodeKelas($klasifikasi);
                            $validAgenda=$nextAgenda."/".$klasCode[0]->kode."/".$yearNow;
                            $newmail= $this->masuksess->newMail($tgl_terima,$perihal,$tujuan,$nama,$alamat,$ringkasan,$tembusan,$salinan,$tgl_surat,$no_surat,$lampiran,$keterangan,$validAgenda,$klasifikasi,$keamanan,$nextAgenda);
                            if($newmail){
                                $this->masuksess->addDisposisi($newmail,$tujuandisposisi);
                                $config = array(
                                'upload_path' => "./incoming/",
                                'allowed_types' => "pdf",
                                'overwrite' => TRUE,
                                'max_size' => "0"
                                );
                                $this->load->library('upload', $config);
                                if($this->upload->do_upload("fileupload")){
                                    $data_upload = $this->upload->data();
                                    $file_name = $data_upload["file_name"];
                                    rename("./incoming/".$file_name, "./incoming/".$tgl_terima."-".md5($perihal).$data_upload['file_ext']);
                                    $newPath="incoming/".$tgl_terima."-".md5($perihal).$data_upload['file_ext'];
                                    $this->masuksess->updateUpload($newmail,$newPath);
                                    $this->session->set_flashdata('status', 'success');
                                    redirect(base_url().'masuk/newmail');
                                }else{
                                    $this->session->set_flashdata('status', 'errorupload');
                                    redirect(base_url().'masuk/newmail');
                                }
                            }else{
                                $this->session->set_flashdata('status', 'error');
                                redirect(base_url().'masuk/newmail');
                            }
                        }
                    }
                }else{
                    $date = DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d H:i:s"));
                    $yearNow=$date->format("Y");
                    $cekNoAgenda= $this->masuksess->cekNoAgenda($yearNow);
                    // var_dump($cekNoAgenda);
                    $data['nextagenda']=($cekNoAgenda[0]->inc_agenda+1);
                    $data['yearnow']=$yearNow;
                    $data['message'] = $this->session->flashdata('status');
                    $this->load->view('element/header',$data);
                    $this->load->view('newmasuk',$data);
                    $this->load->view('element/footer');
                }
            }else{
                redirect(base_url().'login/out');
            }
        }else{
            redirect(base_url().'login/out');
        }
    }

    public function reupload(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="masuk";
            $data['submenu']="mlist";
            if($this->input->post('submit')){
                $tgl_terima = $this->input->post('tgl_terima',TRUE);
                $perihal = $this->input->post('perihal',TRUE);
                $newmail = $this->input->post('id_agenda',TRUE);

                $config = array(
                'upload_path' => "./incoming/",
                'allowed_types' => "pdf",
                'overwrite' => TRUE,
                'max_size' => "0"
                );
                $this->load->library('upload', $config);
                if($this->upload->do_upload("fileupload")){
                    $data_upload = $this->upload->data();
                    $file_name = $data_upload["file_name"];
                    rename("./incoming/".$file_name, "./incoming/".$tgl_terima."-".md5($perihal).$data_upload['file_ext']);
                    $newPath="incoming/".$tgl_terima."-".md5($perihal).$data_upload['file_ext'];
                    $this->masuksess->updateUpload($newmail,$newPath);
                    $this->session->set_flashdata('status', 'success');
                    redirect(base_url().'masuk/maillist');
                }else{
//                        echo $this->upload->display_errors();
                    $this->session->set_flashdata('status', 'errorupload');
                    redirect(base_url().'masuk/maillist');
                }
            }else{
                if ($this->uri->segment(3) === NULL){
                    $data['content']="";
                }else{
                    $data['content'] = $this->masuksess->dataSurat($this->uri->segment(3));
                }
                $data['message'] = $this->session->flashdata('status');
                $this->load->view('element/header',$data);
                $this->load->view('reuploadmasuk',$data);
                $this->load->view('element/footer');
            }
        }else{
            redirect('/login/out');
        }
    }

    public function delagenda()
	{
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            if($this->input->post('submit')){
                $id_agenda=  $this->input->post('id_agenda');

                $delete=  $this->masuksess->copyDisposisi($id_agenda);
                if($delete){
                    $del2=$this->masuksess->copyAgenda($id_agenda);
                    if($del2){
                        $this->masuksess->delDisposisi($id_agenda);
                        $this->masuksess->delAgenda($id_agenda);

                        $message="Admin telah menghapus Agenda dan Disposisi nomor ".$id_agenda;
                        $this->masuksess->logAdmin($message);

                        $this->session->set_flashdata('status', 'hapus');
                        redirect(base_url().'masuk/maillist');
                    }else{
                        $this->session->set_flashdata('status', 'error');
                        redirect(base_url().'masuk/maillist');
                    }
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect(base_url().'masuk/maillist');
                }
            }else{
                $this->session->set_flashdata('status', 'error');
                redirect(base_url().'masuk/maillist');
            }
        }else{
            redirect(base_url().'admin/out');
        }
	}
}
