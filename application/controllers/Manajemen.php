<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manajemen extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('tgl_indonesia');
        $this->load->model('manajemensess');
        $this->load->library('session');
    }

	public function index()
	{
            redirect(base_url()."manajemen/userlist");
	}

    public function userlist()
	{
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="user";
            $data['submenu']="luser";
            $data['content']=  $this->manajemensess->listUser();
            $data['message'] = $this->session->flashdata('status');
            $this->load->view('element/header',$data);
            $this->load->view('admin/userlist',$data);
            $this->load->view('element/footer');
        }else{
            redirect(base_url().'admin/out');
        }
	}

    public function newuser()
	{
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="user";
            $data['submenu']="muser";
            if($this->input->post('submit')){
                $nama=  $this->input->post('lengkap');
                $telp=  $this->input->post('telepon');
                $email=  $this->input->post('email');
                $username=  $this->input->post('username');
                $password=  $this->input->post('password');
                $hakakses=  $this->input->post('hakakses');

                $simpan = $this->manajemensess->addUser($nama,$telp,$email,$username,$password,$hakakses);
                if($simpan){
                    $file_element_name = 'fileupload';

                    $user_upload_path = 'uploads/';

                    $config['upload_path'] = './' . $user_upload_path;
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']  = 1024 * 4;
                    $config['encrypt_name'] = TRUE;
                    $config['overwrite'] = TRUE;

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload($file_element_name)) {
                        $data['upload_error'] = $this->upload->display_errors();
                    } else {
                        $data_upload = $this->upload->data();

                        $file_name = $data_upload["file_name"];
                        $file_name_thumb = $data_upload['raw_name'].'_thumb' . $data_upload['file_ext'];

                        $this->load->library('image_lib');
                        $config_resize['image_library'] = 'gd2';
                        $config_resize['create_thumb'] = TRUE;
                        $config_resize['maintain_ratio'] = TRUE;
                        $config_resize['master_dim'] = 'height';
                        $config_resize['quality'] = "100%";
                        $config_resize['source_image'] = './' . $user_upload_path . $file_name;

                        $config_resize['height'] = 192;
                        $config_resize['width'] = 1;
                        $this->image_lib->initialize($config_resize);
                        $this->image_lib->resize();

                        unlink($user_upload_path.$file_name);
                        rename($user_upload_path.$file_name_thumb, $user_upload_path.$username.$data_upload['file_ext']);

                        $file_name_url= $user_upload_path.$username.$data_upload['file_ext'];

                        $this->manajemensess->updatePath($file_name_url, $simpan);

                        $this->session->set_flashdata('status', 'success');
                        redirect(base_url().'manajemen/newuser');
                    }

                } else {
                    $this->session->set_flashdata('status', 'error');
                    redirect(base_url().'manajemen/newuser');
                }
            }else{
                $data['message'] = $this->session->flashdata('status');
                $data['content']=  $this->manajemensess->listJabatan();
                $this->load->view('element/header',$data);
                $this->load->view('admin/newuser',$data);
                $this->load->view('element/footer');
            }
        }else{
            redirect(base_url().'admin/out');
        }
	}

    public function deluser()
	{
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="user";
            $data['submenu']="luser";
            if($this->input->post('submit')){
                $id_pegawai=  $this->input->post('id_pegawai');

                $delete=  $this->manajemensess->delUser($id_pegawai);
                if($delete){
                    $this->session->set_flashdata('status', 'hapus');
                    redirect(base_url().'manajemen/userlist');
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect(base_url().'manajemen/userlist');
                }
            }else{
                $this->session->set_flashdata('status', 'error');
                redirect(base_url().'manajemen/userlist');
            }
        }else{
            redirect(base_url().'admin/out');
        }
	}

    public function passwd()
	{
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="user";
            $data['submenu']="luser";
            if($this->input->post('submit')){
                $id_pegawai=  $this->input->post('id_pegawai');
                $password=  $this->input->post('password');

                $pass=  $this->manajemensess->passwdUser($id_pegawai,$password);
                if($pass){
                    $this->session->set_flashdata('status', 'password');
                    redirect(base_url().'manajemen/userlist');
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect(base_url().'manajemen/userlist');
                }
            }else{
                $this->session->set_flashdata('status', 'error');
                redirect(base_url().'manajemen/userlist');
            }
        }else{
            redirect(base_url().'admin/out');
        }
	}

    public function edit()
	{
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="user";
            $data['submenu']="luser";
            if($this->input->post('submit')){
                $id_pegawai=  $this->input->post('id_pegawai');
                $nama=  $this->input->post('lengkap');
                $telp=  $this->input->post('telepon');
                $email=  $this->input->post('email');

                $edit=  $this->manajemensess->editUser($id_pegawai,$nama,$telp,$email);
                if($edit){
                    $this->session->set_flashdata('status', 'edit');
                    redirect(base_url().'manajemen/userlist');
                }else{
                    $this->session->set_flashdata('status', 'error');
                    redirect(base_url().'manajemen/userlist');
                }
            }else{
                if($this->uri->segment(3)==NULL){
                    redirect(base_url().'admin/out');
                }else{
                    $data['content']=$this->manajemensess->dataPegawai($this->uri->segment(3));
                    $this->load->view('element/header',$data);
                    $this->load->view('admin/edituser',$data);
                    $this->load->view('element/footer');
                }
            }
        }else{
            redirect(base_url('admin/out'));
        }
	}

    public function switchuser()
	{
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="user";
            $data['submenu']="luser";
            if($this->uri->segment(3)==NULL){
                redirect(base_url().'admin/out');
            }else{
                $coba=$this->manajemensess->dataPegawai($this->uri->segment(3));
                if($coba[0]->status=="0"){
                    $this->manajemensess->nyalakan($this->uri->segment(3));
                    redirect(base_url().'manajemen/userlist');
                }else{
                    $this->manajemensess->matikan($this->uri->segment(3));
                    redirect(base_url().'manajemen/userlist');
                }
            }
        }else{
            redirect(base_url().'admin/out');
        }
	}
}
