<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disposisi extends CI_Controller {

        public function __construct() {
            parent::__construct();
            $this->load->helper('tgl_indonesia');
            $this->load->model('disposisisess');
            $this->load->model('dashboardsess');
            $this->load->library('form_validation');
            $this->load->library('session');
        }

	public function index()
	{
            redirect(base_url('disposisi/elektronik'));
	}

    public function elektronik(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="disposisi";
            $data['submenu']="elek";
            if($this->input->post('submit')){
                $tgl_terima = $this->input->post('tgl_terima',TRUE);
                $disposisi= $this->disposisisess->cekdisposisi($tgl_terima);
                if($disposisi){
                    $data['tgl']=$this->input->post('tgl_terima',TRUE);
                    $data['disposisi']= $this->disposisisess->cekDisposisi($tgl_terima);
                    $this->load->view('element/header',$data);
                    $this->load->view('edisposisilist',$data);
                    $this->load->view('element/footer');
                }else{
                    $this->session->set_flashdata('tgl', $tgl_terima);
                    $this->session->set_flashdata('status', 'warning');
                    redirect('/disposisi/elektronik');
                }
            }else{
                $data['message'] = $this->session->flashdata('status');
                $data['tanggal'] = $this->session->flashdata('tgl');
                $this->load->view('element/header',$data);
                $this->load->view('edisposisi',$data);
                $this->load->view('element/footer');
            }
        }else{
            redirect('/login/out');
        }
    }

    public function manual(){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['menu']="disposisi";
            $data['submenu']="manu";
            if($this->input->post('submit')){
                $tgl_terima = $this->input->post('tgl_terima',TRUE);
                $disposisi= $this->disposisisess->cekDisposisiManual($tgl_terima);
                if($disposisi){
                    $data['tgl']=$this->input->post('tgl_terima',TRUE);
                    $data['disposisi']= $this->disposisisess->cekDisposisiManual($tgl_terima);
                    $this->load->view('element/header',$data);
                    $this->load->view('mdisposisilist',$data);
                    $this->load->view('element/footer');
                }else{
                    $this->session->set_flashdata('tgl', $tgl_terima);
                    $this->session->set_flashdata('status', 'warning');
                    redirect(base_url('disposisi/manual'));
                }
            }else{
                $data['message'] = $this->session->flashdata('status');
                $data['tanggal'] = $this->session->flashdata('tgl');
                $this->load->view('element/header',$data);
                $this->load->view('mdisposisi',$data);
                $this->load->view('element/footer');
            }
        }else{
            redirect('/login/out');
        }
    }
}
