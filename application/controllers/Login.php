<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('loginsess');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

	public function index()
	{
        if($this->input->post('submit')){
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                //invalid login
                $this->session->set_flashdata('message', 'fail');
                redirect(base_url());
            }else{
                $username = $this->input->post('username',TRUE);
                $password = $this->input->post('password',TRUE);
                $result = $this->loginsess->loginauth($username, $password);
//                    var_dump($result);
                if($result){
                    if($result[0]->status=="1"){
                        $sess_data = array(
                            'username' => $username,
                            'valkey' => sha1($password),
                            'privilege' => $result[0]->id_jabatan,
                            'id_pegawai' => $result[0]->id_pegawai,
                            'jabatan' => $result[0]->jabatan,
                            'path' => $result[0]->path
                        );
                        $this->session->set_userdata('logged_in', $sess_data);
                        redirect(base_url().'dashboard');
                    }else{
                        $this->session->set_flashdata('message', 'suspend');
                        redirect(base_url());
                    }
                }else{
                    $this->session->set_flashdata('message', 'fail');
                    redirect(base_url());
                }
            }
        }else{
            $data['message'] = $this->session->flashdata('message');
            $this->load->view('login',$data);
        }
	}

    public function out()
    {
        $sess_data = array(
            'username' => '',
            'valkey' => ''
        );
        $this->session->set_userdata('logged_in', $sess_data);
        redirect(base_url());
    }
}
