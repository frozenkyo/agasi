<?php
class Laporansess extends CI_Model {

    function listTahun(){
        $this -> db -> select('distinct(year(tgl_staff)) as tahun');
        $this -> db -> from('disposisi');
        $this -> db -> where('tgl_staff >', '0');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekSisaLalu($tahun,$klasifikasi){
        $this -> db -> select('count(b.id_agenda) as totagenda');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
//        $this -> db -> group_start()
//                -> where('year(a.tgl_staff ) =', $tahun)
//                -> or_where('year(a.tgl_staff ) =', '0000')
//                ->group_end();
        $this -> db -> where('year(b.tgl_penerimaan) =', $tahun);
        $this -> db -> where('b.id_klasifikasi', $klasifikasi);
        $this -> db -> group_start()
                -> where('a.status <', '6')
                -> or_where('a.status ', '99')
                ->group_end();

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekSuratMasuk($tahun,$klasifikasi){
        $this -> db -> select('count(id_agenda) as totmasuk');
        $this -> db -> from('agenda');
        $this -> db -> where('year(tgl_penerimaan) =', $tahun);
        $this -> db -> where('id_klasifikasi', $klasifikasi);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekTerselesaikan($tahun,$klasifikasi){
        $this -> db -> select('count(b.id_agenda) as totselesai');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('year(b.tgl_penerimaan ) =', $tahun);
        $this -> db -> where('b.id_klasifikasi', $klasifikasi);
        $this -> db -> where('a.status ', '100');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekTunggakan($tahun,$klasifikasi){
        $this -> db -> select('count(b.id_agenda) as tottunggakan');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('year(b.tgl_penerimaan) =', $tahun);
        $this -> db -> where('b.id_klasifikasi', $klasifikasi);
        $this -> db -> group_start()
                -> where('a.status <', '6')
                -> or_where('a.status ', '99')
                ->group_end();

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function listStaff(){
        $this -> db -> select('a.nama,a.id_pegawai,b.jabatan');
        $this -> db -> from('pegawai a');
        $this -> db -> join('jabatan b', 'a.id_jabatan = b.id_jabatan', 'left');
        $this -> db -> where('a.id_jabatan >', '20');
        $this -> db -> where('a.id_jabatan <', '35');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekTerima($tahun,$klasifikasi,$id_pegawai){
        $this->db->select('count(a.id_disposisi) as totterima');
        $this->db->from('disposisi a');
        $this->db->join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this->db->where('year(a.tgl_staff)', $tahun);
        $this->db->where('b.id_klasifikasi', $klasifikasi);
        $this->db->where('a.id_pegawai', $id_pegawai);
        $this->db->where('a.status >', '4');

        $query = $this->db->get();

        if($query->num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekTotalTerima($tahun,$id_pegawai){
        $this -> db -> select('count(a.id_disposisi) as totterima');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('year(a.tgl_staff)', $tahun);
        $this -> db -> where('a.id_pegawai', $id_pegawai);
        $this -> db -> where('a.status >', '4');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekSelesai($tahun,$klasifikasi,$id_pegawai){
        $this -> db -> select('count(a.id_disposisi) as totselesai');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('year(a.tgl_staff)', $tahun);
        $this -> db -> where('b.id_klasifikasi', $klasifikasi);
        $this -> db -> where('a.id_pegawai', $id_pegawai);
        $this -> db -> where('a.status ', '100');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekTotalSelesai($tahun,$id_pegawai){
        $this -> db -> select('count(a.id_disposisi) as totselesai');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('year(a.tgl_staff)', $tahun);
        $this -> db -> where('a.id_pegawai', $id_pegawai);
        $this -> db -> where('a.status ', '100');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function terimaMax($tahun){
        $this -> db -> select('MAX(tgl_penerimaan) as maksimal');
        $this -> db -> from('agenda');
        $this -> db -> where('year(tgl_penerimaan)', $tahun);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
}