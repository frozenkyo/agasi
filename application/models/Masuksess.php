<?php
class Masuksess extends CI_Model {

    function cekSurat($tgl_terima,$perihal){
        $this -> db -> select('*');
        $this -> db -> from('agenda');
        $this -> db -> where('tgl_penerimaan', $tgl_terima);
        $this -> db -> where('hal_surat', $perihal);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function newMail($tgl_terima,$perihal,$tujuan,$nama,$alamat,$ringkasan,$tembusan,$salinan,$tgl_surat,$no_surat,$lampiran,$keterangan,$no_agenda,$klasifikasi,$keamanan,$nextAgenda){
        $data = array
            (
                'tgl_penerimaan' => $tgl_terima,
                'hal_surat' => $perihal,
                'tujuan' => $tujuan,
                'nama_pengirim' => $nama,
                'alamat_pengirim' => $alamat,
                'isi_ringkas' => $ringkasan,
                'tembusan' => $tembusan,
                'salinan' => $salinan,
                'tgl_srt_masuk' => $tgl_surat,
                'no_srt_masuk' => $no_surat,
                'lampiran' => $lampiran,
                'keterangan' => $keterangan,
                'no_agenda' => $no_agenda,
                'id_klasifikasi' => $klasifikasi,
                'keamanan' => $keamanan,
                'inc_agenda' => $nextAgenda
            );
        $return=$this->db->insert('agenda',$data);
        if($return){
            $insert_id = $this->db->insert_id();
            return  $insert_id;
        }else{
            return false;
        }
    }

    function addDisposisi($idagenda,$tujuandisp){
        $data = array
            (
                'tujuan_utama'      => $tujuandisp,
                'id_agenda'         => $idagenda,
                'status'            => '1'
            );
        $return=$this->db->insert('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function cekNoAgenda($year){
        $this->db->select_max('inc_agenda');
        $this->db->from('agenda');
        $this->db->like('YEAR(tgl_penerimaan)', $year);

        $query = $this->db->get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            if ($year == "2016") {
                return "201";
            }
            return "0";
        }
    }

    function daftarSurat(){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,a.id_pegawai,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tgl_penerimaan,b.nama_pengirim,b.hal_surat,b.isi_ringkas,b.filename,c.nama,a.id_pegawai,a.note,a.tgl_staff');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> order_by('id_disposisi','DESC');
        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function daftarKlasifikasi(){
        $this -> db -> select('*');
        $this -> db -> from('klasifikasi');
        $this -> db -> order_by('id_klasifikasi', 'ASC');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function updateUpload($newmail,$newPath){
        $data = array
            (
                'filename' => $newPath
            );
        $this -> db -> where('id_agenda', $newmail);
        $return=$this->db->update('agenda',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function kodeKelas($klasifikasi){
        $this -> db -> select('kode');
        $this -> db -> from('klasifikasi');
        $this -> db -> where('id_klasifikasi', $klasifikasi);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function dataSurat($idagenda){
        $this -> db -> select('*');
        $this -> db -> from('agenda');
        $this -> db -> where('id_agenda', $idagenda);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function idJabatanToNama($idjabatan){
        $this -> db -> select('jabatan');
        $this -> db -> from('jabatan');
        $this -> db -> where('id_jabatan', $idjabatan);


        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function delDisposisi($idagenda){
       $this->db->where('id_agenda', $idagenda);
        $return=$this->db->delete('disposisi');
        if($return){
            return true;
        }else{
            return false;
        }
    }

    function copyDisposisi($idagenda){
        $query = $this->db->query("insert into trash_disposisi select * from disposisi where id_agenda = '".$idagenda."'");

        if($query)
        {
            return $query;
        }else{
            return false;
        }
    }

    function delAgenda($idagenda){
       $this->db->where('id_agenda', $idagenda);
        $return=$this->db->delete('agenda');
        if($return){
            return true;
        }else{
            return false;
        }
    }

    function copyAgenda($idagenda){
        $query = $this->db->query("insert into trash_agenda select * from agenda where id_agenda = '".$idagenda."'");

        if($query)
        {
            return $query;
        }else{
            return false;
        }
    }

    function logAdmin($message){
        $data = array
            (
                'ket' => $message
            );
        $this->db->set('exetime', 'NOW()', FALSE);
        $return=$this->db->insert('log_admin',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }
}