<?php
class Disposisisess extends CI_Model {
    
    function cekDisposisi($tgl){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,a.id_pegawai,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tgl_penerimaan,b.nama_pengirim,b.hal_surat,b.isi_ringkas,b.filename,c.nama,a.id_pegawai,a.note,a.tgl_staff');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '4');
        $this -> db -> like('b.tgl_penerimaan', $tgl);
        

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function cekDisposisiManual($tgl){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,a.id_pegawai,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tgl_penerimaan,b.nama_pengirim,b.hal_surat,b.isi_ringkas,b.filename,c.nama,a.id_pegawai,a.note,a.tgl_staff');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '0');
        $this -> db -> like('b.tgl_penerimaan', $tgl);
        

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
}