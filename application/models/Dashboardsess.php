<?php
class Dashboardsess extends CI_Model {

    function cekDisposisi($hakakses){
        $this -> db -> select('*');
        $this -> db -> from('disposisi a');
        $this -> db -> from('agenda b');
        $this -> db -> where('a.status', '5');
//        $this -> db -> or_where('a.status', '5');
        $this -> db -> where('a.id_pegawai', $hakakses);
        $this -> db -> where('a.id_agenda', 'b.id_agenda');


        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekDisposisi2($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat,a.id_pegawai,a.note,a.tgl_staff');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('a.status', '5');
//        $this -> db -> or_where('a.status', '5');
        $this -> db -> where('a.id_pegawai', $hakakses);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekDisposisiStaff($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('a.status', '5');
        $this -> db -> where('a.id_pegawai', $hakakses);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekDisposisiKetua($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tujuan,b.nama_pengirim,b.tgl_penerimaan,b.filename,b.isi_ringkas');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('a.status', '1');
        $this -> db -> where('a.tujuan_utama', $hakakses);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekDisposisiPansek($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tujuan,b.nama_pengirim,b.tgl_penerimaan,b.filename,b.isi_ringkas');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('a.status', '2');
        $this -> db -> where('a.tujuan_pansek', $hakakses);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekDisposisiWaPansek($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tujuan,b.nama_pengirim,b.tgl_penerimaan,b.filename,b.isi_ringkas');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('a.status', '3');
        $this -> db -> where('a.tujuan_wapansek', $hakakses);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekDisposisiPanmud($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tujuan,b.nama_pengirim,b.tgl_penerimaan,b.filename,b.isi_ringkas');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('a.status', '4');
        $this -> db -> where('a.tujuan_panmud', $hakakses);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }


    function jmlMasuk(){
        $this -> db -> select('*');
        $this -> db -> from('agenda');

        $query = $this->db->count_all_results();

        if($query)
        {
            return $query;
        }else{
            return "0";
        }
    }

    function jmlKeluar(){
        $this -> db -> select('*');
        $this -> db -> from('surat_keluar');

        $query = $this->db->count_all_results();

        if($query)
        {
            return $query;
        }else{
            return "0";
        }
    }

    function jmlSelesai(){
        $this -> db -> select('*');
        $this -> db -> from('disposisi');
        $this -> db -> where('status', '100');

        $query = $this->db->count_all_results();

        if($query)
        {
            return $query;
        }else{
            return "0";
        }
    }

    function updateReffSelesai($reff){
        $data = array
            (
                'status' => '100',
                'note'  => 'Selesai'
            );
        $this->db->set('tgl_staff', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $reff);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function updateReffPending($reff,$note){
        $data = array
            (
                'status' => '99',
                'note'  => $note
            );
        $this->db->set('tgl_staff', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $reff);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function disposisiToAgenda($iddisposisi){
        $this -> db -> select('id_agenda');
        $this -> db -> from('disposisi');
        $this -> db -> where('id_disposisi', $iddisposisi);


        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function viewAgenda($idagenda){
        $this -> db -> select('*');
        $this -> db -> from('agenda');
        $this -> db -> where('id_agenda', $idagenda);


        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function viewDisposisi($iddisposisi){
        $this -> db -> select('*');
        $this -> db -> from('disposisi');
        $this -> db -> where('id_disposisi', $iddisposisi);


        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function listStaff(){
        $this -> db -> select('a.nama,a.id_pegawai,b.jabatan');
        $this -> db -> from('pegawai a');
        $this -> db -> join('jabatan b', 'a.id_jabatan = b.id_jabatan', 'left');
        $this -> db -> where('a.id_jabatan >', '20');
        $this -> db -> where('a.id_jabatan <', '30');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function listStaffKhusus($staff){
        $this -> db -> select('*');
        $this -> db -> from('pegawai');
        $this -> db -> where('id_jabatan', $staff);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function cekStaff($hakakses){
        $this->db->select('staff');
        $this->db->from('koordinasi');
        $this->db->where('kepala', $hakakses);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function disposisiKePansek($id_disposisi,$tanggapan,$arahan){
        $data = array
            (
                'tanggapan_utama' => $tanggapan,
                'tujuan_pansek' => $arahan,
                'status' => '2'
            );
        $this->db->set('tgl_utama', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $id_disposisi);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function disposisiKeWaPansek($id_disposisi,$tanggapan,$arahan){
        $data = array
            (
                'tanggapan_utama' => $tanggapan,
                'tujuan_wapansek' => $arahan,
                'status' => '3'
            );
        $this->db->set('tgl_utama', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $id_disposisi);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function disposisiKePanmud($id_disposisi,$tanggapan,$arahan){
        $data = array
            (
                'tanggapan_utama' => $tanggapan,
                'tujuan_panmud' => $arahan,
                'status' => '4'
            );
        $this->db->set('tgl_utama', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $id_disposisi);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function disposisiPansekKeWapansek($id_disposisi,$tanggapan,$arahan){
        $data = array
            (
                'tanggapan_pansek' => $tanggapan,
                'tujuan_wapansek' => $arahan,
                'status' => '3'
            );
        $this->db->set('tgl_pansek', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $id_disposisi);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function disposisiPansekKePanmud($id_disposisi,$tanggapan,$arahan){
        $data = array
            (
                'tanggapan_pansek' => $tanggapan,
                'tujuan_panmud' => $arahan,
                'status' => '4'
            );
        $this->db->set('tgl_pansek', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $id_disposisi);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function disposisiPansekKeStaff($id_disposisi,$tanggapan,$arahan){
        $data = array
            (
                'tanggapan_pansek' => $tanggapan,
                'status' => '5',
                'id_pegawai' => $arahan
            );
        $this->db->set('tgl_pansek', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $id_disposisi);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function disposisiWaPansekKePanmud($id_disposisi,$tanggapan,$arahan){
        $data = array
            (
                'tanggapan_wapansek' => $tanggapan,
                'tujuan_panmud' => $arahan,
                'status' => '4'
            );
        $this->db->set('tgl_wapansek', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $id_disposisi);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function disposisiWaPansekKeStaff($id_disposisi,$tanggapan,$arahan){
        $data = array
            (
                'tanggapan_wapansek' => $tanggapan,
                'status' => '5',
                'id_pegawai' => $arahan
            );
        $this->db->set('tgl_wapansek', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $id_disposisi);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function disposisiPanmudKeStaff($id_disposisi,$tanggapan,$arahan){
        $data = array
            (
                'tanggapan_panmud' => $tanggapan,
                'status' => '5',
                'id_pegawai' => $arahan
            );
        $this->db->set('tgl_panmud', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $id_disposisi);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }

    function daftarSuratMasukReff($idagenda){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_panmud,a.status,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('id_disposisi', $idagenda);
        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function idJabatanToNama($idjabatan){
        $this -> db -> select('jabatan');
        $this -> db -> from('jabatan');
        $this -> db -> where('id_jabatan', $idjabatan);


        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function idPegawaiToidJabatan($idpegawai){
        $this -> db -> select('id_jabatan');
        $this -> db -> from('pegawai');
        $this -> db -> where('id_pegawai', $idpegawai);


        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
}