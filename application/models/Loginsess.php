<?php
class Loginsess extends CI_Model {
    
    function loginauth($username, $password){
        $this -> db -> select('a.id_jabatan,a.id_pegawai,b.jabatan,a.path,a.status');
        $this -> db -> from('pegawai a');
        $this -> db -> join('jabatan b', 'a.id_jabatan = b.id_jabatan', 'left');
        $this -> db -> where('a.username', $username);
        $this -> db -> where('a.password', sha1($password));
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
}