<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klasifikasi extends CI_Model {

	public function get() {
		$query = $this->db->get('klasifikasi');
		return $query->result();
	}
}