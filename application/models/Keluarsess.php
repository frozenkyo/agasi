<?php
class Keluarsess extends CI_Model {
    
    function cekSurat($tgl_surat,$perihal){
        $this -> db -> select('*');
        $this -> db -> from('surat_keluar');
        $this -> db -> where('tgl_srt_keluar', $tgl_surat);
        $this -> db -> where('perihal', $perihal);
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function newMail($tgl_surat,$no_surat,$perihal,$tujuan,$lampiran,$keterangan,$referensi,$idpegawai){
        $data = array
            (
                'tgl_srt_keluar' => $tgl_surat,
                'no_srt_keluar' => $no_surat,
                'perihal' => $perihal,
                'tujuan' => $tujuan,
                'lampiran' => $lampiran,
                'ket_srt' => $keterangan,
                'id_agenda' => $referensi,
                'id_pegawai' => $idpegawai
            );
        $return=$this->db->insert('surat_keluar',$data);
        if($return){
            $insert_id = $this->db->insert_id();
            return  $insert_id;
        }else{
            return false;
        }
    }
    
    function daftarSurat(){
        $this -> db -> select('a.id_srt_keluar,a.no_srt_keluar,a.tgl_srt_keluar,a.perihal,a.tujuan,a.filename,b.nama');
        $this -> db -> from('surat_keluar a');
        $this -> db -> join('pegawai b', 'a.id_pegawai = b.id_pegawai', 'left');
        $this -> db -> order_by('id_srt_keluar', 'DESC');
        
        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function daftarKlasifikasi(){
        $this -> db -> select('*');
        $this -> db -> from('klasifikasi');
        $this -> db -> order_by('id_klasifikasi', 'ASC');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function daftarSuratMasuk(){
        $this -> db -> select('*');
        $this -> db -> from('agenda');
        $this -> db -> order_by('tgl_penerimaan', 'DESC');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function daftarSuratMasukReff($idagenda){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_panmud,a.status,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> where('id_disposisi', $idagenda);
        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function cekNoAgenda($year){
        $this -> db -> select_max('id_srt_keluar');
        $this -> db -> from('surat_keluar');
        $this -> db -> where('YEAR(tgl_srt_keluar)', $year);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            if ($year == "2016") {
                return "201";
            }
            return "0";
        }
    }
    
    function updateUpload($newmail,$newPath){
        $data = array
            (
                'filename' => $newPath
            );
        $this -> db -> where('id_srt_keluar', $newmail);
        $return=$this->db->update('surat_keluar',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }
    
    function updateReff($reff){
        $data = array
            (
                'status' => '100',
                'note'  => 'Selesai'
            );
        $this->db->set('tgl_staff', 'NOW()', FALSE);
        $this -> db -> where('id_disposisi', $reff);
        $return=$this->db->update('disposisi',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }
    
    function dataSurat($idsrtkeluar){
        $this -> db -> select('*');
        $this -> db -> from('surat_keluar');
        $this -> db -> where('id_srt_keluar', $idsrtkeluar);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function delKeluar($idsrtkeluar){
       $this->db->where('id_srt_keluar', $idsrtkeluar);
        $return=$this->db->delete('surat_keluar');
        if($return){
            return true;
        }else{
            return false;
        }
    }
    
    function copyKeluar($idsrtkeluar){
        $query = $this->db->query("insert into trash_keluar select * from surat_keluar where id_srt_keluar = '".$idsrtkeluar."'");

        if($query)
        {
            return $query;
        }else{
            return false;
        }
    }
    
    function logAdmin($message){
        $data = array
            (
                'ket' => $message
            );
        $this->db->set('exetime', 'NOW()', FALSE);
        $return=$this->db->insert('log_admin',$data);
        if($return){
            return $return;
        }else{
            return false;
        }
    }
}