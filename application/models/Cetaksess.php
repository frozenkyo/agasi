<?php
class Cetaksess extends CI_Model {
    
    function getDisposisi($iddisposisi){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.tgl_staff,a.note,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tujuan,b.nama_pengirim,b.tgl_penerimaan,b.filename,b.isi_ringkas,b.keamanan,b.tgl_srt_masuk,b.no_srt_masuk,b.lampiran,c.nama');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '4');
        $this -> db -> where('a.id_disposisi', $iddisposisi);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function getDisposisiDate($tgl){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.tgl_staff,a.note,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tujuan,b.nama_pengirim,b.tgl_penerimaan,b.filename,b.isi_ringkas,b.keamanan,b.tgl_srt_masuk,b.no_srt_masuk,b.lampiran,c.nama');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '4');
        $this -> db -> like('b.tgl_penerimaan', $tgl);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function getDisposisiManual($iddisposisi){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tujuan,b.nama_pengirim,b.tgl_penerimaan,b.filename,b.isi_ringkas,b.keamanan,b.tgl_srt_masuk,b.no_srt_masuk,b.lampiran,c.nama');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '0');
        $this -> db -> where('a.id_disposisi', $iddisposisi);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function getDisposisiDateManual($tgl){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tujuan,b.nama_pengirim,b.tgl_penerimaan,b.filename,b.isi_ringkas,b.keamanan,b.tgl_srt_masuk,b.no_srt_masuk,b.lampiran,c.nama');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '0');
        $this -> db -> like('b.tgl_penerimaan', $tgl);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function idJabatanToNama($idjabatan){
        $this -> db -> select('jabatan');
        $this -> db -> from('jabatan');
        $this -> db -> where('id_jabatan', $idjabatan);
        

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
}