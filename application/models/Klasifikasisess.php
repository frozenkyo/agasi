<?php
class Klasifikasisess extends CI_Model {
    
    function listKlasifikasi(){
        $this -> db -> select('*');
        $this -> db -> from('klasifikasi');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function cekKlas($idklasifikasi){
        $this -> db -> select('*');
        $this -> db -> from('agenda');
        $this -> db -> where('id_klasifikasi',$idklasifikasi);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function delKlas($idklasifikasi){
        $this->db->where('id_klasifikasi', $idklasifikasi);
        $return=$this->db->delete('klasifikasi');
        if($return){
            return true;
        }else{
            return false;
        }
    }
    
    function addKlas($jenis,$kode){
        $data = array
            (
                'jenis' => $jenis,
                'kode' => $kode
            );
        $return=$this->db->insert('klasifikasi',$data);
        if($return){
            $insert_id = $this->db->insert_id();
            return  $insert_id;
        }else{
            return false;
        }
    }
    
}