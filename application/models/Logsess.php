<?php
class Logsess extends CI_Model {
    
    function cekDisposisiKetua($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,a.id_pegawai,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tgl_penerimaan,b.nama_pengirim,b.hal_surat,b.isi_ringkas,b.filename,c.nama,a.id_pegawai,a.note,a.tgl_staff');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '1');
        $this -> db -> where('a.tujuan_utama', $hakakses);
        $this -> db -> order_by('b.tgl_penerimaan', 'DESC');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function cekDisposisiPansek($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,a.id_pegawai,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tgl_penerimaan,b.nama_pengirim,b.hal_surat,b.isi_ringkas,b.filename,c.nama,a.id_pegawai,a.note,a.tgl_staff');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '2');
        $this -> db -> where('a.tujuan_pansek', $hakakses);
        $this -> db -> order_by('b.tgl_penerimaan', 'DESC');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function cekDisposisiWaPansek($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,a.id_pegawai,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tgl_penerimaan,b.nama_pengirim,b.hal_surat,b.isi_ringkas,b.filename,c.nama,a.id_pegawai,a.note,a.tgl_staff');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '3');
        $this -> db -> where('a.tujuan_wapansek', $hakakses);
        $this -> db -> order_by('b.tgl_penerimaan', 'DESC');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function cekDisposisiPanmud($hakakses){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,a.id_pegawai,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tgl_penerimaan,b.nama_pengirim,b.hal_surat,b.isi_ringkas,b.filename,c.nama,a.id_pegawai,a.note,a.tgl_staff');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '4');
        $this -> db -> where('a.tujuan_panmud', $hakakses);
        $this -> db -> order_by('b.tgl_penerimaan', 'DESC');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function cekDisposisiStaff($idpegawai){
        $this -> db -> select('a.id_disposisi,a.tujuan_utama,a.tanggapan_utama,a.tujuan_pansek,a.tanggapan_pansek,a.tujuan_wapansek,a.tanggapan_wapansek,a.tujuan_panmud,a.tanggapan_panmud,a.tgl_utama,a.tgl_pansek,a.tgl_wapansek,a.tgl_panmud,a.status,a.id_pegawai,b.id_agenda,b.no_agenda,b.no_srt_masuk,b.hal_surat,b.tgl_penerimaan,b.nama_pengirim,b.hal_surat,b.isi_ringkas,b.filename,c.nama,a.id_pegawai,a.note,a.tgl_staff');
        $this -> db -> from('disposisi a');
        $this -> db -> join('agenda b', 'a.id_agenda = b.id_agenda', 'left');
        $this -> db -> join('pegawai c', 'a.id_pegawai = c.id_pegawai', 'left');
        $this -> db -> where('a.status >', '5');
        $this -> db -> where('a.id_pegawai', $idpegawai);
        $this -> db -> order_by('b.tgl_penerimaan', 'DESC');

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
    
    function idJabatanToNama($idjabatan){
        $this -> db -> select('jabatan');
        $this -> db -> from('jabatan');
        $this -> db -> where('id_jabatan', $idjabatan);
        

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }
}