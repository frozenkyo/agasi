<?php
class Manajemensess extends CI_Model {

    function listUser(){
        $this->db->select('a.*, b.jabatan');
        $this->db->from('pegawai a');
        $this->db->join('jabatan b', 'a.id_jabatan = b.id_jabatan', 'left');
        $this->db->where('a.id_jabatan <', '99');
        $this->db->order_by('a.id_jabatan asc, a.id_pegawai asc');

        $query = $this->db->get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function listJabatan() {
        $query = $this->db->query("
                SELECT id_jabatan, jabatan
                FROM jabatan
                WHERE id_jabatan < 99
                ORDER BY id_jabatan
            ");

        return $query->result();
    }

    function addUser($nama,$telp,$email,$username,$password,$hakakses){
        $data = array(
                'nama'          => $nama,
                'telepon'       => $telp,
                'email'         => $email,
                'username'      => $username,
                'password'      => sha1($password),
                'vpassword'     => $password,
                'id_jabatan'    => $hakakses,
                'status'        => '1'
            );
        $result = $this->db->insert('pegawai', $data);
        if ($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    function delUser($idpegawai){
        $this->db->where('id_pegawai', $idpegawai);
        $return=$this->db->delete('pegawai');
        if($return){
            return true;
        }else{
            return false;
        }
    }

    function passwdUser($idpegawai,$password){
        $data = array
            (
                'password' => sha1($password),
                'vpassword' => $password,
            );
        $this->db->where('id_pegawai', $idpegawai);
        $return=$this->db->update('pegawai',$data);
        if($return){
            return  true;
        }else{
            return false;
        }
    }

    function editUser($idpegawai,$nama,$telp,$email){
        $data = array
            (
                'nama' => $nama,
                'telepon' => $telp,
                'email' => $email
            );
        $this->db->where('id_pegawai', $idpegawai);
        $return=$this->db->update('pegawai',$data);
        if($return){
            return  true;
        }else{
            return false;
        }
    }

    function dataPegawai($idpegawai){
        $this -> db -> select('a.*, b.jabatan');
        $this -> db -> from('pegawai a');
        $this -> db -> join('jabatan b', 'a.id_jabatan = b.id_jabatan', 'left');
        $this -> db -> where('a.id_pegawai ', $idpegawai);

        $query = $this -> db -> get();

        if($query -> num_rows() >= 1)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function updatePath($path, $idpegawai){
        $data = array(
                'path' => $path
            );
        // $this->db->where('id_pegawai', $idpegawai);
        $result = $this->db->update('pegawai', $data, array('id_pegawai' => $idpegawai));
        return $result;
    }

    function nyalakan($idpegawai){
        $data = array
            (
                'status' => '1'
            );
        $this->db->where('id_pegawai', $idpegawai);
        $return=$this->db->update('pegawai',$data);
        if($return){
            return  true;
        }else{
            return false;
        }
    }

    function matikan($idpegawai){
        $data = array
            (
                'status' => '0'
            );
        $this->db->where('id_pegawai', $idpegawai);
        $return=$this->db->update('pegawai',$data);
        if($return){
            return  true;
        }else{
            return false;
        }
    }
}