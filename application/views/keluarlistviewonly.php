<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="#"><i class="fa fa-home"></i> Surat Keluar</a></li>
                <li class="active">Data Surat</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Surat Keluar</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    
                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">DATA SURAT KELUAR</header>
                    <div class="panel-body">
                        <?php if($message=="success"){ ?>
                        <!-- Error Message -->
                        <div class="alert fade in alert-success fail" id="fail">
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Data upload berhasil ditambahkan.
                        </div>   
                        <?php }else if($message=="errorupload"){ ?>
                        <div class="alert fade in alert-danger invalid" id="invalid">
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Data upload yang anda masukkan gagal diupload.
                        </div> 
                        
                        <?php } ?>
                        <div class="adv-table">
                            <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No. Surat</th>
                                    <th class="hidden-phone">Tanggal Surat</th>
                                    <th class="hidden-phone">Perihal</th>
                                    <th class="hidden-phone">Tujuan</th>
                                    <th class="hidden-phone">Nama Pengirim</th>
                                    <th class="hidden-phone">Berkas</th>
                                </thead>
                                <tbody>
                                <?php 
                                if($content!=false){
                                $i=0;
                                foreach($content as $isi){
                                ?>    
                                <tr>
                                    <td><?php echo $i+1; ?></td>
                                    <td><?php echo $isi->no_srt_keluar; ?></td>
                                    <td class="hidden-phone"><?php echo tgl_indo2(date("Y-m-d", strtotime($isi->tgl_srt_keluar))); ?></td>
                                    <td class="hidden-phone"><?php echo $isi->perihal; ?></td>
                                    <td class="hidden-phone"><?php echo $isi->tujuan; ?></td>
                                    <td class="hidden-phone"><?php echo $isi->nama?></td>
                                    <td class="hidden-phone center"><?php if($isi->filename==""){ echo "<span class=\"label bg-danger\">Belum Diupload</span>"; }else{ echo "<a href=\"".base_url().$isi->filename."\" target=\"_blank\" ><span class=\"label bg-success\">Lihat Berkas</span></a>"; } ?></td>
                                </tr>
                                <?php   
                                $i++;
                                } 
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

    <script type="text/javascript">
      $(document).ready(function() {
          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });
      } );
  </script>