<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Surat Keluar</a></li>
                <li class="active">Data Surat</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Surat Keluar</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">REUPLOAD FILE SURAT</header>
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo base_url('keluar/reupload'); ?>">
                    <div class="panel-body">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">File Surat<font color="#FF0000 ">*</font></label>
                          <div class="col-sm-10">
                              <input type="file" name="fileupload" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s">
                              <input type="hidden" name="id_srt_keluar" value="<?php echo $content[0]->id_srt_keluar; ?>">
                              <input type="hidden" name="perihal" value="<?php echo $content[0]->perihal; ?>">
                              <input type="hidden" name="tgl_surat" value="<?php echo $content[0]->tgl_srt_keluar; ?>">
                          </div>
                        </div>   
                    </div>
                    <footer class="panel-footer text-right bg-light lter">
                        <button type="submit" name="submit" value="submit" class="btn btn-success btn-s-xs"><i class="fa fa-upload"></i> Reupload File</button>
                    </footer>
                    </form>
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>