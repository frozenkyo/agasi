<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Laporan Surat</a></li>
                <li class="active">Berdasar Pelaksana</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Laporan Berdasar Pelaksana</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <section class="panel panel-default">
                      <header class="panel-heading font-bold">
                          <a style="padding: 10px;" target="_blank" href="<?php echo base_url("cetak/lappelaksana/".$tahun); ?>" role="button" class="nav pull-right btn btn-info"><i class="fa fa-print"></i> Cetak Laporan</a>
                          LAPORAN SURAT TAHUN <?php echo $tahun; ?>
                      </header>
                        <div class="panel-body">
                            <div class="">
                                <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                    <thead align="center">
                                        <tr>
                                            <th valign="center" rowspan="3">No</th>
                                            <th valign="center" rowspan="3">Pelaksana</th>
                                            <th class="text-center" colspan="<?php echo sizeof($klasifikasi) * 3; ?>" >Jumlah Surat Masuk</th>
                                            <th class="text-center" rowspan="2" colspan="3" >Total Surat</th>
                                        </tr>
                                        <tr>
                                            <?php foreach($klasifikasi as $kls): ?>
                                            <th class="text-center" colspan="3" ><?php echo $kls->kode; ?></th>
                                            <?php endforeach; ?>
                                        </tr>
                                        <tr>
                                            <?php foreach($klasifikasi as $kls): ?>
                                            <th class="text-center">T</th>
                                            <th class="text-center">Se</th>
                                            <th class="text-center">Si</th>
                                            <?php endforeach; ?>
                                            <th class="text-center">T</th>
                                            <th class="text-center">Se</th>
                                            <th class="text-center">Si</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($staff as $key => $data): ?>
                                        <tr>
                                            <td class="text-center"><?php echo ($key + 1); ?></td>
                                            <td><?php echo $data->nama; ?></td>
                                            <?php foreach($data->kerja as $krj): ?>
                                            <td class="text-center"><?php echo $krj['terima']; ?></td>
                                            <td class="text-center"><?php echo $krj['selesai']; ?></td>
                                            <td class="text-center"><?php echo ($krj['terima'] - $krj['selesai']); ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                      <div style="padding: 30px; margin-top: -50px;">
                        <p><strong>Keterangan :</strong></p>
                        <?php foreach($klasifikasi as $kls): ?>
                        <p><?php echo $kls->kode .' = '. $kls->jenis; ?></p>
                        <?php endforeach; ?>
                        <p>T = Surat Diterima</p>
                        <p>Se = Surat Terselesaikan</p>
                        <p>Si = Surat Sisa</p>
                      </div>
                  </section>
                </div>
              </div>


            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

     <script type="text/javascript">


      $(document).ready(function() {
          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": [],
              "scrollX": true,
          });


      } );
  </script>