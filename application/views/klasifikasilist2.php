<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Laporan Surat</a></li>
                <li class="active">Berdasar Klasifikasi</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Laporan Berdasar Klasifikasi</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <section class="panel panel-default">
                      <header class="panel-heading font-bold">
                          <a style="padding: 10px;" target="_blank" href="<?php echo site_url("cetak/lapklas/".$tahun); ?>" role="button" class="nav pull-right btn btn-info"><i class="fa fa-print"></i> Cetak Laporan</a>
                          LAPORAN SURAT TAHUN <?php echo $tahun; ?>
                      </header>
                        <div class="panel-body">
                            <div class="">
                                <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                    <thead align="center">
                                        <tr>
                                            <th valign="center" rowspan="2">No</th>
                                            <th valign="center" rowspan="2">Jenis Agenda Surat</th>
                                            <th class="text-center" colspan="4" >Jumlah</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center">Belum Terselesaikan (tahun lalu)</th>
                                            <th class="text-center">Total Surat Masuk</th>
                                            <th class="text-center">Surat Terselesaikan</th>
                                            <th class="text-center">Tunggakan belum diselesaikan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Delegasi Perdata (Del.Pdt)</td>
                                            <td class="text-center"><?php echo $pdtsisa[0]->totagenda; ?></td>
                                            <td class="text-center"><?php echo $pdtsisa[0]->totagenda+$pdtmsk[0]->totmasuk; ?></td>
                                            <td class="text-center"><?php echo $pdtsls[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $pdtsisa[0]->totagenda+$pdttgk[0]->tottunggakan; ?></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Delegasi Pidana (Del.Pid)</td>
                                            <td class="text-center"><?php echo $pidsisa[0]->totagenda; ?></td>
                                            <td class="text-center"><?php echo $pidsisa[0]->totagenda+$pidmsk[0]->totmasuk; ?></td>
                                            <td class="text-center"><?php echo $pidsls[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $pidsisa[0]->totagenda+$pidtgk[0]->tottunggakan; ?></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Permintaan Biaya (PMB)</td>
                                            <td class="text-center"><?php echo $pmbsisa[0]->totagenda; ?></td>
                                            <td class="text-center"><?php echo $pmbsisa[0]->totagenda+$pmbmsk[0]->totmasuk; ?></td>
                                            <td class="text-center"><?php echo $pmbsls[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $pmbsisa[0]->totagenda+$pmbtgk[0]->tottunggakan; ?></td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Surat Umum (A)</td>
                                            <td class="text-center"><?php echo $asisa[0]->totagenda; ?></td>
                                            <td class="text-center"><?php echo $asisa[0]->totagenda+$amsk[0]->totmasuk; ?></td>
                                            <td class="text-center"><?php echo $asls[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $asisa[0]->totagenda+$atgk[0]->tottunggakan; ?></td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Berkas Perkara (BB)</td>
                                            <td class="text-center"><?php echo $bbsisa[0]->totagenda; ?></td>
                                            <td class="text-center"><?php echo $bbsisa[0]->totagenda+$bbmsk[0]->totmasuk; ?></td>
                                            <td class="text-center"><?php echo $bbsls[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $bbsisa[0]->totagenda+$bbtgk[0]->tottunggakan; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                  </section>
                </div>
              </div>


            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

     <script type="text/javascript">


      $(document).ready(function() {
          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });


      } );
  </script>