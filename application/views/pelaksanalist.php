<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Laporan Surat</a></li>
                <li class="active">Berdasar Pelaksana</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Laporan Berdasar Pelaksana</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <section class="panel panel-default">
                      <header class="panel-heading font-bold">
                          <a style="padding: 10px;" target="_blank" href="<?php echo base_url("cetak/lappelaksana/".$tahun); ?>" role="button" class="nav pull-right btn btn-info"><i class="fa fa-print"></i> Cetak Laporan</a>
                          LAPORAN SURAT TAHUN <?php echo $tahun; ?>
                      </header>
                        <div class="panel-body">
                            <div class="">
                                <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                    <thead align="center">
                                        <tr>
                                            <th valign="center" rowspan="3">No</th>
                                            <th valign="center" rowspan="3">Pelaksana</th>
                                            <th class="text-center" colspan="15" >Jumlah Surat Masuk</th>                                   
                                            <th class="text-center" rowspan="2" colspan="3" >Total Surat</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center" colspan="3" >Del.Pid</th>
                                            <th class="text-center" colspan="3" >Del.Pdt</th>
                                            <th class="text-center" colspan="3" >PMB</th>
                                            <th class="text-center" colspan="3" >A</th>
                                            <th class="text-center" colspan="3" >Pemberkasan</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center">T</th>
                                            <th class="text-center">Se</th>
                                            <th class="text-center">Si</th>
                                            <th class="text-center">T</th>
                                            <th class="text-center">Se</th>
                                            <th class="text-center">Si</th>
                                            <th class="text-center">T</th>
                                            <th class="text-center">Se</th>
                                            <th class="text-center">Si</th>
                                            <th class="text-center">T</th>
                                            <th class="text-center">Se</th>
                                            <th class="text-center">Si</th>
                                            <th class="text-center">T</th>
                                            <th class="text-center">Se</th>
                                            <th class="text-center">Si</th>
                                            <th class="text-center">T</th>
                                            <th class="text-center">Se</th>
                                            <th class="text-center">Si</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no=1;
                                        foreach($staff as $data) { ?>
                                        <tr>
                                            <td class="text-center"><?php echo $no; ?></td>
                                            <td><?php echo $data->nama; ?></td>
                                            <td class="text-center"><?php $tes=$this->laporansess->cekTerima($tahun,'1',$data->id_pegawai); echo $tes[0]->totterima; ?></td>
                                            <td class="text-center"><?php $test=$this->laporansess->cekSelesai($tahun,'1',$data->id_pegawai); echo $test[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $tes[0]->totterima-$test[0]->totselesai;?></td>
                                            <td class="text-center"><?php $tes2=$this->laporansess->cekTerima($tahun,'2',$data->id_pegawai); echo $tes2[0]->totterima; ?></td>
                                            <td class="text-center"><?php $test2=$this->laporansess->cekSelesai($tahun,'2',$data->id_pegawai); echo $test2[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $tes2[0]->totterima-$test2[0]->totselesai;?></td>
                                            <td class="text-center"><?php $tes3=$this->laporansess->cekTerima($tahun,'3',$data->id_pegawai); echo $tes3[0]->totterima; ?></td>
                                            <td class="text-center"><?php $test3=$this->laporansess->cekSelesai($tahun,'3',$data->id_pegawai); echo $test3[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $tes3[0]->totterima-$test3[0]->totselesai;?></td>
                                            <td class="text-center"><?php $tes4=$this->laporansess->cekTerima($tahun,'4',$data->id_pegawai); echo $tes4[0]->totterima; ?></td>
                                            <td class="text-center"><?php $test4=$this->laporansess->cekSelesai($tahun,'4',$data->id_pegawai); echo $test4[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $tes4[0]->totterima-$test4[0]->totselesai;?></td>
                                            <td class="text-center"><?php $tes5=$this->laporansess->cekTerima($tahun,'5',$data->id_pegawai); echo $tes5[0]->totterima; ?></td>
                                            <td class="text-center"><?php $test5=$this->laporansess->cekSelesai($tahun,'5',$data->id_pegawai); echo $test5[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $tes5[0]->totterima-$test5[0]->totselesai;?></td>
                                            <td class="text-center"><?php $tes6=$this->laporansess->cekTotalTerima($tahun,$data->id_pegawai); echo $tes6[0]->totterima; ?></td>
                                            <td class="text-center"><?php $test6=$this->laporansess->cekTotalSelesai($tahun,$data->id_pegawai); echo $test6[0]->totselesai; ?></td>
                                            <td class="text-center"><?php echo $tes6[0]->totterima-$test6[0]->totselesai;?></td>
                                        </tr>
                                        <?php 
                                        $no++;
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                      <div style="padding: 30px; margin-top: -50px;">
                        <p><strong>Keterangan :</strong></p>
                        <p>Del Pdt = Delegasi Perdata</p>
                        <p>Del Pid = Delegasi Pidana</p>
                        <p>PMB = Permintaan Biaya</p>
                        <p>A = Surat Umum</p>
                        <p>Pemberkasan = Berkas Perkara</p>
                        <p>T = Surat Diterima</p>
                        <p>Se = Surat Terselesaikan</p>
                        <p>Si = Surat Sisa</p>
                      </div>
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

     <script type="text/javascript">
      

      $(document).ready(function() { 
          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });

         
      } );
  </script>