<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Dashboard</a></li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Dashboard</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">REVIEW SURAT</header>
                    <form class="form-horizontal" method="post" action="<?php echo base_url('dashboard/review'); ?>">
                    <div class="panel-body">
                    <input type="hidden" name="disposisi" value="<?php echo $referensi; ?>">
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Nomor Agenda</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->no_agenda; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Tanggal Penerimaan</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo date("d M Y g:i A", strtotime($disposisi[0]->tgl_penerimaan)); ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Klasifikasi Surat</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php if($disposisi[0]->id_klasifikasi=="1"){ echo "Pidana"; }else if($disposisi[0]->id_klasifikasi=="2"){ echo "Perdata"; }else if($disposisi[0]->id_klasifikasi=="3"){ echo "Juru Sita"; }else if($disposisi[0]->id_klasifikasi=="4"){ echo "Berkas Perkara"; }else if($disposisi[0]->id_klasifikasi=="5"){ echo "Umum"; } ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Tanggal Surat</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo tgl_indo2($disposisi[0]->tgl_srt_masuk); ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Nomor Surat</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->no_srt_masuk; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Perihal</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->hal_surat; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Tujuan Surat</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->tujuan; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Nama Pengirim</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->nama_pengirim; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Alamat Pengirim</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->alamat_pengirim; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Ringkasan Isi</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->isi_ringkas; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Tembusan</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->tembusan; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Salinan</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->salinan; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Lampiran</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->lampiran; ?></p>
                        </div>
                      </div>
                      <div class="form-group pull-in clearfix">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $disposisi[0]->keterangan; ?></p>
                        </div>
                      </div>
                    </div>
                    <footer class="panel-footer text-right bg-light lter">
                        <?php if(($disp[0]->status=="5")&&($hakakses=="22")){ ?>
                        <a href="<?php echo base_url('cetak/'.$disp[0]->id_disposisi) ?>" target="_blank" class="btn btn-info" role="button"><i class="fa fa-print text"></i> Cetak Disposisi</a>
                        <?php } ?>
                        <a href="<?php echo base_url('keluar/newmail/'.$disp[0]->id_disposisi) ?>" class="btn btn-warning" role="button"><i class="fa fa-cloud-upload text"></i> Buat Surat Keluar</a>
                        <a role="button" href="#myModal" data-toggle="modal" title="Tidak dikerjakan" class="btn btn-danger btn-s-xs"><i class="fa fa-times"></i> Tidak Dikerjakan</a>
                        <button type="submit" name="submit" value="submit" class="btn btn-success btn-s-xs"><i class="fa fa-check"></i> Selesai Dikerjakan</button>
                    </footer>
                    </form>
                  </section>
                </div>
              </div>
              <form action="<?php echo base_url('dashboard/reject') ?>" method="post">
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Perhatian</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Alasan tidak dikerjakan </label>
                                    <textarea name="note" class="form-control" rows="6" data-minwords="6" data-required="true" required="required"></textarea>
                                    <input type="hidden" name="disposisi" value="<?php echo $disp[0]->id_disposisi; ?>">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" type="submit" name="submit" value="submit"><i class="fa fa-save text"></i> Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->
                </form>

            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>