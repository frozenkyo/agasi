<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Surat Masuk</a></li>
                <li class="active">Tulis Surat</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Surat Masuk</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <?php if($message=="fail"){ ?>
                    <!-- Error Message -->
                    <div class="alert fade in alert-danger fail" id="fail">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Terjadi kesalahan.
                    </div>   
                    <?php }else if($message=="duplicate"){ ?>
                    <div class="alert fade in alert-danger invalid" id="invalid">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data yang anda masukkan sudah ada. Periksa kembali inputan anda.
                    </div>   
                    <?php }else if($message=="success"){ ?>
                    <div class="alert fade in alert-success reg" id="reg">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data berhasil ditambahkan
                    </div>
                    <?php }else if($message=="error"){ ?>
                    <div class="alert fade in alert-danger" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Terjadi kesalahan pada inputan anda
                    </div>
                    <?php }else if($message=="errorupload"){ ?>
                    <div class="alert fade in alert-warning" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data berhasil ditambahkan tetapi dokumen file belum anda upload
                    </div>
                    <?php } ?>
                    <section class="panel panel-default">
                       <div class="wizard clearfix" id="form-wizard">
                        <ul class="steps">
                          <li data-target="#step1" class="active"><span class="badge badge-info">1</span>Input Data Surat</li>
                          <li data-target="#step2"><span class="badge">2</span>Disposisi Surat</li>
                          <li data-target="#step3"><span class="badge">3</span>Review Data Surat</li>
                        </ul>
                      </div>
                      <div class="step-content">
                        <form method="post" enctype="multipart/form-data" action="<?php echo base_url("masuk/newmail"); ?>">
                          <div class="step-pane active" id="step1">
                            <div class="form-group pull-in clearfix">
                            <div class="col-sm-3">
                            <p>Tanggal Penerimaan</p>
                            <input type="text" class="form-control" value="<?php echo tgl_indo2(date("Y-m-d")); ?>" disabled="disabled">
                            <input name="tgl_terima" type="hidden" class="form-control" value="<?php echo date("Y-m-d H:i:s"); ?>">
                            </div>   
                            </div>
                            <div class="form-group ">
                            <p class="m-t">Klasifikasi Surat<font color="#FF0000 ">*</font></p>
                            <select name="klasifikasi" id="select2-option3" style="width:260px" data-required="true" required="required">
                                    <option value=""></option>
                                    <?php foreach ($klasifikasi as $klas){ ?>
                                    <option value="<?php echo $klas->id_klasifikasi; ?>"><?php echo $klas->kode; ?></option>
                                    <?php } ?>
                            </select>
                            </div> 
                            <div class="form-group pull-in clearfix">
                            <div class="col-sm-3">
                            <p>Tanggal Surat<font color="#FF0000 ">*</font></p>
                            <input class="datepicker-input form-control" size="16" type="text" name="tgl_surat" data-required="true" data-date-format="yyyy-mm-dd" required="required">
                            </div>
                            </div>
                            <div class="form-group">
                            <p class="m-t">Nomor Surat<font color="#FF0000 ">*</font></p>
                            <input name="no_surat" type="text" class="form-control" data-trigger="change" data-required="true" required="required">
                            </div>
                            <div class="form-group">
                            <p class="m-t">Perihal<font color="#FF0000 ">*</font></p>
                            <input name="perihal" type="text" class="form-control" data-trigger="change" data-required="true" required="required" >
                            </div>
                            <div class="form-group">
                            <p class="m-t">Tujuan<font color="#FF0000 ">*</font></p>
                            <input name="tujuan" type="text" class="form-control" data-trigger="change" data-required="true" required="required" >
                            </div>
                            <div class="form-group">
                            <p class="m-t">Nama Pengirim<font color="#FF0000 ">*</font></p>
                            <input name="nama" type="text" class="form-control" data-trigger="change" data-required="true" required="required" >
                            </div>
                            <div class="form-group">
                            <p class="m-t">Alamat Pengirim<font color="#FF0000 ">*</font></p>
                            <textarea name="alamat" class="form-control" rows="6" data-minwords="6" data-required="true" required="required"></textarea>
                            </div>
                            <div class="form-group">
                            <p class="m-t">Ringkasan Isi<font color="#FF0000 ">*</font></p>
                            <textarea name="ringkasan" class="form-control" rows="6" maxlength="150" data-minwords="6" data-required="true" required="required"></textarea>
                            </div>
                            <div class="form-group">
                            <p class="m-t">Tembusan</p>
                            <input name="tembusan" type="text" class="form-control" data-trigger="change" >
                            </div>
                            <div class="form-group">
                            <p class="m-t">Salinan</p>
                            <input name="salinan" type="text" class="form-control" data-trigger="change" >
                            </div>
                            <div class="form-group">
                            <p class="m-t">Lampiran</p>
                            <input name="lampiran" type="text" class="form-control" data-trigger="change" >
                            </div>
                            <div class="form-group">
                            <p class="m-t">Keterangan</p>
                            <textarea name="keterangan" class="form-control" rows="6"></textarea>
                            </div>
                            <div class="form-group">
                            <p class="m-t">Upload Berkas</p>
                            <input type="file" name="fileupload" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s">
                            </div>
                            <input name="noagenda" type="hidden" value="<?php echo $nextagenda; ?>">
                            <input name="yearnow" type="hidden" value="<?php echo $yearnow; ?>">
                          </div>
                          <div class="step-pane" id="step2">
                                <div class="form-group">
                                <p class="m-t">Tujuan Disposisi<font color="#FF0000 ">*</font></p>
                                    <select name="tujuandisp" id="select2-option" style="width:260px" data-required="true" required="required">
                                            <option value=""></option>
                                            <option value="1">Ketua</option>
                                            <option value="2"> Wakil Ketua</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                <p class="m-t">Tingkat Keamanan<font color="#FF0000 ">*</font></p>
                                    <select name="keamanan" id="select2-option2" style="width:260px" data-required="true" required="required">
                                            <option value=""></option>
                                            <option value="Sangat Rahasia">Sangat Rahasia</option>
                                            <option value="Rahasia">Rahasia</option>
                                            <option value="Biasa">Biasa</option>
                                    </select>
                                </div>
                          </div>
                          <div class="step-pane" id="step3">
                              <div class="form-group pull-in clearfix">
                                <div class="col-sm-10">
                                  <p id="header" class="form-control-static"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Nomor Agenda</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="agenda"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tanggal Penerimaan</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="terima"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Klasifikasi Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="klasifikasi"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tanggal Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="surat"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Nomor Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="nomor"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Perihal</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="perihal"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tujuan Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="tujuan"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Nama Pengirim</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="nama"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Alamat Pengirim</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="alamat"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Ringkasan Isi</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="ringkasan"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tembusan</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="tembusan"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Salinan</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="salinan"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Lampiran</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="lampiran"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Keterangan</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="keterangan"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Upload Berkas</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="upload"></p>
                                </div>
                              </div>
                          </div>                
                        <div class="actions m-t" align="right">
                          <button type="button" class="btn btn-info btn-sm btn-prev" data-target="#form-wizard" data-wizard="previous" disabled="disabled">Prev</button>
                          <button type="button" class="btn btn-info btn-sm btn-next" data-target="#form-wizard" data-wizard="next">Next</button> 
                          <button type="submit" name="submit" value="submit" class="btn btn-success btn-sm btn-finish" data-target="#form-wizard" data-wizard="finish" >Finish</button>
                        </div> 
                        </form>
                      </div>
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>
    
    <script>
    $(document).ready(function() {
        var tujuandisp = "";
        var keamanan = "";
        var a = $( "input[name=tgl_terima]" ).val();
        $('#terima').text(a);
        
        $( "input[name=perihal]" ).change(function(){
            var b = $( "input[name=perihal]" ).val();
            $('#perihal').text(b);  
        }); 
        $( "input[name=tujuan]" ).change(function(){
            var c = $( "input[name=tujuan]" ).val();
            $('#tujuan').text(c);
        });
        $( "input[name=nama]" ).change(function(){
            var d = $( "input[name=nama]" ).val();
            $('#nama').text(d);
        });
        $( "textarea[name=alamat]" ).change(function(){
            var e = $( "textarea[name=alamat]" ).val();
            $('#alamat').text(e);
        });
        $( "textarea[name=ringkasan]" ).change(function(){
            var f = $( "textarea[name=ringkasan]" ).val();
            $('#ringkasan').text(f);
        });
        $( "input[name=tembusan]" ).change(function(){
            var g = $( "input[name=tembusan]" ).val();
            $('#tembusan').text(g);
        });
        $( "input[name=salinan]" ).change(function(){
            var h = $( "input[name=salinan]" ).val();
            $('#salinan').text(h);
        });
        $( "input[name=fileupload]" ).change(function(){
            var i = $( "input[name=fileupload]" ).val();
            $('#upload').text(i);
        });
        
        $( "input[name=no_surat]" ).change(function(){
            var j = $( "input[name=no_surat]" ).val();
            $('#nomor').text(j);
        });
        
        $( "input[name=lampiran]" ).change(function(){
            var k = $( "input[name=lampiran]" ).val();
            $('#lampiran').text(k);
        });
        
        $( "textarea[name=keterangan]" ).change(function(){
            var l = $( "textarea[name=keterangan]" ).val();
            $('#keterangan').text(l);
        });
       
    });
    </script>