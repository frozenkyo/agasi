<!DOCTYPE html>
<html lang="en">
<head>
 <title>Cetak Laporan</title>
</head>
<body>
    <table style="width:100%;border-bottom: none; border-top: none;" align="center">
        <tr>
            <td style="padding-top: 10px;padding-bottom: 5px;" colspan="3" align="center"><p style="font-size:20px"><strong>LAPORAN SURAT MASUK BERDASAR PENGAGENDAAN</strong></p></td>
        </tr>
    </table>
    <table style="width:100%;border-bottom: none; border-top: none;" align="center">
        <tr>
            <td style="padding-top: 5px;padding-bottom: 30px;" colspan="3" align="center"><p style="font-size:17px">(Dari bulan Januari <?php echo $tahun; ?> sampai dengan tanggal <?php $date = DateTime::createFromFormat("Y-m-d H:i:s",$maks[0]->maksimal); echo tgl_indo2($date->format("Y-m-d")) ?> ) </p></td>
        </tr>
    </table>
    <table cellpadding="10" cellspacing="0" border="1" class="display table table-striped table-bordered" id="hidden-table-info">
        <thead align="center">
            <tr>
                <th class="text-center" rowspan="2">No</th>
                <th class="text-center" rowspan="2">Jenis Agenda Surat</th>
                <th class="text-center" colspan="4" >Jumlah</th>
            </tr>
            <tr>
                <th class="text-center">Belum Terselesaikan (tahun lalu)</th>
                <th class="text-center">Total Surat Masuk</th>
                <th class="text-center">Surat Terselesaikan</th>
                <th class="text-center">Tunggakan belum diselesaikan</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($klasifikasi as $key => $value): ?>
            <tr>
                <td><?php echo ($key + 1); ?></td>
                <td><?php echo $value->jenis . " (" . $value->kode . ")" ?></td>
                <td align="center" class="text-center"><?php echo $value->sisa; ?></td>
                <td align="center" class="text-center"><?php echo $value->masuk ?></td>
                <td align="center" class="text-center"><?php echo $value->selesai ?></td>
                <td align="center" class="text-center"><?php echo ($value->sisa + $value->tunggakan); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>