<!DOCTYPE html>
<html lang="en">
<head>
 <title>Cetak Laporan</title>
</head>
<body>
    <table style="width:100%;border-bottom: none; border-top: none;" align="center">
        <tr>
            <td style="padding-top: 10px;padding-bottom: 5px;" colspan="3" align="center"><p style="font-size:20px"><strong>LAPORAN SURAT MASUK BERDASAR PELAKSANA SURAT</strong></p></td>
        </tr>
    </table>
    <table style="width:100%;border-bottom: none; border-top: none;" align="center">
        <tr>
            <td style="padding-top: 5px;padding-bottom: 30px;" colspan="3" align="center"><p style="font-size:17px">(Dari bulan Januari <?php echo $tahun; ?> sampai dengan tanggal <?php $date = DateTime::createFromFormat("Y-m-d H:i:s",$maks[0]->maksimal); echo tgl_indo2($date->format("Y-m-d")); ?> ) </p></td>
        </tr>
    </table>
    <table style="width: 100%" cellpadding="10" cellspacing="0" border="1" class="display table table-striped table-bordered" id="hidden-table-info">
        <thead align="center">
            <tr>
                <th class="text-center" rowspan="3">No</th>
                <th class="text-center" rowspan="3">Pelaksana</th>
                <th class="text-center" colspan="15" >Jumlah Surat Masuk</th>                                   
                <th class="text-center" rowspan="2" colspan="3" >Total Surat</th>
            </tr>
            <tr>
                <th class="text-center" colspan="3" >Delegasi Pidana (Del.Pid)</th>
                <th class="text-center" colspan="3" >Delegasi Perdata (Del.Pdt)</th>
                <th class="text-center" colspan="3" >Permintaan Biaya (PMB)</th>
                <th class="text-center" colspan="3" >Surat Umum (A)</th>
                <th class="text-center" colspan="3" >Berkas Perkara (Pemberkasan)</th>
            </tr>
            <tr>
                <th class="text-center">Terima</th>
                <th class="text-center">Selesai</th>
                <th class="text-center">Sisa</th>
                <th class="text-center">Terima</th>
                <th class="text-center">Selesai</th>
                <th class="text-center">Sisa</th>
                <th class="text-center">Terima</th>
                <th class="text-center">Selesai</th>
                <th class="text-center">Sisa</th>
                <th class="text-center">Terima</th>
                <th class="text-center">Selesai</th>
                <th class="text-center">Sisa</th>
                <th class="text-center">Terima</th>
                <th class="text-center">Selesai</th>
                <th class="text-center">Sisa</th>
                <th class="text-center">Terima</th>
                <th class="text-center">Selesai</th>
                <th class="text-center">Sisa</th>
            </tr>
        </thead>
        <tbody>   
            <?php 
            $no=1;
            foreach($staff as $data) { ?>
            <tr>
                <td class="text-center"><?php echo $no; ?></td>
                <td><?php echo $data->nama; ?></td>
                <td align="center" class="text-center"><?php $tes=$this->laporansess->cekTerima($tahun,'1',$data->id_pegawai); echo $tes[0]->totterima; ?></td>
                <td align="center" class="text-center"><?php $test=$this->laporansess->cekSelesai($tahun,'1',$data->id_pegawai); echo $test[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $tes[0]->totterima-$test[0]->totselesai;?></td>
                <td align="center" class="text-center"><?php $tes2=$this->laporansess->cekTerima($tahun,'2',$data->id_pegawai); echo $tes2[0]->totterima; ?></td>
                <td align="center" class="text-center"><?php $test2=$this->laporansess->cekSelesai($tahun,'2',$data->id_pegawai); echo $test2[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $tes2[0]->totterima-$test2[0]->totselesai;?></td>
                <td align="center" class="text-center"><?php $tes3=$this->laporansess->cekTerima($tahun,'3',$data->id_pegawai); echo $tes3[0]->totterima; ?></td>
                <td align="center" class="text-center"><?php $test3=$this->laporansess->cekSelesai($tahun,'3',$data->id_pegawai); echo $test3[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $tes3[0]->totterima-$test3[0]->totselesai;?></td>
                <td align="center" class="text-center"><?php $tes4=$this->laporansess->cekTerima($tahun,'4',$data->id_pegawai); echo $tes4[0]->totterima; ?></td>
                <td align="center" class="text-center"><?php $test4=$this->laporansess->cekSelesai($tahun,'4',$data->id_pegawai); echo $test4[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $tes4[0]->totterima-$test4[0]->totselesai;?></td>
                <td align="center" class="text-center"><?php $tes5=$this->laporansess->cekTerima($tahun,'5',$data->id_pegawai); echo $tes5[0]->totterima; ?></td>
                <td align="center" class="text-center"><?php $test5=$this->laporansess->cekSelesai($tahun,'5',$data->id_pegawai); echo $test5[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $tes5[0]->totterima-$test5[0]->totselesai;?></td>
                <td align="center" class="text-center"><?php $tes6=$this->laporansess->cekTotalTerima($tahun,$data->id_pegawai); echo $tes6[0]->totterima; ?></td>
                <td align="center" class="text-center"><?php $test6=$this->laporansess->cekTotalSelesai($tahun,$data->id_pegawai); echo $test6[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $tes6[0]->totterima-$test6[0]->totselesai;?></td>
            </tr>
            <?php 
            $no++;
            } ?>
        </tbody>
    </table>
</body>
</html>