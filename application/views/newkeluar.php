<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Surat Keluar</a></li>
                <li class="active">Tulis Surat</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Surat Keluar</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <?php if($message=="fail"){ ?>
                    <!-- Error Message -->
                    <div class="alert fade in alert-danger fail" id="fail">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Terjadi kesalahan.
                    </div>   
                    <?php }else if($message=="duplicate"){ ?>
                    <div class="alert fade in alert-danger invalid" id="invalid">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data yang anda masukkan sudah ada. Periksa kembali inputan anda.
                    </div>   
                    <?php }else if($message=="success"){ ?>
                    <div class="alert fade in alert-success reg" id="reg">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data berhasil ditambahkan
                    </div>
                    <?php }else if($message=="error"){ ?>
                    <div class="alert fade in alert-danger" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Terjadi kesalahan pada inputan anda
                    </div>
                    <?php }else if($message=="errorupload"){ ?>
                    <div class="alert fade in alert-warning" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data berhasil ditambahkan tetapi dokumen file belum anda upload
                    </div>
                    <?php } ?>
                    <section class="panel panel-default">
                       <div class="wizard clearfix" id="form-wizard">
                        <ul class="steps">
                          <li data-target="#step1" class="active"><span class="badge badge-info">1</span>Input Data Surat</li>
                          <li data-target="#step2"><span class="badge">2</span>Referensi Surat Masuk</li>
                          <li data-target="#step3"><span class="badge">3</span>Review Data Surat</li>
                        </ul>
                      </div>
                      <div class="step-content">
                        <?php if($referensi!=""){ ?>  
                        <div class="alert alert-info">
                            <i class="fa fa-info-sign"></i>Isi data yang anda inputkan pada kolom ini mengacu pada surat masuk <strong><?php echo $datareff[0]->no_srt_masuk."-".$datareff[0]->hal_surat; ?></strong>.
                        </div>
                        <?php } ?>
                        <form method="post" enctype="multipart/form-data" action="<?php echo base_url("keluar/newmail"); ?>">
                          <div class="step-pane active" id="step1">
                            <div class="form-group pull-in clearfix">
                            <div class="col-sm-3">
                            <p>Tanggal Surat<font color="#FF0000 ">*</font></p>
                            <input class="datepicker-input2 form-control" size="16" type="text" name="tgl_surat" data-required="true" data-date-format="yyyy-mm-dd" required="required">
                            <input type="hidden" name="reff" value="<?php echo $referensi; ?>">
                            </div>
                            </div>
                            <div class="form-group">
                            <p class="m-t">Nomor Surat<font color="#FF0000 ">*</font></p>
                            </div>
                            <div class="form-group" style="margin-top: -5px;">
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="surat1" data-trigger="change" data-required="true">
                                    <input type="hidden" name="agenda" value="<?php echo $next; ?>">
                                    <span class="help-block m-b-none"><i>Kode Wilayah PN</i></span>
                                </div>
                                <div class="col-md-1">
                                    <input type="text" class="form-control" value="/" disabled="disabled">
                                </div>
                                <div class="col-md-2">
                                  <input type="text" class="form-control" name="surat2" data-trigger="change" data-required="true">
                                  <span class="help-block m-b-none"><i>Klasifikasi Surat</i></span>
                                </div>
                                <div class="col-md-1">
                                    <input type="text" class="form-control" value="/" disabled="disabled">
                                </div>
                                <div class="col-md-2">
                                  <input type="text" class="form-control" name="surat3" data-trigger="change" data-required="true">
                                  <span class="help-block m-b-none"><i>Bulan</i></span>
                                </div>
                                <div class="col-md-1">
                                    <input type="text" class="form-control" value="/" disabled="disabled">
                                </div>
                                <div class="col-md-2">
                                  <input type="text" class="form-control" name="surat4" data-trigger="change" data-required="true">
                                  <span class="help-block m-b-none"><i>Tahun</i></span> 
                                </div>
                            </div>
                            </div>  
                            <div class="form-group">
                            <p class="m-t">Perihal<font color="#FF0000 ">*</font></p>
                            <input name="perihal" type="text" class="form-control" data-trigger="change" data-required="true" required="required" >
                            </div>
                            <div class="form-group">
                            <p class="m-t">Tujuan Surat<font color="#FF0000 ">*</font></p>
                            <input name="tujuan" type="text" class="form-control" data-trigger="change" data-required="true" required="required" >
                            </div>
                            <div class="form-group">
                            <p class="m-t">Lampiran</p>
                            <input name="lampiran" type="text" class="form-control" data-trigger="change" >
                            </div>
                            <div class="form-group">
                            <p class="m-t">Keterangan</p>
                            <textarea name="keterangan" class="form-control" rows="6"></textarea>
                            </div>
                            <div class="form-group">
                            <p class="m-t">Upload Berkas</p>
                            <input type="file" name="fileupload" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s">
                            </div>
                          </div>
                          <div class="step-pane" id="step2">
                                <?php if($referensi==""){ ?>
                                <div class="form-group">
                                <p class="m-t">Memiliki referensi dengan surat masuk ?<font color="#FF0000 ">*</font></p>
                                <div class="radio">
                                <label class="radio-custom">
                                      <input type="radio" name="radio" value="ya" data-required="true">
                                      <i class="fa fa-circle-o"></i>
                                      Ya
                                </label>
                                </div>
                                <div class="radio">
                                <label class="radio-custom">
                                      <input type="radio" name="radio" value="tidak">
                                      <i class="fa fa-circle-o"></i>
                                      Tidak
                                </label>
                                </div>
                                </div>
                                <?php } ?>
                                
                                <div class="form-group" id="ref" style="display: none;">
                                <p class="m-t">Referensi Surat<font color="#FF0000 ">*</font></p>
                                    <?php if($referensi==""){ ?>
                                    <select name="referensi" id="select2-option4" style="width:560px">
                                            <option value=""></option>
                                            <?php foreach ($suratmasuk as $masuk){ ?>
                                            <option value="<?php echo $masuk->id_agenda; ?>"><?php echo $masuk->no_srt_masuk."-".$masuk->hal_surat; ?></option>
                                            <?php }?>
                                    </select>
                                    <?php }else{ ?>
                                    <select name="referensi" id="select2-option4" style="width:560px" >
                                            <option value=""></option>
                                            <option selected="selected" value="<?php echo $datareff[0]->id_agenda; ?>"><?php echo $datareff[0]->no_srt_masuk."-".$datareff[0]->hal_surat; ?></option>
                                    </select>
                                    <?php } ?>
                                </div>
                          </div>
                          <div class="step-pane" id="step3">
                              <div class="form-group pull-in clearfix">
                                <div class="col-sm-10">
                                  <p id="header" class="form-control-static"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tanggal Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="tgl_surat"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Nomor Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="no_surat"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Perihal</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="perihal"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tujuan Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="tujuan"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Lampiran</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="lampiran"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Keterangan</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="keterangan"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Upload Berkas</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="upload"></p>
                                </div>
                              </div>
                          </div>                
                        <div class="actions m-t" align="right">
                          <button type="button" class="btn btn-info btn-sm btn-prev" data-target="#form-wizard" data-wizard="previous" disabled="disabled">Prev</button>
                          <button type="button" class="btn btn-info btn-sm btn-next" data-target="#form-wizard" data-wizard="next">Next</button> 
                          <button type="submit" name="submit" value="submit" class="btn btn-success btn-sm btn-finish" data-target="#form-wizard" data-wizard="finish" >Finish</button>
                        </div> 
                        </form>
                      </div>
                  </section>
                </div>
              </div>             
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>
    <script>
    $(document).ready(function() {
        
        $('input[name="radio"]').on('change', function() {
            var value = $(this).val();
//                alert(value);
            if(value=="ya"){
                $('#ref').show();
                $('#header').show();
                $('select[name="referensi"]').attr('data-required', true);
            }else{
                $('#ref').hide();
                $('#header').hide();
                $('select[name="referensi"]').removeAttr('data-required');
            }
        });
        var b ="";
        var d ="";
        var e ="";
        var f ="";
        var c = $( "input[name=agenda]" ).val();
        
        $( "input[name=surat1]" ).change(function(){
            b = $( "input[name=surat1]" ).val();
            $('#no_surat').text(b+"/"+c+"/"+d+"/"+e+"/"+f);
        }); 
        $( "input[name=surat2]" ).change(function(){
            d = $( "input[name=surat2]" ).val();
            $('#no_surat').text(b+"/"+c+"/"+d+"/"+e+"/"+f);
        });
        $( "input[name=surat3]" ).change(function(){
            e = $( "input[name=surat3]" ).val();
            $('#no_surat').text(b+"/"+c+"/"+d+"/"+e+"/"+f);
        });
        $( "input[name=surat4]" ).change(function(){
            f = $( "input[name=surat4]" ).val();
            $('#no_surat').text(b+"/"+c+"/"+d+"/"+e+"/"+f);
        });
        
        
        $( "input[name=perihal]" ).change(function(){
            var g = $( "input[name=perihal]" ).val();
            $('#perihal').text(g);
        });
        $( "input[name=tujuan]" ).change(function(){
            var h = $( "input[name=tujuan]" ).val();
            $('#tujuan').text(h);
        });
        $( "input[name=lampiran]" ).change(function(){
            var k = $( "input[name=lampiran]" ).val();
            $('#lampiran').text(k);
        });
        $( "textarea[name=keterangan]" ).change(function(){
            var l = $( "textarea[name=keterangan]" ).val();
            $('#keterangan').text(l);
        });
        $( "input[name=fileupload]" ).change(function(){
            var i = $( "input[name=fileupload]" ).val();
            $('#upload').text(i);
        });
      
    });
    </script>