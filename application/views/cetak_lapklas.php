<!DOCTYPE html>
<html lang="en">
<head>
 <title>Cetak Laporan</title>
</head>
<body>
    <table style="width:100%;border-bottom: none; border-top: none;" align="center">
        <tr>
            <td style="padding-top: 10px;padding-bottom: 5px;" colspan="3" align="center"><p style="font-size:20px"><strong>LAPORAN SURAT MASUK BERDASAR PENGAGENDAAN</strong></p></td>
        </tr>
    </table>
    <table style="width:100%;border-bottom: none; border-top: none;" align="center">
        <tr>
            <td style="padding-top: 5px;padding-bottom: 30px;" colspan="3" align="center"><p style="font-size:17px">(Dari bulan Januari <?php echo $tahun; ?> sampai dengan tanggal <?php $date = DateTime::createFromFormat("Y-m-d H:i:s",$maks[0]->maksimal); echo tgl_indo2($date->format("Y-m-d")) ?> ) </p></td>
        </tr>
    </table>
    <table cellpadding="10" cellspacing="0" border="1" class="display table table-striped table-bordered" id="hidden-table-info">
        <thead align="center">
            <tr>
                <th class="text-center" rowspan="2">No</th>
                <th class="text-center" rowspan="2">Jenis Agenda Surat</th>
                <th class="text-center" colspan="4" >Jumlah</th>                                   
            </tr>
            <tr>
                <th class="text-center">Belum Terselesaikan (tahun lalu)</th>
                <th class="text-center">Total Surat Masuk</th>
                <th class="text-center">Surat Terselesaikan</th>
                <th class="text-center">Tunggakan belum diselesaikan</th>
            </tr>
        </thead>
        <tbody>   
            <tr>
                <td>1</td>
                <td>Delegasi Perdata (Del.Pdt)</td>
                <td align="center" class="text-center"><?php echo $pdtsisa[0]->totagenda; ?></td>
                <td align="center" class="text-center"><?php echo $pdtsisa[0]->totagenda+$pdtmsk[0]->totmasuk; ?></td>
                <td align="center" class="text-center"><?php echo $pdtsls[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $pdtsisa[0]->totagenda+$pdttgk[0]->tottunggakan; ?></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Delegasi Pidana (Del.Pid)</td>
                <td align="center" class="text-center"><?php echo $pidsisa[0]->totagenda; ?></td>
                <td align="center" class="text-center"><?php echo $pidsisa[0]->totagenda+$pidmsk[0]->totmasuk; ?></td>
                <td align="center" class="text-center"><?php echo $pidsls[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $pidsisa[0]->totagenda+$pidtgk[0]->tottunggakan; ?></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Permintaan Biaya (PMB)</td>
                <td align="center" class="text-center"><?php echo $pmbsisa[0]->totagenda; ?></td>
                <td align="center" class="text-center"><?php echo $pmbsisa[0]->totagenda+$pmbmsk[0]->totmasuk; ?></td>
                <td align="center" class="text-center"><?php echo $pmbsls[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $pmbsisa[0]->totagenda+$pmbtgk[0]->tottunggakan; ?></td>
            </tr>
            <tr>
                <td>4</td>
                <td>Surat Umum (A)</td>
                <td align="center" class="text-center"><?php echo $asisa[0]->totagenda; ?></td>
                <td align="center" class="text-center"><?php echo $asisa[0]->totagenda+$amsk[0]->totmasuk; ?></td>
                <td align="center" class="text-center"><?php echo $asls[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $asisa[0]->totagenda+$atgk[0]->tottunggakan; ?></td>
            </tr>
            <tr>
                <td>5</td>
                <td>Berkas Perkara (BB)</td>
                <td align="center" class="text-center"><?php echo $bbsisa[0]->totagenda; ?></td>
                <td align="center" class="text-center"><?php echo $bbsisa[0]->totagenda+$bbmsk[0]->totmasuk; ?></td>
                <td align="center" class="text-center"><?php echo $bbsls[0]->totselesai; ?></td>
                <td align="center" class="text-center"><?php echo $bbsisa[0]->totagenda+$bbtgk[0]->tottunggakan; ?></td>
            </tr>
        </tbody>
    </table>
</body>
</html>