<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Laporan Surat</a></li>
                <li class="active">Berdasar Klasifikasi</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Laporan Berdasar Klasifikasi</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">LAPORAN SURAT BERDASAR KLASIFIKASI</header>
                    <form class="form-horizontal" method="post" action="<?php echo base_url('laporan/klasifikasi'); ?>">
                    <div class="panel-body">
                        <?php if($message=="error"){ ?>
                        <div class="alert fade in alert-danger" >
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Terjadi kesalahan pada inputan anda
                        </div>
                        <?php } ?>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Tahun Pelaporan<font color="#FF0000 ">*</font></label>
                              <div class="col-sm-10">
                                  <select name="tahun" id="select2-option" style="width:250px" data-required="true" required="required">
                                        <option value=""></option>
                                        <?php foreach ($list as $data){ ?>
                                        <option value="<?php echo $data->tahun; ?>"><?php echo $data->tahun; ?></option>
                                        <?php } ?>
                                  </select>
                              </div>
                            </div>
                    </div>
                    <footer class="panel-footer text-right bg-light lter">
                        <button type="submit" name="submit" value="submit" class="btn btn-success btn-s-xs"><i class="fa fa-search"></i> Cari Laporan</button>
                    </footer>
                    </form>
                  </section>
                </div>
              </div>


            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>