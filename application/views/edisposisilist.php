<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Cetak Disposisi</a></li>
                <li class="active">Elektronik</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Disposisi Elektronik</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <section class="panel panel-default">
                      <header class="panel-heading font-bold">
                          <a style="padding: 10px;" target="_blank" href="<?php echo base_url().'cetak/multipages/'.$tgl; ?>" role="button" class="nav pull-right btn btn-info"><i class="fa fa-print"></i> Cetak Semua Disposisi</a>
                          DISPOSISI ELEKTRONIK
                      </header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No. Agenda</th>
                                        <th class="hidden-phone">Tanggal Penerimaan</th>
                                        <th class="hidden-phone">Nama Pengirim</th>
                                        <th class="hidden-phone">No. Surat Masuk</th>
                                        <th class="hidden-phone">Pelaksana</th>
                                        <th class="hidden-phone">Aksi</th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                        <th class="hidden"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if($disposisi!=false){
                                    $no=0;
                                    foreach($disposisi as $disp){
                                    ?>
                                    <tr>
                                        <td><?php echo $no+1; ?></td>
                                        <td><?php echo $disp->no_agenda; ?></td>
                                        <td class="hidden-phone"><?php echo tgl_indo2(date("Y-m-d", strtotime($disp->tgl_penerimaan))); ?></td>
                                        <td class="hidden-phone"><?php echo $disp->nama_pengirim; ?></td>
                                        <td class="hidden-phone"><?php echo $disp->no_srt_masuk; ?></td>
                                        <td class="hidden-phone"><?php if($disp->id_pegawai=="0"){ echo "Belum Ditentukan"; }else{ echo $disp->nama; } ?></td>
                                        <td class="hidden-phone">
                                            <a href="<?php echo base_url().'cetak/single/'.$disp->id_disposisi ?>" target="_blank" title="Cetak" class="btn btn-sm btn-icon btn-info"><i class="fa fa-print"></i></a>
                                        </td>
                                        <td class="hidden"><?php $jabatan=$this->dashboardsess->idJabatanToNama($disp->tujuan_utama); echo $jabatan[0]->jabatan; ?></td>
                                        <td class="hidden"><?php if($disp->tujuan_pansek==0){ /*echo "Panitera";*/ }else{ $jabatan2=$this->dashboardsess->idJabatanToNama($disp->tujuan_pansek); echo $jabatan2[0]->jabatan; } ?></td>
                                        <td class="hidden"><?php if($disp->tujuan_wapansek==0){ /*echo "Wakil Panitera Sekretaris";*/ }else{ $jabatan3=$this->dashboardsess->idJabatanToNama($disp->tujuan_wapansek); echo $jabatan3[0]->jabatan; } ?></td>
                                        <td class="hidden"><?php if($disp->tujuan_panmud==0){ /*echo "Panmud/Kabag";*/ }else{ $jabatan4=$this->dashboardsess->idJabatanToNama($disp->tujuan_panmud); echo $jabatan4[0]->jabatan; } ?></td>
                                        <td class="hidden"><?php if($disp->tanggapan_utama==""){ echo "Belum ada tanggapan"; }else{ echo $disp->tanggapan_utama; } ?></td>
                                        <td class="hidden"><?php if($disp->tanggapan_pansek==""){ echo "Belum ada tanggapan"; }else{ echo $disp->tanggapan_pansek; } ?></td>
                                        <td class="hidden"><?php if($disp->tanggapan_wapansek==""){ echo "Belum ada tanggapan"; }else{ echo $disp->tanggapan_wapansek; } ?></td>
                                        <td class="hidden"><?php if($disp->tanggapan_panmud==""){ echo "Belum ada tanggapan"; }else{ echo $disp->tanggapan_panmud; } ?></td>
                                        <td class="hidden"><?php if($disp->tgl_utama=="0000-00-00 00:00:00" || $disp->tgl_utama==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($disp->tgl_utama)).")"; } ?></td>
                                        <td class="hidden"><?php if($disp->tgl_pansek=="0000-00-00 00:00:00" || $disp->tgl_pansek==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($disp->tgl_pansek)).")"; } ?></td>
                                        <td class="hidden"><?php if($disp->tgl_wapansek=="0000-00-00 00:00:00" || $disp->tgl_wapansek==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($disp->tgl_wapansek)).")"; } ?></td>
                                        <td class="hidden"><?php if($disp->tgl_panmud=="0000-00-00 00:00:00" || $disp->tgl_panmud==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($disp->tgl_panmud)).")"; } ?></td>
                                        <td class="hidden"><?php if($disp->id_pegawai==0){ /*echo "Staff";*/ }else{ $pegawai=$this->dashboardsess->idPegawaiToidJabatan($disp->id_pegawai); $jabatan5=$this->dashboardsess->idJabatanToNama($pegawai[0]->id_jabatan);echo $jabatan5[0]->jabatan; } ?></td>
                                        <td class="hidden"><?php if($disp->note==""){ echo "Dalam proses"; }else{ echo $disp->note; } ?></td>
                                        <td class="hidden"><?php if($disp->tgl_staff=="0000-00-00 00:00:00" || $disp->tgl_staff==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($disp->tgl_staff)).")"; } ?></td>
                                    </tr>
                                    <?php
                                    $no++;
                                    }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <?php
                                if($disposisi!=false){
                                $no=0;
                                foreach($disposisi as $disp){
                                ?>
                                <form action="<?php echo base_url('dashboard') ?>" method="post">
                                <!-- Modal -->
                                <div class="modal fade" id="myModal<?php echo $no+1; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Perhatian</h4>
                                            </div>
                                            <div class="modal-body">

                                                Pilih salah satu tombol dibawah untuk melanjutkan

                                            </div>
                                            <div class="modal-footer">
                                                <?php if($disp->status=="99") { ?>
                                                <a href="<?php echo base_url('cetak/single/'.$disp->id_disposisi) ?>" target="_blank" class="btn btn-info" role="button"><i class="fa fa-print text"></i> Cetak Disposisi</a>
                                                <?php } ?>
                                                <input type="hidden" name="disposisi" value="<?php echo $disp->id_disposisi; ?>">
                                                <button class="btn btn-success" type="submit" name="submit" value="submit"><i class="fa fa-check text"></i> Selesai Dikerjakan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- modal -->
                                </form>
                                <?php
                                $no++;
                                }
                                }
                                ?>
                            </div>
                        </div>
                  </section>
                </div>
              </div>


            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

    <script type="text/javascript">
      /* Formating function for row details */
      function fnFormatDetails ( oTable, nTr )
      {
          var aData = oTable.fnGetData( nTr );
          var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
          if (aData[8]) {
              sOut += '<tr><td>Tanggapan '+aData[8]+':</td><td>'+aData[12]+'. <i>'+aData[16]+'</i></td></tr>';
          }
          if (aData[9]) {
              sOut += '<tr><td>Tanggapan '+aData[9]+':</td><td>'+aData[13]+'. <i>'+aData[17]+'</i></td></tr>';
          }
          if (aData[10]) {
              sOut += '<tr><td>Tanggapan '+aData[10]+':</td><td>'+aData[14]+'. <i>'+aData[18]+'</i></td></tr>';
          }
          if (aData[11]) {
              sOut += '<tr><td>Tanggapan '+aData[11]+':</td><td>'+aData[15]+'. <i>'+aData[19]+'</i></td></tr>';
          }
          if (aData[20]) {
              sOut += '<tr><td>Tanggapan '+aData[20]+':</td><td>'+aData[21]+'. <i>'+aData[22]+'</i></td></tr>';
          }
          sOut += '</table>';
          return sOut;
      }

      $(document).ready(function() {
          /*
           * Insert a 'details' column to the table
           */
          var nCloneTh = document.createElement( 'th' );
          var nCloneTd = document.createElement( 'td' );
          nCloneTd.innerHTML = '<img src="<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_open.png">';
          nCloneTd.className = "center";

          $('#hidden-table-info thead tr').each( function () {
              this.insertBefore( nCloneTh, this.childNodes[0] );
          } );

          $('#hidden-table-info tbody tr').each( function () {
              this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
          } );

          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });

          /* Add event listener for opening and closing details
           * Note that the indicator for showing which row is open is not controlled by DataTables,
           * rather it is done here
           */
          $('#hidden-table-info tbody td img').live('click', function () {
              var nTr = $(this).parents('tr')[0];
              if ( oTable.fnIsOpen(nTr) )
              {
                  /* This row is already open - close it */
                  this.src = "<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_open.png";
                  oTable.fnClose( nTr );
              }
              else
              {
                  /* Open this row */
                  this.src = "<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_close.png";
                  oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
              }
          } );
      } );
  </script>