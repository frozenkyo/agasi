<!DOCTYPE html>
<html lang="en">
<head>
 <title>Cetak Disposisi</title>
</head>
<style>
    table{
        border: 1;
    }
    td{
        border: none;
    }
    @page {
    margin-top: 1cm;
    margin-bottom: 1cm;
    margin-left: 1cm;
    margin-right: 1cm;
    }
</style>
<body>
    <?php foreach ((array) $content as $isi){ ?>
    <table style="width:100%;border-top: none;border-left: none;border-right: none;" align="center" cellspacing="10">
        <tr>
            <td width="22%" align="right" ><img src="<?php echo base_url(); ?>/images/new_pns2.png"/></td>
            <td align="center" style="margin-right: 5px;margin-left: -5px;">
                <p style="font-size:21px"><strong>PENGADILAN NEGERI / NIAGA / HAM</strong></p> 
                <p style="font-size:21px;"><strong>HUBUNGAN INDUSTRIAL / TIPIKOR SURABAYA</strong></p>
                <p style="font-size:8px;color: #fff">&nbsp;j</p>
                <p style="font-size:15px">Jalan Raya Arjuno No. 16 - 18 Surabaya</p>
                <p style="font-size:15px">Telepon 031-5311523 Faximile 031-5343907</p>
                <p style="font-size:15px">Website : www.pn-surabayakota.go.id</p>
                <p style="font-size:15px">Email : mail@pn-surabayakota.go.id</p>
            </td>
            <td width="2%">&nbsp;</td>
        </tr>
    </table>
    <table style="width:100%;border-bottom: none; border-top: none;" align="center">
        <tr>
            <td style="padding-top: 10px;padding-bottom: 10px;" colspan="3" align="center"><p style="font-size:18px"><strong>LEMBAR DISPOSISI</strong></p></td>
        </tr>
    </table>
    <table style="width:100%;border-bottom: none;" align="left" cellpadding="10">
        <tr>
            <td valign="center" width="15%"><p style="font-size:13px">No. Agenda</p></td>
            <td valign="center" width="33%"><p style="font-size:13px">: <?php echo $isi->no_agenda; ?></p></td>
            <!--<td width="1%" style="border-left: solid 1px black;" rowspan="2"></td>-->
            <td valign="center" width="20%"><p style="font-size:13px">Tk. Keamanan</p></td>
            <td valign="center" ><p style="font-size:13px">: <strong>SR / R / B</strong></p></td>
        </tr>
        <tr>
            <td valign="center" ><p style="font-size:13px">Tgl. Terima</p></td>
            <td valign="center" ><p style="font-size:13px">: <?php $time = strtotime($isi->tgl_penerimaan);echo tgl_indo2(date("Y-m-d", $time)); ?></p></td>
            <td valign="center" ><p style="font-size:13px">Tgl. Penyelesaian</p></td>
            <td valign="center" ><p style="font-size:13px">: </p></td>
        </tr>
    </table>
    <table style="width:100%;" cellpadding="10" align="left">
        <tr >
            <td valign="top" width="48%" height="60px;"><p style="font-size:13px">Tanggal dan Nomor Surat</p></td>
            <!--<td width="1%" style="border-left: solid 1px black;" rowspan="4"></td>-->
            <td valign="top" width="1%" ><p style="font-size:13px">:</p></td>
            <td valign="top" width="45%" style="margin-top: -10px;" >
                <p style="font-size:13px"><?php echo tgl_indo2($isi->tgl_srt_masuk); ?></p><br/>
                <p style="font-size:13px"><?php echo $isi->no_srt_masuk; ?></p>
            </td>
        </tr>
        <tr>
            <td valign="top" width="48%" height="30px;" ><p style="font-size:13px">Dari</p></td>
            <td valign="top" width="1%" ><p style="font-size:13px">:</p></td>
            <td valign="top" width="45%" ><p style="font-size:13px"><?php echo $isi->nama_pengirim; ?></p></td>
        </tr>
        <tr>
            <td valign="top" width="48%" height="50px;" ><p style="font-size:13px">Ringkasan Isi</p></td>
            <td valign="top" width="1%" ><p style="font-size:13px">:</p></td>
            <td valign="top" width="45%" ><p style="font-size:13px"><?php echo $isi->isi_ringkas; ?></p></td>
        </tr>
        <tr>
            <td valign="top" width="48%" height="20px;" ><p style="font-size:13px">Lampiran</p></td>
            <td valign="top" width="1%" ><p style="font-size:13px">:</p></td>
            <td valign="top" width="45%" ><p style="font-size:13px"><?php echo $isi->lampiran; ?></p></td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left" cellpadding="10">
        <tr>
            <td align="center" width="48%"><p style="font-size:13px"><strong>DISPOSISI</strong></p></td>
            <td style="border-left: solid 1px black;" align="center" width="37%"><p style="font-size:13px"><strong>DITUJUKAN KEPADA</strong></p></td>
            <td style="border-left: solid 1px black;" align="center" width="15%"><p style="font-size:13px"><strong>PARAF</strong></p></td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left" cellpadding="10">
        <tr>
            <td valign="top" align="left" width="48%">
                <p style="font-size:13px">KETUA / WAKIL KETUA</p>
            </td>
            <td style="border-left: solid 1px black;" valign="top" align="left" width="37%">
                <p style="font-size:13px">&#9633; PANITERA</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; SEKRETARIS</p>
            </td>
            <td style="border-left: solid 1px black;" valign="top" align="left" width="15%"><p style="font-size:13px"><strong></strong></p></td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left" cellpadding="10">
        <tr>
            <td valign="top" align="left" width="48%" height="80px;">
                <p style="font-size:13px">PANITERA :</p>
            </td>
            <td style="border-left: solid 1px black;" valign="top" align="left" width="52%" colspan="2">
                <p style="font-size:13px">SEKRETARIS :</p>
            </td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left">
        <tr>
            <td style="padding: 10px;border-right: solid 1px black;" valign="top" align="center" width="34%">
                <p style="font-size:13px">DITERUSKAN KEPADA</p>
            </td>
            <td style="padding: 10px;border-right: solid 1px black;" valign="top" align="center" width="15%">
                <p style="font-size:13px">PARAF</p>
            </td>
            <td style="padding: 10px;border-right: solid 1px black;" valign="top" align="center" width="37%">
                <p style="font-size:13px">DITERUSKAN KEPADA</p>
            </td>
            <td style="padding: 10px;" valign="top" align="center" width="15%">
                <p style="font-size:13px">PARAF</p>
            </td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left">
        <tr>
            <td style="padding: 10px;border-right: solid 1px black;" valign="top" align="left" width="39.5%;">
                <p style="font-size:13px">&#9633; WAKIL PANITERA</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; PANMUD PERDATA</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; PANMUD PIDANA</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; PANMUD HUKUM</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; PANMUD TIPIKOR</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; PANMUD NIAGA</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; PANMUD PHI</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; KOORDINATOR DELEGASI</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; LAIN-LAIN</p>
            </td>
            <td style="padding: 10px;border-right: solid 1px black;" valign="top" align="left" width="17.5%">
                <p style="font-size:13px"></p>
            </td>
            <td style="padding: 10px;border-right: solid 1px black;" valign="top" align="left" width="43%">
                <p style="font-size:13px">&#9633; KABAG. UMUM</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; KASUB BAG UMUM DAN KEUANGAN</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; KASUB BAG KEPEGAWAIAN,</p>
                <p style="font-size:13px">&nbsp; &nbsp; ORGANISASI DAN TATA LAKSANA</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; KASUB BAG TEKNOLOGI INFORMASI,</p>
                <p style="font-size:13px">&nbsp; &nbsp; PERENCANAAN, DAN PELAPORAN</p>
                <p style="font-size:13px">&nbsp;</p>
                <p style="font-size:13px">&#9633; LAIN-LAIN</p>
            </td>
            <td style="padding: 10px;" valign="top"  align="left" width="17.5%">
                <p style="font-size:13px"></p>
            </td>
        </tr>
    </table>
    <?php } ?>
</body>
</html>