<!-- .aside -->
        <aside class="bg-dark lter aside-md hidden-print" id="nav">
          <section class="vbox">
            <header class="header bg-primary lter text-center clearfix">
              <div class="btn-group">
                <img src="<?php echo base_url(); ?>/images/pengadilanSby.png" width="40%" height="40%" style="padding-bottom: 10px;padding-top: 10px;"/>
              </div>
            </header>
            <section class="w-f scrollable" style="margin-top: 80px;">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">

                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                  <ul class="nav">
                    <li <?php if($menu=="dashboard"){?> class="active" <?php } ?> >
                      <a href="<?php echo base_url('dashboard'); ?>"  >
                        <i class="fa fa-dashboard icon">
                          <b class="bg-danger"></b>
                        </i>
                        <span>Dashboard</span>
                      </a>
                    </li>
                    <?php
                    $session_data = $this->session->userdata('logged_in');
                    if(($session_data['privilege']>="1")&&($session_data['privilege']<="99")){ ?>
                    <li <?php if($menu=="masuk"){?> class="active" <?php } ?> >
                      <a href=""  >
                        <i class="fa fa-columns icon">
                          <b class="bg-warning"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Surat Masuk</span>
                      </a>
                      <ul class="nav lt">
                        <li <?php if($submenu=="mlist"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('masuk/maillist'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Data Surat</span>
                          </a>
                        </li>
                        <?php if(($session_data['privilege']=="22")){ ?>
                        <li <?php if($submenu=="mwrite"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('masuk/newmail'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Tulis Surat</span>
                          </a>
                        </li>
                        <?php } ?>
                      </ul>
                    </li>
                    <?php } ?>
                    <li <?php if($menu=="keluar"){?> class="active" <?php } ?> >
                      <a href=""  >
                        <i class="fa fa-flask icon">
                          <b class="bg-success"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Surat Keluar</span>
                      </a>
                      <ul class="nav lt">
                        <li <?php if($submenu=="klist"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('keluar/maillist'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Data Surat</span>
                          </a>
                        </li>
                        <?php if((($session_data['privilege']>="17")&&($session_data['privilege']<="35"))){ ?>
                        <li <?php if($submenu=="kwrite"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('keluar/newmail'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Tulis Surat</span>
                          </a>
                        </li>
                        <?php } ?>
                      </ul>
                    </li>
                    <?php if(($session_data['privilege']>="1")&&($session_data['privilege']<="35")){ ?>
                    <li <?php if($menu=="log"){?> class="active" <?php } ?> >
                      <a href="<?php echo base_url('log'); ?>"  >
                        <i class="fa fa-flag icon">
                          <b class="bg-primary"></b>
                        </i>
                        <span>Surat Saya</span>
                      </a>
                    </li>
                    <?php
                    }
                    if(($session_data['privilege']=="22")){ ?>
                    <li <?php if($menu=="disposisi"){?> class="active" <?php } ?> >
                      <a href=""  >
                        <i class="fa fa-envelope-o icon">
                          <b class="bg-primary"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Cetak Disposisi</span>
                      </a>
                      <ul class="nav lt">
                        <li <?php if($submenu=="elek"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('disposisi/elektronik'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Elektronik</span>
                          </a>
                        </li>
                        <li <?php if($submenu=="manu"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('disposisi/manual'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Manual</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <?php } ?>
                    <?php
                    if((($session_data['privilege']>="1")&&($session_data['privilege']<="16"))||($session_data['privilege']=="22")){ ?>
                    <li <?php if($menu=="laporan"){?> class="active" <?php } ?> >
                      <a href=""  >
                        <i class="fa fa-pencil icon">
                          <b class="bg-info"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Laporan Surat</span>
                      </a>
                      <ul class="nav lt">
                        <li <?php if($submenu=="lklas"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('laporan/klasifikasi'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Berdasar Klasifikasi</span>
                          </a>
                        </li>
                        <li <?php if($submenu=="lpelaksana"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('laporan/pelaksana'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Berdasar Pelaksana</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <?php } ?>
                    <?php
                    if($session_data['privilege']=="99"){ ?>
                    <li <?php if($menu=="user"){?> class="active" <?php } ?> >
                      <a href=""  >
                        <i class="fa fa-user-md icon">
                          <b class="bg-info"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Manajemen User</span>
                      </a>
                      <ul class="nav lt">
                        <li <?php if($submenu=="luser"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('manajemen/userlist'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Data User</span>
                          </a>
                        </li>
                        <li <?php if($submenu=="muser"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('manajemen/newuser'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Tambah User</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <?php } ?>
                    <?php if($session_data['privilege']=="99"){ ?>
                    <li <?php if($menu=="klasifikasi"){?> class="active" <?php } ?> >
                      <a href=""  >
                        <i class="fa fa-envelope-o icon">
                          <b class="bg-primary"></b>
                        </i>
                        <span class="pull-right">
                          <i class="fa fa-angle-down text"></i>
                          <i class="fa fa-angle-up text-active"></i>
                        </span>
                        <span>Klasifikasi</span>
                      </a>
                      <ul class="nav lt">
                        <li <?php if($submenu=="klaslist"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('klasifikasi/klasifikasilist'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Data Klasifikasi</span>
                          </a>
                        </li>
                        <li <?php if($submenu=="klasadd"){?> class="active" <?php } ?> >
                          <a href="<?php echo base_url('klasifikasi/newklasifikasi'); ?>" >
                            <i class="fa fa-angle-right"></i>
                            <span>Tambah Klasifikasi</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <?php } ?>
                  </ul>
                </nav>
                <!-- / nav -->
              </div>
            </section>


          </section>
        </aside>
        <!-- /.aside -->