<!DOCTYPE html>
<html lang="en" class="app">
<head>
  <meta charset="utf-8" />
  <title>Agenda dan Disposisi | Pengadilan Negeri Surabaya</title>
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/font.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>js/select2/select2.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>js/select2/theme.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>js/datepicker/datepicker.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>js/slider/slider.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>js/fuelux/fuelux.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo base_url(); ?>js/calendar/bootstrap_calendar.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" type="text/css" />

  <link href="<?php echo base_url(); ?>assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.css" />

  <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/style-responsive.css" rel="stylesheet" />

  <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>

  <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/advanced-datatable/media/js/jquery.js"></script>
  <!--<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>-->
  <!--
  <script class="include" type="text/javascript" src="<?php echo base_url(); ?>js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.scrollTo.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>js/respond.min.js"></script>
  -->
  <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>

  <!--common script for all pages-->
  <script src="<?php echo base_url(); ?>js/common-scripts.js"></script>


</head>
<body>
  <section class="vbox">
    <header class="bg-dark dk header navbar navbar-fixed-top-xs">
      <div class="navbar-header aside-md">
        <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
          <i class="fa fa-bars"></i>
        </a>
        <a href="#" class="navbar-brand" data-toggle="fullscreen"><img src="<?php echo base_url(); ?>images/logo.png" class="m-r-sm">AGASI</a>
        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
          <i class="fa fa-cog"></i>
        </a>
      </div>

      <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user">

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="thumb-sm avatar pull-left">
                <?php
                $session_data = $this->session->userdata('logged_in');
                ?>
               <?php if($session_data['path']==""){ ?>
                <img src="<?php echo base_url(); ?>images/avatar.jpg">
               <?php }else{ ?>
                <img src="<?php echo base_url().$session_data['path']; ?>">
               <?php } ?>
            </span>
            <?php echo ucwords($username); ?><b class="caret"></b>
          </a>
          <ul class="dropdown-menu animated fadeInRight">
            <span class="arrow"></span>

            <li>
              <a href="<?php echo base_url('login/out') ?>"  >Logout</a>
            </li>
          </ul>
        </li>
      </ul>
    </header>