</section>

  <!-- Bootstrap -->
  <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
  <!-- App -->
  <script src="<?php echo base_url(); ?>js/app.js"></script>
  <script src="<?php echo base_url(); ?>js/app.plugin.js"></script>
  <script src="<?php echo base_url(); ?>js/slimscroll/jquery.slimscroll.min.js"></script>
  
<script src="<?php echo base_url(); ?>js/charts/easypiechart/jquery.easy-pie-chart.js"></script>
  <script src="<?php echo base_url(); ?>js/charts/sparkline/jquery.sparkline.min.js"></script>
  <script src="<?php echo base_url(); ?>js/charts/flot/jquery.flot.min.js"></script>
  <script src="<?php echo base_url(); ?>js/charts/flot/jquery.flot.tooltip.min.js"></script>
  <script src="<?php echo base_url(); ?>js/charts/flot/jquery.flot.resize.js"></script>
  <script src="<?php echo base_url(); ?>js/charts/flot/jquery.flot.grow.js"></script>
  <script src="<?php echo base_url(); ?>js/charts/flot/demo.js"></script>
  
  <script src="<?php echo base_url(); ?>js/calendar/bootstrap_calendar.js"></script>
  <script src="<?php echo base_url(); ?>js/calendar/demo.js"></script>
  
  <!-- fuelux -->
  <script src="<?php echo base_url(); ?>js/fuelux/fuelux.js"></script>
  <!-- datepicker -->
  <script src="<?php echo base_url(); ?>js/datepicker/bootstrap-datepicker.js"></script>
  <!-- slider -->
  <script src="<?php echo base_url(); ?>js/slider/bootstrap-slider.js"></script>
  <!-- select2 -->
  <script src="<?php echo base_url(); ?>js/select2/select2.min.js"></script>

  <script src="<?php echo base_url(); ?>js/parsley/parsley.min.js"></script>

  <script src="<?php echo base_url(); ?>js/sortable/jquery.sortable.js"></script>
<!-- file input -->  
<script src="<?php echo base_url(); ?>js/file-input/bootstrap-filestyle.min.js"></script>

</body>
</html>