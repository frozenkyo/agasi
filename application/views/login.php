<!DOCTYPE html>
<html lang="en" class="bg-dark">
<head>
  <meta charset="utf-8" />
  <title>Agenda dan Disposisi | Pengadilan Negeri Surabaya</title>
  <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url();?>css/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url();?>css/font.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/app.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>js/ie/html5shiv.js"></script>
    <script src="<?php echo base_url();?>js/ie/respond.min.js"></script>
    <script src="<?php echo base_url();?>js/ie/excanvas.js"></script>
  <![endif]-->
</head>
<body>
  <section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
    <div class="container aside-xxl">
      <a class="navbar-brand2 block" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/pengadilanSby.png"/></a>
      <section class="panel panel-default bg-white m-t-lg">
        <header class="panel-heading text-center">
          <strong>Sign in</strong>
        </header>
        <form action="<?php echo base_url('login');?>" method="post" class="panel-body wrapper-lg">
          <?php if($message=="fail"){ ?>
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-ban-circle"></i>Username atau password anda salah.
          </div> 
          <?php } else if($message=="suspend"){ ?>
          <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-ban-circle"></i>Akun anda dimatikan. Coba hubungi Administrator.
          </div> 
          <?php } ?>
          <div class="form-group">
            <label class="control-label">Username</label>
            <input type="text" name="username" placeholder="Username" class="form-control input-lg">
          </div>
          <div class="form-group">
            <label class="control-label">Password</label>
            <input type="password" name="password" id="inputPassword" placeholder="Password" class="form-control input-lg">
          </div>
<!--          <div class="checkbox">
            <label>
              <input type="checkbox"> Keep me logged in
            </label>
          </div>-->
          <button type="submit" name="submit"value="submit" class="btn btn-primary">Sign in</button>
          <div class="line line-dashed"></div>
          
        </form>
      </section>
    </div>
  </section>
  <!-- footer -->
  <footer id="footer">
    <div class="text-center padder">
      <p>
          <small>Developed By <a href="http://www.arifaradisa.com" target="_blank">AGASI Team</a><br>&copy; 2015</small>
      </p>
    </div>
  </footer>
  <!-- / footer -->
  <script src="<?php echo base_url();?>js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?php echo base_url();?>js/bootstrap.js"></script>
  <!-- App -->
  <script src="<?php echo base_url();?>js/app.js"></script>
  <script src="<?php echo base_url();?>js/app.plugin.js"></script>
  <script src="<?php echo base_url();?>js/slimscroll/jquery.slimscroll.min.js"></script>
  
</body>
</html>