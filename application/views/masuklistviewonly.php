<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="#"><i class="fa fa-home"></i> Surat Masuk</a></li>
                <li class="active">Data Surat</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Surat Masuk</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">

                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">DATA SURAT MASUK</header>
                    <div class="panel-body">
                        <?php if($message=="success"){ ?>
                        <!-- Error Message -->
                        <div class="alert fade in alert-success fail" id="fail">
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Data upload berhasil ditambahkan.
                        </div>
                        <?php }else if($message=="errorupload"){ ?>
                        <div class="alert fade in alert-danger invalid" id="invalid">
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Data upload yang anda masukkan gagal diupload.
                        </div>

                        <?php } ?>
                        <div class="adv-table">
                            <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                <thead>
                                <tr>
                                    <th>No. Agenda</th>
                                    <th class="hidden-phone">Tgl Penerimaan</th>
                                    <th class="hidden-phone">Pengirim</th>
                                    <th class="hidden-phone">No. Surat Masuk</th>
                                    <th class="hidden-phone">Pelaksana</th>
                                    <th class="hidden-phone">Status</th>
                                    <th class="hidden-phone">File</th>
                                    <th class="hidden">Perihal</th>
                                    <th class="hidden">Ringkasan</th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($content!=false){
                                $no=0;
                                foreach($content as $isi){
                                ?>
                                <tr>
                                    <td><?php echo $isi->no_agenda; ?></td>
                                    <td class="hidden-phone"><?php echo tgl_indo2(date("Y-m-d", strtotime($isi->tgl_penerimaan))); ?></td>
                                    <td class="hidden-phone"><?php echo $isi->nama_pengirim; ?></td>
                                    <td class="hidden-phone"><?php echo $isi->no_srt_masuk; ?></td>
                                    <td class="hidden-phone"><?php if($isi->id_pegawai=="0"){ echo "Belum Ditentukan"; }else{ echo $isi->nama; } ?></td>
                                    <td class="hidden-phone"><?php if($isi->status<99){ echo "<strong><span class=\"text-info\">Dikerjakan</span></strong>"; } else if($isi->status==99) { echo "<strong><span class=\"text-warning\">Tidak Dikerjakan</span></strong>"; } else if($isi->status==100) { echo "<strong><span class=\"text-success\">Selesai</span></strong>"; } ?></td>
                                    <td class="hidden-phone center"><?php if($isi->filename==""){ echo "<span class=\"label bg-danger\"><i class=\"fa fa-exclamation\"></i></span>"; }else{ echo "<a href=\"".base_url().$isi->filename."\" target=\"_blank\" ><span class=\"label bg-success\">Lihat</span></a>"; } ?></td>
                                    <td class="hidden"><?php echo $isi->hal_surat; ?></td>
                                    <td class="hidden"><?php echo $isi->isi_ringkas; ?></td>
                                    <td class="hidden"><?php $jabatan=$this->masuksess->idJabatanToNama($isi->tujuan_utama); echo $jabatan[0]->jabatan; ?></td>
                                    <td class="hidden"><?php if($isi->tujuan_pansek==0){ /*echo "Panitera/Sekretaris";*/ }else{ $jabatan2=$this->masuksess->idJabatanToNama($isi->tujuan_pansek); echo $jabatan2[0]->jabatan; } ?></td>
                                    <td class="hidden"><?php if($isi->tujuan_wapansek==0){ /*echo "Wakil Panitera/Sekretaris";*/ }else{ $jabatan3=$this->masuksess->idJabatanToNama($isi->tujuan_wapansek); echo $jabatan3[0]->jabatan; } ?></td>
                                    <td class="hidden"><?php if($isi->tujuan_panmud==0){ /*echo "Panmud/Kabag";*/ }else{ $jabatan4=$this->masuksess->idJabatanToNama($isi->tujuan_panmud); echo $jabatan4[0]->jabatan; } ?></td>
                                    <td class="hidden"><?php if($isi->tanggapan_utama==""){ echo "Belum ada tanggapan"; }else{ echo $isi->tanggapan_utama; } ?></td>
                                    <td class="hidden"><?php if($isi->tanggapan_pansek==""){ echo "Belum ada tanggapan"; }else{ echo $isi->tanggapan_pansek; } ?></td>
                                    <td class="hidden"><?php if($isi->tanggapan_wapansek==""){ echo "Belum ada tanggapan"; }else{ echo $isi->tanggapan_wapansek; } ?></td>
                                    <td class="hidden"><?php if($isi->tanggapan_panmud==""){ echo "Belum ada tanggapan"; }else{ echo $isi->tanggapan_panmud; } ?></td>
                                    <td class="hidden"><?php if($isi->tgl_utama=="0000-00-00 00:00:00" || $isi->tgl_utama==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($isi->tgl_utama)).")"; } ?></td>
                                    <td class="hidden"><?php if($isi->tgl_pansek=="0000-00-00 00:00:00" || $isi->tgl_pansek==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($isi->tgl_pansek)).")"; } ?></td>
                                    <td class="hidden"><?php if($isi->tgl_wapansek=="0000-00-00 00:00:00" || $isi->tgl_wapansek==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($isi->tgl_wapansek)).")"; } ?></td>
                                    <td class="hidden"><?php if($isi->tgl_panmud=="0000-00-00 00:00:00" || $isi->tgl_panmud==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($isi->tgl_panmud)).")"; } ?></td>
                                    <td class="hidden"><?php if($isi->id_pegawai==0){ /*echo "Staff";*/ }else{ $pegawai=$this->dashboardsess->idPegawaiToidJabatan($isi->id_pegawai); $jabatan5=$this->dashboardsess->idJabatanToNama($pegawai[0]->id_jabatan);echo $jabatan5[0]->jabatan; } ?></td>
                                    <td class="hidden"><?php if($isi->note==""){ echo "Dalam proses"; }else{ echo $isi->note; } ?></td>
                                    <td class="hidden"><?php if($isi->tgl_staff=="0000-00-00 00:00:00" || $isi->tgl_staff==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($isi->tgl_staff)).")"; } ?></td>
                                </tr>
                                <?php
                                $no++;
                                }
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                  </section>
                </div>
              </div>


            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

    <script type="text/javascript">
      /* Formating function for row details */
      function fnFormatDetails ( oTable, nTr )
      {
          var aData = oTable.fnGetData( nTr );
          console.log(aData);
          var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
          sOut += '<tr><td>Perihal:</td><td>'+aData[8]+'</td></tr>';
          sOut += '<tr><td>Ringkasan Surat:</td><td>'+aData[9]+'</td></tr>';
          sOut += '<tr><td>Tanggapan '+aData[10]+':</td><td>'+aData[14]+'. <i>'+aData[18]+'</i></td></tr>';
          if (aData[11] != "") {
              sOut += '<tr><td>Tanggapan '+aData[11]+':</td><td>'+aData[15]+'. <i>'+aData[19]+'</i></td></tr>';
          }
          if (aData[12] != "") {
              sOut += '<tr><td>Tanggapan '+aData[12]+':</td><td>'+aData[16]+'. <i>'+aData[20]+'</i></td></tr>';
          }
          if (aData[13] != "") {
              sOut += '<tr><td>Tanggapan '+aData[13]+':</td><td>'+aData[17]+'. <i>'+aData[21]+'</i></td></tr>';
          }
          if (aData[22] != "") {
              sOut += '<tr><td>Tanggapan '+aData[22]+':</td><td>'+aData[23]+'. <i>'+aData[24]+'</i></td></tr>';
          }
          sOut += '</table>';
          return sOut;
      }

      $(document).ready(function() {
          /*
           * Insert a 'details' column to the table
           */
          var nCloneTh = document.createElement( 'th' );
          var nCloneTd = document.createElement( 'td' );
          nCloneTd.innerHTML = '<img src="<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_open.png">';
          nCloneTd.className = "center";

          $('#hidden-table-info thead tr').each( function () {
              this.insertBefore( nCloneTh, this.childNodes[0] );
          } );

          $('#hidden-table-info tbody tr').each( function () {
              this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
          } );

          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });

          /* Add event listener for opening and closing details
           * Note that the indicator for showing which row is open is not controlled by DataTables,
           * rather it is done here
           */
          $('#hidden-table-info tbody td img').live('click', function () {
              var nTr = $(this).parents('tr')[0];
              if ( oTable.fnIsOpen(nTr) )
              {
                  /* This row is already open - close it */
                  this.src = "<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_open.png";
                  oTable.fnClose( nTr );
              }
              else
              {
                  /* Open this row */
                  this.src = "<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_close.png";
                  oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
              }
          } );
      } );
  </script>