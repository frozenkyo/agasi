<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="#"><i class="fa fa-home"></i> Klasifikasi</a></li>
                <li class="active">Data Klasifikasi</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Klasifikasi</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    
                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">DATA KLASIFIKASI </header>
                    <div class="panel-body">
                        <?php if($message=="hapus"){ ?>
                    <div class="alert fade in alert-success reg" id="reg">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Klasifikasi berhasil dihapus.
                    </div>
                    <?php }else if($message=="password"){ ?>
                    <div class="alert fade in alert-success" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Password user berhasil diganti
                    </div>
                    <?php }else if($message=="edit"){ ?>
                    <div class="alert fade in alert-success" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data user berhasil diperbarui
                    </div>
                    <?php }else if($message=="error"){ ?>
                    <div class="alert fade in alert-danger" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data klasifikasi tidak bisa dihapus karena memiliki referensi pada Agenda dan Disposisi
                    </div>
                    <?php } ?>
                        <div class="adv-table">
                            <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th class="hidden-phone">Nama Klasifikasi</th>
                                    <th class="hidden-phone">Kode Klasifikasi</th>
                                    <th class="hidden-phone">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($content!=false){
                                $no=1;
                                foreach($content as $isi){
                                ?>    
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $isi->jenis;?></td>
                                    <td><?php echo $isi->kode;?></td>
                                    <td>
                                        <a href="#myModal<?php echo $no; ?>" data-toggle="modal" title="Hapus Klasifikasi" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <?php 
                                $no++;
                                }
                                }
                                ?>
                                </tbody>
                            </table>
                            <?php
                                if($content!=false){
                                $no=0;
                                foreach($content as $isi){
                                ?>
                                <form action="<?php echo base_url('klasifikasi/delklas') ?>" method="post">
                                <!-- Modal -->
                                <div class="modal fade" id="myModal<?php echo $no+1; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Perhatian</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p><strong>Pastikan klasifikasi yang anda hapus tidak memiliki referensi pada Agenda dan Disposisi</strong></p>
                                                <br />
                                                <p>Apakah Anda yakin menghapus klasifikasi <strong><?php echo $isi->jenis; ?></strong>?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="id_klasifikasi" value="<?php echo $isi->id_klasifikasi; ?>">
                                                <button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true" >Batal</button>
                                                <button class="btn btn-danger" type="submit" name="submit" value="submit"><i class="fa fa-times text"></i> Hapus Klasifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                    
                                <!-- modal -->
                                </form>
                                <?php
                                $no++;
                                }
                                }
                                ?>
                        </div>
                    </div>
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

    <script type="text/javascript">
      
      $(document).ready(function() {
          
          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });

          /* Add event listener for opening and closing details
           * Note that the indicator for showing which row is open is not controlled by DataTables,
           * rather it is done here
           */
      } );
  </script>