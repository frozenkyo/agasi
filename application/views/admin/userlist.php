<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="#"><i class="fa fa-home"></i> Manajemen User</a></li>
                <li class="active">Data User</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Manajemen User</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    
                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">DATA USER </header>
                    <div class="panel-body">
                        <?php if($message=="hapus"){ ?>
                    <div class="alert fade in alert-success reg" id="reg">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            User berhasil dihapus.
                    </div>
                    <?php }else if($message=="password"){ ?>
                    <div class="alert fade in alert-success" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Password user berhasil diganti
                    </div>
                    <?php }else if($message=="edit"){ ?>
                    <div class="alert fade in alert-success" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data user berhasil diperbarui
                    </div>
                    <?php }else if($message=="error"){ ?>
                    <div class="alert fade in alert-danger" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Terjadi kesalahan pada inputan anda
                    </div>
                    <?php } ?>
                        <div class="adv-table">
                            <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th class="hidden-phone">Username</th>
                                    <th class="hidden-phone">Password</th>
                                    <th class="hidden-phone">Nama Lengkap</th>
                                    <th class="hidden-phone">Hak Akses</th>
                                    <th class="hidden-phone">Status</th>
                                    <th class="hidden-phone">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($content!=false){
                                $no=1;
                                foreach($content as $isi){
                                ?>    
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $isi->username;?></td>
                                    <td><?php echo $isi->vpassword;?></td>
                                    <td><?php echo ucfirst($isi->nama);?></td>
                                    <td><?php echo $isi->jabatan;?></td>
                                    <td><?php if($isi->status==0) { echo "<span class=\"label bg-danger\">Tidak Aktif</span>"; } else if($isi->status==1) { echo "<span class=\"label bg-success\">Aktif</span>"; }?></td>
                                    <td>
                                        <a href="<?php echo base_url('manajemen/switchuser/'.$isi->id_pegawai); ?>" title="Nyalakan/Matikan Akun" class="btn btn-sm btn-icon btn-dark"><i class="fa fa-power-off"></i></a> 
                                        <a href="#myModals<?php echo $no; ?>" data-toggle="modal" title="Ganti Password" class="btn btn-sm btn-icon btn-warning"><i class="fa fa-key"></i></a>
                                        <a href="<?php echo base_url('manajemen/edit/'.$isi->id_pegawai); ?>" title="Edit User" class="btn btn-sm btn-icon btn-info"><i class="fa fa-edit"></i></a> 
                                        <a href="#myModal<?php echo $no; ?>" data-toggle="modal" title="Hapus User" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-times"></i></a> 
                                    </td>
                                </tr>
                                <?php 
                                $no++;
                                }
                                }
                                ?>
                                </tbody>
                            </table>
                            <?php
                                if($content!=false){
                                $no=0;
                                foreach($content as $isi){
                                ?>
                                <form action="<?php echo base_url('manajemen/deluser') ?>" method="post">
                                <!-- Modal -->
                                <div class="modal fade" id="myModal<?php echo $no+1; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Perhatian</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p><strong>Data yang akan ikut terhapus antara lain :</strong></p>
                                                    <p><i class="fa fa-check text"></i> Surat masuk yang pernah dikerjakan oleh user</p>
                                                    <p><i class="fa fa-check text"></i> Surat keluar yang pernah dikerjakan oleh user</p>
                                                    <p><i class="fa fa-check text"></i> Seluruh data user tersebut</p>
                                                <br />
                                                <p>Apakah Anda yakin menghapus user <strong><?php echo $isi->username; ?></strong>?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="id_pegawai" value="<?php echo $isi->id_pegawai; ?>">
                                                <button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true" >Batal</button>
                                                <button class="btn btn-danger" type="submit" name="submit" value="submit"><i class="fa fa-times text"></i> Hapus User</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                    
                                <!-- modal -->
                                </form>
                                <?php
                                $no++;
                                }
                                }
                                ?>
                            
                            <?php
                                if($content!=false){
                                $no=0;
                                foreach($content as $isi){
                                ?>
                                <form action="<?php echo base_url('manajemen/passwd') ?>" method="post">
                                <!-- Modal -->
                                <div class="modal fade" id="myModals<?php echo $no+1; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Ganti Password</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>Masukkan password baru untuk user <strong><?php echo $isi->username; ?></strong></label>
                                                    <input name="password" type="password" class="form-control" required="required" >
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="id_pegawai" value="<?php echo $isi->id_pegawai; ?>">
                                                <button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true" >Batal</button>
                                                <button class="btn btn-success" type="submit" name="submit" value="submit"><i class="fa fa-save text"></i> Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                    
                                <!-- modal -->
                                </form>
                                <?php
                                $no++;
                                }
                                }
                                ?>
                        </div>
                    </div>
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

    <script type="text/javascript">
      
      $(document).ready(function() {
          
          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });

          /* Add event listener for opening and closing details
           * Note that the indicator for showing which row is open is not controlled by DataTables,
           * rather it is done here
           */
      } );
  </script>