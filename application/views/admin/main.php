<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li class="active"><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Dashboard</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <section class="panel panel-default">
                <div class="row m-l-none m-r-none bg-light lter">
                  <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                    <span class="fa-stack fa-2x pull-left m-r-sm">
                      <i class="fa fa-circle fa-stack-2x text-info"></i>
                      <i class="fa fa-cloud-download fa-stack-1x text-white"></i>
                    </span>
                    <span class="h3 block m-t-xs"><strong><?php echo $masuk;?></strong></span>
                    <small class="text-muted text-uc">Surat Masuk</small>
                  </div>
                  <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                    <span class="fa-stack fa-2x pull-left m-r-sm">
                      <i class="fa fa-circle fa-stack-2x text-warning"></i>
                      <i class="fa fa-cloud-upload fa-stack-1x text-white"></i>
                    </span>
                    <span class="h3 block m-t-xs"><strong><?php echo $keluar;?></strong></span>
                    <small class="text-muted text-uc">Surat Keluar</small>
                  </div>
                  <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                    <span class="fa-stack fa-2x pull-left m-r-sm">
                      <i class="fa fa-circle fa-stack-2x text-danger"></i>
                      <i class="fa fa-check fa-stack-1x text-white"></i>
                    </span>
                    <span class="h3 block m-t-xs"><strong><?php echo $selesai;?></strong></span>
                    <small class="text-muted text-uc">Terselesaikan</small>
                  </div>
                  <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                    <span class="fa-stack fa-2x pull-left m-r-sm">
                      <i class="fa fa-circle fa-stack-2x text-muted"></i>
                      <i class="fa fa-bar-chart-o fa-stack-1x text-white"></i>
                    </span>
                    <span class="h3 block m-t-xs"><strong><?php echo $surat;?></strong></span>
                    <small class="text-muted text-uc">Total Surat</small>
                  </div>
                </div>
              </section>
              <div class="row">
                <div class="col-md-12">
                  <section class="panel panel-default">
                      
                        
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

    <script type="text/javascript">
      /* Formating function for row details */
      function fnFormatDetails ( oTable, nTr )
      {
          var aData = oTable.fnGetData( nTr );
          var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
          sOut += '<tr><td>Tanggapan '+aData[6]+':</td><td>'+aData[10]+'. <i>'+aData[14]+'</i></td></tr>';
          sOut += '<tr><td>Tanggapan '+aData[7]+':</td><td>'+aData[11]+'. <i>'+aData[15]+'</i></td></tr>';
          sOut += '<tr><td>Tanggapan '+aData[8]+':</td><td>'+aData[12]+'. <i>'+aData[16]+'</i></td></tr>';
          sOut += '<tr><td>Tanggapan '+aData[9]+':</td><td>'+aData[13]+'. <i>'+aData[17]+'</i></td></tr>';
          sOut += '</table>';
          return sOut;
      }

      $(document).ready(function() {
          /*
           * Insert a 'details' column to the table
           */
          var nCloneTh = document.createElement( 'th' );
          var nCloneTd = document.createElement( 'td' );
          nCloneTd.innerHTML = '<img src="<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_open.png">';
          nCloneTd.className = "center";

          $('#hidden-table-info thead tr').each( function () {
              this.insertBefore( nCloneTh, this.childNodes[0] );
          } );

          $('#hidden-table-info tbody tr').each( function () {
              this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
          } );

          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });

          /* Add event listener for opening and closing details
           * Note that the indicator for showing which row is open is not controlled by DataTables,
           * rather it is done here
           */
          $('#hidden-table-info tbody td img').live('click', function () {
              var nTr = $(this).parents('tr')[0];
              if ( oTable.fnIsOpen(nTr) )
              {
                  /* This row is already open - close it */
                  this.src = "<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_open.png";
                  oTable.fnClose( nTr );
              }
              else
              {
                  /* Open this row */
                  this.src = "<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_close.png";
                  oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
              }
          } );
      } );
  </script>