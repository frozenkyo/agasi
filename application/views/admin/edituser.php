<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="#"><i class="fa fa-home"></i> Manajemen User</a></li>
                <li class="active">Data User</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Manajemen User</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">EDIT DATA USER</header>
                    <form class="form-horizontal" method="post" action="<?php echo base_url('manajemen/edit'); ?>">
                        <div class="panel-body" style="padding-left: 30px;padding-right: 30px;">
                    <input type="hidden" name="id_pegawai" value="<?php echo $this->uri->segment(3); ?>">
                        <div class="form-group">
                        <p class="m-t">Nama Lengkap<font color="#FF0000 ">*</font></p>
                        <input name="lengkap" type="text" class="form-control" data-trigger="change" data-required="true" required="required" value="<?php echo $content[0]->nama; ?>">
                        </div>
                        <div class="form-group ">
                        <p class="m-t">Telepon<font color="#FF0000 ">*</font></p>
                        <input name="telepon" type="text" class="form-control" data-trigger="change" data-required="true" required="required" value="<?php echo $content[0]->telepon; ?>">
                        </div>
                        <div class="form-group ">
                        <p class="m-t">Email</p>
                        <input name="email" type="email" class="form-control" value="<?php echo $content[0]->email; ?>">
                        </div>
                    </div>
                    <footer class="panel-footer text-right bg-light lter">
                        <button type="submit" name="submit" value="submit" class="btn btn-success btn-s-xs"><i class="fa fa-save"></i> Simpan Perubahan</button>
                    </footer>
                    </form>
                  </section>
                </div>
              </div>
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>