<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Klasifikasi</a></li>
                <li class="active">Tambah Klasifikasi</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Klasifikasi</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <?php if($message=="success"){ ?>
                    <div class="alert fade in alert-success reg" id="reg">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Klasifikasi baru berhasil ditambahkan.
                    </div>
                    <?php }else if($message=="error"){ ?>
                    <div class="alert fade in alert-danger" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Terjadi kesalahan pada inputan anda
                    </div>
                    <?php } ?>
                    <section class="panel panel-default">
                        <div class="wizard clearfix" id="form-wizard5">
                        <ul class="steps">
                          <li data-target="#step1" class="active"><span class="badge badge-info">1</span>Input Data Klasifikasi</li>
                          <li data-target="#step2"><span class="badge">2</span>Review Data Klasifikasi</li>
                        </ul>
                      </div>
                      <div class="step-content">
                        <form method="post" action="<?php echo base_url("klasifikasi/newklasifikasi"); ?>">
                          <div class="step-pane active" id="step1">
                            <div class="form-group">
                            <p class="m-t">Nama Klasifikasi<font color="#FF0000 ">*</font></p>
                            <input name="nama" type="text" class="form-control" data-trigger="change" data-required="true" required="required">
                            </div>
                            <div class="form-group ">
                            <p class="m-t">Kode Klasifikasi<font color="#FF0000 ">*</font></p>
                            <input name="kode" type="text" class="form-control" data-trigger="change" data-required="true" required="required">
                            </div>
                          </div>
                          <div class="step-pane" id="step2">
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-10">
                                    <p class="form-control-static"><strong>Berikut adalah data untuk pembuatan klasifikasi baru :</strong></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Nama Klasifikasi</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="nama"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Kode Klasifikasi</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="kode"></p>
                                </div>
                              </div>
                          </div>               
                        <div class="actions m-t" align="right">
                          <button type="button" class="btn btn-info btn-sm btn-prev" data-target="#form-wizard5" data-wizard5="previous" disabled="disabled">Prev</button>
                          <button type="button" class="btn btn-info btn-sm btn-next" data-target="#form-wizard5" data-wizard5="next">Next</button> 
                          <button type="submit" name="submit" value="submit" class="btn btn-success btn-sm btn-finish" data-target="#form-wizard5" data-wizard5="finish" >Finish</button>
                        </div> 
                        </form>
                      </div>
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>
    
    <script>
    $(document).ready(function() {
        $( "input[name=nama]" ).change(function(){
            var b = $( "input[name=nama]" ).val();
            $('#nama').text(b);  
        }); 
        $( "input[name=kode]" ).change(function(){
            var c = $( "input[name=kode]" ).val();
            $('#kode').text(c);
        });
    });
    </script>