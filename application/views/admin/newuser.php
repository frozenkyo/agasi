<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Manajemen User</a></li>
                <li class="active">Tambah User</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Manajemen User</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <?php if($message=="success"){ ?>
                    <div class="alert fade in alert-success reg" id="reg">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            User baru berhasil ditambahkan.
                    </div>
                    <?php }else if($message=="error"){ ?>
                    <div class="alert fade in alert-danger" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Terjadi kesalahan pada inputan anda
                    </div>
                    <?php } ?>
                    <section class="panel panel-default">
                       <div class="wizard clearfix" id="form-wizard">
                        <ul class="steps">
                          <li data-target="#step1" class="active"><span class="badge badge-info">1</span>Identitas Diri</li>
                          <li data-target="#step2"><span class="badge">2</span>Data Administrasi</li>
                          <li data-target="#step3"><span class="badge">3</span>Review Data User</li>
                        </ul>
                      </div>
                      <div class="step-content">
                        <form method="post" enctype="multipart/form-data" action="<?php echo base_url("manajemen/newuser"); ?>">
                          <div class="step-pane active" id="step1">
                            <div class="form-group">
                            <p class="m-t">Nama Lengkap<font color="#FF0000 ">*</font></p>
                            <input name="lengkap" type="text" class="form-control" data-trigger="change" data-required="true" required="required">
                            </div>
                            <div class="form-group ">
                            <p class="m-t">Telepon<font color="#FF0000 ">*</font></p>
                            <input name="telepon" type="text" class="form-control" data-trigger="change" data-required="true" required="required">
                            </div>
                            <div class="form-group ">
                            <p class="m-t">Email</p>
                            <input name="email" type="email" class="form-control">
                            </div>
                            <div class="form-group">
                            <p class="m-t">Foto<font color="#FF0000 ">*</font></p>
                            <input type="file" name="fileupload" class="filestyle" data-icon="false" data-classButton="btn btn-default" data-classInput="form-control inline input-s">
                            </div>
                          </div>
                          <div class="step-pane" id="step2">
                                <div class="form-group">
                                <p class="m-t">Username<font color="#FF0000 ">*</font></p>
                                <input name="username" type="text" class="form-control" data-trigger="change" data-required="true" required="required">
                                </div>
                                <div class="form-group">
                                <p class="m-t">Password<font color="#FF0000 ">*</font></p>
                                <input name="password" type="password" class="form-control" data-trigger="change" data-required="true" required="required">
                                </div>
                                <div class="form-group">
                                <p class="m-t">Hak Akses<font color="#FF0000 ">*</font></p>
                                    <select name="hakakses" id="select2-option5" style="width:260px" data-required="true" required="required">
                                            <option value=""></option>
                                            <?php foreach($content as $isi){ ?>
                                            <option value="<?php echo $isi->id_jabatan; ?>"><?php echo $isi->jabatan; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                          </div>
                          <div class="step-pane" id="step3">
                              <div class="form-group pull-in clearfix">
                                <div class="col-sm-10">
                                    <p class="form-control-static"><strong>Berikut adalah data untuk pembuatan user baru :</strong></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Nama Lengkap</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="lengkap"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Telepon</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="telepon"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="email"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="username"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">*******</p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Hak Akses</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="hakakses"></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static" id="foto"></p>
                                </div>
                              </div>
                          </div>
                        <div class="actions m-t" align="right">
                          <button type="button" class="btn btn-info btn-sm btn-prev" data-target="#form-wizard" data-wizard="previous" disabled="disabled">Prev</button>
                          <button type="button" class="btn btn-info btn-sm btn-next" data-target="#form-wizard" data-wizard="next">Next</button>
                          <button type="submit" name="submit" value="submit" class="btn btn-success btn-sm btn-finish" data-target="#form-wizard" data-wizard="finish" >Finish</button>
                        </div>
                        </form>
                      </div>
                  </section>
                </div>
              </div>


            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

    <script>
    $(document).ready(function() {

        $( "input[name=lengkap]" ).change(function(){
            var b = $( "input[name=lengkap]" ).val();
            $('#lengkap').text(b);
        });
        $( "input[name=telepon]" ).change(function(){
            var c = $( "input[name=telepon]" ).val();
            $('#telepon').text(c);
        });
        $( "input[name=email]" ).change(function(){
            var d = $( "input[name=email]" ).val();
            $('#email').text(d);
        });
        $( "input[name=username]" ).change(function(){
            var g = $( "input[name=username]" ).val();
            $('#username').text(g);
        });
        $( "input[name=fileupload]" ).change(function(){
            var i = $( "input[name=fileupload]" ).val();
            $('#foto').text(i);
        });
    });
    </script>