<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Cetak Disposisi</a></li>
                <li class="active">Manual</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Disposisi Manual</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">DISPOSISI MANUAL</header>
                    <form class="form-horizontal" method="post" action="<?php echo base_url('disposisi/manual'); ?>">
                    <div class="panel-body">
                        <?php if($message=="error"){ ?>
                        <div class="alert fade in alert-danger" >
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Terjadi kesalahan pada inputan anda
                        </div>
                        <?php }else if($message=="warning"){ ?>
                        <div class="alert fade in alert-warning" >
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Disposisi yang diterbitkan tanggal <strong><?php echo tgl_indo2($tanggal); ?></strong> tidak ditemukan
                        </div>
                        <?php } ?>
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Tanggal Penerimaan<font color="#FF0000 ">*</font></label>
                              <div class="col-sm-10">
                                  <input name="tgl_terima" class="input-sm input-s datepicker-input form-control" size="16" data-date-format="yyyy-mm-dd" type="text" required="required">
                              </div>
                            </div>   
                    </div>
                    <footer class="panel-footer text-right bg-light lter">
                        <button type="submit" name="submit" value="submit" class="btn btn-success btn-s-xs"><i class="fa fa-search"></i> Cari Disposisi</button>
                    </footer>
                    </form>
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>