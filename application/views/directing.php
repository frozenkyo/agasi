<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li class="active"><a href="#"><i class="fa fa-home"></i> Tanggapan</a></li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Tanggapan</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <?php if($message=="fail"){ ?>
                    <!-- Error Message -->
                    <div class="alert fade in alert-danger fail" id="fail">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Terjadi kesalahan.
                    </div>
                    <?php }else if($message=="duplicate"){ ?>
                    <div class="alert fade in alert-danger invalid" id="invalid">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data yang anda masukkan sudah ada. Periksa kembali inputan anda.
                    </div>
                    <?php }else if($message=="success"){ ?>
                    <div class="alert fade in alert-success reg" id="reg">
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data berhasil ditambahkan
                    </div>
                    <?php }else if($message=="error"){ ?>
                    <div class="alert fade in alert-danger" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Terjadi kesalahan pada inputan anda
                    </div>
                    <?php }else if($message=="errorupload"){ ?>
                    <div class="alert fade in alert-warning" >
                            <i class="icon-remove close" data-dismiss="alert"></i>
                            Data berhasil ditambahkan tetapi dokumen file belum anda upload
                    </div>
                    <?php } ?>
                    <section class="panel panel-default">
                       <div class="wizard clearfix" id="form-wizard3">
                        <ul class="steps">
                          <li data-target="#step1" class="active"><span class="badge badge-info">1</span>Review Disposisi</li>
                          <li data-target="#step2"><span class="badge">2</span>Beri Tanggapan</li>
                        </ul>
                      </div>
                      <div class="step-content">
                        <form method="post" action="<?php echo base_url("dashboard/direct"); ?>">
                          <div class="step-pane active" id="step1">
                              <input type="hidden" data-required="true" name="referensi" value="<?php echo $referensi; ?>">
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Nomor Agenda</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->no_agenda; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tanggal Penerimaan</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo date("d M Y g:i A", strtotime($disposisi[0]->tgl_penerimaan)); ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Klasifikasi Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php if($disposisi[0]->id_klasifikasi=="1"){ echo "Pidana"; }else if($disposisi[0]->id_klasifikasi=="2"){ echo "Perdata"; }else if($disposisi[0]->id_klasifikasi=="3"){ echo "Juru Sita"; }else if($disposisi[0]->id_klasifikasi=="4"){ echo "Berkas Perkara"; }else if($disposisi[0]->id_klasifikasi=="5"){ echo "Umum"; } ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tanggal Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo tgl_indo2($disposisi[0]->tgl_srt_masuk); ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Nomor Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->no_srt_masuk; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Perihal</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->hal_surat; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tujuan Surat</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->tujuan; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Nama Pengirim</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->nama_pengirim; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Alamat Pengirim</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->alamat_pengirim; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Ringkasan Isi</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->isi_ringkas; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Tembusan</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->tembusan; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Salinan</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->salinan; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Lampiran</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->lampiran; ?></p>
                                </div>
                              </div>
                              <div class="form-group pull-in clearfix">
                                <label class="col-sm-2 control-label">Keterangan</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $disposisi[0]->keterangan; ?></p>
                                </div>
                              </div>
                          </div>
                          <div class="step-pane" id="step2">
                            <div class="form-group">
                            <p class="m-t">Tanggapan<font color="#FF0000 ">*</font></p>
                            <textarea name="tanggapan" class="form-control" rows="6" data-minwords="6" data-required="true" required="required"></textarea>
                            </div>
                            <div class="form-group">
                            <p class="m-t">Arahan<font color="#FF0000 ">*</font></p>
                                <select name="tujuandisp" id="select2-option" style="width:400px" data-required="true" required="required">
                                        <option value=""></option>
                                        <?php if (($hakakses=="1")||($hakakses=="2")): ?>
                                        <option value="3"> Panitera</option>
                                        <option value="5"> Wakil Panitera</option>
                                        <option value="4"> Sekretaris</option>
                                        <option value="6"> Kepala Bagian Umum</option>
                                        <option value="7"> Panitera Muda Pidana</option>
                                        <option value="8"> Panitera Muda Perdata</option>
                                        <option value="9"> Panitera Muda Hukum</option>
                                        <option value="10"> Panitera Muda Tipikor</option>
                                        <option value="11"> Panitera Muda Niaga</option>
                                        <option value="12"> Panitera Muda PHI</option>
                                        <option value="16"> Koordinator Delegasi</option>
                                        <option value="13"> Kasub Bagian Umum dan Keuangan</option>
                                        <option value="14"> Kasub Bagian Kepegawaian, Organisasi dan Tata Laksana</option>
                                        <option value="15"> Kasub Bagian Teknologi Informasi, Perencanaan, dan Pelaporan</option>

                                        <?php elseif($hakakses=="3" || $hakakses=="5"): ?>
                                        <?php if($hakakses=="3"): ?>
                                        <option value="5"> Wakil Panitera</option>
                                        <?php endif;?>
                                        <option value="7"> Panitera Muda Pidana</option>
                                        <option value="8"> Panitera Muda Perdata</option>
                                        <option value="9"> Panitera Muda Hukum</option>
                                        <option value="10"> Panitera Muda Tipikor</option>
                                        <option value="11"> Panitera Muda Niaga</option>
                                        <option value="12"> Panitera Muda PHI</option>
                                        <option value="16"> Koordinator Delegasi</option>
                                        <?php foreach($staff as $stff): ?>
                                        <option value="A<?php echo $stff->id_pegawai; ?>"><?php echo $stff->nama;?> (<?php echo $stff->jabatan;?>)</option>
                                        <?php endforeach; ?>

                                        <?php elseif($hakakses=="4" || $hakakses=="6"): ?>
                                        <?php if($hakakses=="4"): ?>
                                        <option value="6"> Kepala Bagian Umum</option>
                                        <?php endif;?>
                                        <option value="13"> Kasub Bagian Umum dan Keuangan</option>
                                        <option value="14"> Kasub Bagian Kepegawaian, Organisasi dan Tata Laksana</option>
                                        <option value="15"> Kasub Bagian Teknologi Informasi, Perencanaan, dan Pelaporan</option>
                                        <?php foreach($staff as $stff): ?>
                                        <option value="A<?php echo $stff->id_pegawai; ?>"><?php echo $stff->nama;?> (<?php echo $stff->jabatan;?>)</option>
                                        <?php endforeach; ?>

                                        <?php elseif(($hakakses>="7")&&($hakakses<="16")): ?>
                                        <?php foreach($staff as $stff): ?>
                                        <option value="<?php echo $stff->id_pegawai; ?>"><?php echo $stff->nama;?></option>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                </select>
                            </div>
                          </div>
                        <div class="actions m-t" align="right">
                          <button type="button" class="btn btn-info btn-sm btn-prev" data-target="#form-wizard3" data-wizard2="previous" disabled="disabled">Prev</button>
                          <button type="button" class="btn btn-info btn-sm btn-next" data-target="#form-wizard3" data-wizard2="next">Next</button>
                          <button type="submit" name="submit" value="submit" class="btn btn-success btn-sm btn-finish" data-target="#form-wizard3" data-wizard2="finish" >Finish</button>
                        </div>
                        </form>
                      </div>
                  </section>
                </div>
              </div>

            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>
    <script>
    $(document).ready(function() {

        $('input[name="radio"]').on('change', function() {
            var value = $(this).val();
//                alert(value);
            if(value=="ya"){
                $('#ref').show();
                $('#header').show();
                $('select[name="referensi"]').attr('data-required', true);
            }else{
                $('#ref').hide();
                $('#header').hide();
                $('select[name="referensi"]').removeAttr('data-required');
            }
        });
        var b ="";
        var d ="";
        var e ="";
        var f ="";
        var c = $( "input[name=agenda]" ).val();

        $( "input[name=surat1]" ).change(function(){
            b = $( "input[name=surat1]" ).val();
            $('#no_surat').text(b+"/"+c+"/"+d+"/"+e+"/"+f);
        });
        $( "input[name=surat2]" ).change(function(){
            d = $( "input[name=surat2]" ).val();
            $('#no_surat').text(b+"/"+c+"/"+d+"/"+e+"/"+f);
        });
        $( "input[name=surat3]" ).change(function(){
            e = $( "input[name=surat3]" ).val();
            $('#no_surat').text(b+"/"+c+"/"+d+"/"+e+"/"+f);
        });
        $( "input[name=surat4]" ).change(function(){
            f = $( "input[name=surat4]" ).val();
            $('#no_surat').text(b+"/"+c+"/"+d+"/"+e+"/"+f);
        });


        $( "input[name=perihal]" ).change(function(){
            var g = $( "input[name=perihal]" ).val();
            $('#perihal').text(g);
        });
        $( "input[name=tujuan]" ).change(function(){
            var h = $( "input[name=tujuan]" ).val();
            $('#tujuan').text(h);
        });
        $( "input[name=lampiran]" ).change(function(){
            var k = $( "input[name=lampiran]" ).val();
            $('#lampiran').text(k);
        });
        $( "textarea[name=keterangan]" ).change(function(){
            var l = $( "textarea[name=keterangan]" ).val();
            $('#keterangan').text(l);
        });
        $( "input[name=fileupload]" ).change(function(){
            var i = $( "input[name=fileupload]" ).val();
            $('#upload').text(i);
        });

    });
    </script>