<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">          
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="#"><i class="fa fa-home"></i> Surat Keluar</a></li>
                <li class="active">Data Surat</li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Surat Keluar</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <div class="row">
                <div class="col-md-12">
                    
                    <section class="panel panel-default">
                    <header class="panel-heading font-bold">DATA SURAT KELUAR</header>
                    <div class="panel-body">
                        <?php if($message=="success"){ ?>
                        <!-- Error Message -->
                        <div class="alert fade in alert-success fail" id="fail">
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Data upload berhasil ditambahkan.
                        </div>   
                        <?php }else if($message=="errorupload"){ ?>
                        <div class="alert fade in alert-danger invalid" id="invalid">
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Data upload yang anda masukkan gagal diupload.
                        </div> 
                        <?php }else if($message=="hapus"){ ?>
                        <div class="alert fade in alert-success reg" id="reg">
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Data surat berhasil dihapus.
                        </div>
                        <?php }else if($message=="error"){ ?>
                        <div class="alert fade in alert-danger" >
                                <i class="icon-remove close" data-dismiss="alert"></i>
                                Terjadi kesalahan pada inputan anda
                        </div>
                        <?php } ?>
                        <div class="adv-table">
                            <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No. Surat</th>
                                    <th class="hidden-phone">Tanggal Surat</th>
                                    <th class="hidden-phone">Perihal</th>
                                    <th class="hidden-phone">Tujuan Surat</th>
                                    <th class="hidden-phone">Nama Pengirim</th>
                                    <th class="hidden-phone">Berkas</th>
                                    <th class="hidden-phone">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($content!=false){
                                $i=1;
                                foreach($content as $isi){
                                ?>    
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $isi->no_srt_keluar; ?></td>
                                    <td class="hidden-phone"><?php echo tgl_indo2(date("Y-m-d", strtotime($isi->tgl_srt_keluar))); ?></td>
                                    <td class="hidden-phone"><?php echo $isi->perihal; ?></td>
                                    <td class="hidden-phone"><?php echo $isi->tujuan; ?></td>
                                    <td class="hidden-phone"><?php echo $isi->nama?></td>
                                    <td class="hidden-phone"><?php if($isi->filename==""){ echo "<span class=\"label bg-danger\">Belum Diupload</span>"; }else{ echo "<a href=\"".base_url().$isi->filename."\" target=\"_blank\" ><span class=\"label bg-success\">Lihat Berkas</span></a>"; } ?></td>
                                    <td class="hidden-phone">
                                        <?php if ($hakakses=="99"){ ?>
                                        <a href="#myModals<?php echo $i; ?>" data-toggle="modal" title="Hapus Surat" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-times"></i></a> 
                                        <?php }else{ ?>
                                        <a href="<?php echo base_url('keluar/reupload/'.$isi->id_srt_keluar) ?>" title="Upload Berkas" class="btn btn-sm btn-icon btn-warning"><i class="fa fa-upload"></i></a> 
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php   
                                $i++;
                                } 
                                }
                                ?>
                                </tbody>
                            </table>
                            <?php
                                if($content!=false){
                                $no=1;
                                foreach($content as $isi){
                                ?>
                                <form action="<?php echo base_url('keluar/delkeluar') ?>" method="post">
                                <!-- Modal -->
                                <div class="modal fade" id="myModals<?php echo $no; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Perhatian</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Apakah Anda yakin menghapus Surat Keluar No. <strong><?php echo $isi->no_srt_keluar; ?></strong>?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="id_srt_keluar" value="<?php echo $isi->id_srt_keluar; ?>">
                                                <button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true" >Batal</button>
                                                <button class="btn btn-danger" type="submit" name="submit" value="submit"><i class="fa fa-times text"></i> Hapus Surat</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                    
                                <!-- modal -->
                                </form>
                                <?php
                                $no++;
                                }
                                }
                                ?>
                        </div>
                    </div>
                  </section>
                </div>
              </div>
              
              
            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

    <script type="text/javascript">
      

      $(document).ready(function() { 
          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });

         
      } );
  </script>