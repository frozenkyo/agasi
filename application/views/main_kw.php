<section>
      <section class="hbox stretch">
        <?php $this->load->view('element/sidebar') ?>
        <section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li class="active"><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
              </ul>
              <div class="m-b-md">
                <h3 class="m-b-none">Dashboard</h3>
                <small>Welcome back, <?php echo ucwords($username); ?></small>
              </div>
              <section class="panel panel-default">
                <div class="row m-l-none m-r-none bg-light lter">
                  <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                    <span class="fa-stack fa-2x pull-left m-r-sm">
                      <i class="fa fa-circle fa-stack-2x text-info"></i>
                      <i class="fa fa-cloud-download fa-stack-1x text-white"></i>
                    </span>
                    <span class="h3 block m-t-xs"><strong><?php echo $masuk;?></strong></span>
                    <small class="text-muted text-uc">Surat Masuk</small>
                  </div>
                  <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                    <span class="fa-stack fa-2x pull-left m-r-sm">
                      <i class="fa fa-circle fa-stack-2x text-warning"></i>
                      <i class="fa fa-cloud-upload fa-stack-1x text-white"></i>
                    </span>
                    <span class="h3 block m-t-xs"><strong><?php echo $keluar;?></strong></span>
                    <small class="text-muted text-uc">Surat Keluar</small>
                  </div>
                  <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                    <span class="fa-stack fa-2x pull-left m-r-sm">
                      <i class="fa fa-circle fa-stack-2x text-danger"></i>
                      <i class="fa fa-check fa-stack-1x text-white"></i>
                    </span>
                    <span class="h3 block m-t-xs"><strong><?php echo $selesai;?></strong></span>
                    <small class="text-muted text-uc">Terselesaikan</small>
                  </div>
                  <div class="col-sm-6 col-md-3 padder-v b-r b-light">
                    <span class="fa-stack fa-2x pull-left m-r-sm">
                      <i class="fa fa-circle fa-stack-2x text-muted"></i>
                      <i class="fa fa-bar-chart-o fa-stack-1x text-white"></i>
                    </span>
                    <span class="h3 block m-t-xs"><strong><?php echo $surat;?></strong></span>
                    <small class="text-muted text-uc">Total Surat</small>
                  </div>
                </div>
              </section>
              <div class="row">
                <div class="col-md-12">
                  <section class="panel panel-default">
                      <header class="panel-heading font-bold">NOTIFIKASI</header>
                        <div class="panel-body">
                            <div class="alert alert-info">
                                <i class="fa fa-info-sign"></i>Disposisi yang tampil pada notifikasi ini adalah disposisi yang statusnya sudah diarahkan ke <strong><?php echo $jabatan; ?></strong>.
                            </div>
                            <div class="adv-table">
                                <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered" id="hidden-table-info">
                                    <thead>
                                    <tr>
                                    <th>No</th>
                                    <th>Tujuan Surat</th>
                                    <th class="hidden-phone">Nama Pengirim</th>
                                    <th class="hidden-phone">Tanggal Penerimaan</th>
                                    <th class="hidden-phone">Berkas</th>
                                    <th class="hidden-phone">Aksi</th>
                                    <th class="hidden">Perihal</th>
                                    <th class="hidden">Ringkasan</th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                    <th class="hidden"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $no=0;
                                if($disposisi!=false){
                                foreach($disposisi as $isi){
                                ?>
                                <tr>
                                    <td><?php echo $no+1; ?></td>
                                    <td><?php echo $isi->tujuan; ?></td>
                                    <td><?php echo $isi->nama_pengirim; ?></td>
                                    <td class="hidden-phone"><?php echo tgl_indo2(date("Y-m-d", strtotime($isi->tgl_penerimaan))); ?></td>
                                    <td class="hidden-phone center"><?php if($isi->filename==""){ echo "<span class=\"label bg-danger\">Belum Diupload</span>"; }else{ echo "<a href=\"".base_url().$isi->filename."\" target=\"_blank\" ><span class=\"label bg-success\">Lihat Berkas</span></a>"; } ?></td>
                                    <td class="hidden-phone">
                                        <?php if($isi->status=="99") { ?>
                                        <a href="<?php echo base_url('cetak/'.$isi->id_disposisi) ?>" target="_blank" title="Cetak" class="btn btn-sm btn-icon btn-info"><i class="fa fa-print"></i></a>
                                        <?php } ?>
                                        <a href="#myModal<?php echo $no+1; ?>" data-toggle="modal" title="Kerjakan" class="btn btn-sm btn-icon btn-success"><i class="fa fa-check"></i></a>
                                        <!--<a class="btn btn-success" data-toggle="modal" href="#myModal">sda</a>-->
                                    </td>
                                    <td class="hidden"><?php echo $isi->hal_surat; ?></td>
                                    <td class="hidden"><?php echo $isi->isi_ringkas; ?></td>
                                    <td class="hidden"><?php $jabatan=$this->dashboardsess->idJabatanToNama($isi->tujuan_utama); echo $jabatan[0]->jabatan; ?></td>
                                    <td class="hidden"><?php if($isi->tujuan_pansek==0){ /*echo "Panitera";*/ }else{ $jabatan2=$this->dashboardsess->idJabatanToNama($isi->tujuan_pansek); echo $jabatan2[0]->jabatan; } ?></td>
                                    <td class="hidden"><?php if($isi->tujuan_wapansek==0){ /*echo "Wakil Panitera Sekretaris";*/ }else{ $jabatan3=$this->dashboardsess->idJabatanToNama($isi->tujuan_wapansek); echo $jabatan3[0]->jabatan; } ?></td>
                                    <td class="hidden"><?php if($isi->tujuan_panmud==0){ /*echo "Panmud/Kabag";*/ }else{ $jabatan4=$this->dashboardsess->idJabatanToNama($isi->tujuan_panmud); echo $jabatan4[0]->jabatan; } ?></td>
                                    <td class="hidden"><?php if($isi->tanggapan_utama==""){ echo "Belum ada tanggapan"; }else{ echo $isi->tanggapan_utama; } ?></td>
                                    <td class="hidden"><?php if($isi->tanggapan_pansek==""){ echo "Belum ada tanggapan"; }else{ echo $isi->tanggapan_pansek; } ?></td>
                                    <td class="hidden"><?php if($isi->tanggapan_wapansek==""){ echo "Belum ada tanggapan"; }else{ echo $isi->tanggapan_wapansek; } ?></td>
                                    <td class="hidden"><?php if($isi->tanggapan_panmud==""){ echo "Belum ada tanggapan"; }else{ echo $isi->tanggapan_panmud; } ?></td>
                                    <td class="hidden"><?php if($isi->tgl_utama=="0000-00-00 00:00:00" || $isi->tgl_utama==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($isi->tgl_utama)).")"; } ?></td>
                                    <td class="hidden"><?php if($isi->tgl_pansek=="0000-00-00 00:00:00" || $isi->tgl_pansek==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($isi->tgl_pansek)).")"; } ?></td>
                                    <td class="hidden"><?php if($isi->tgl_wapansek=="0000-00-00 00:00:00" || $isi->tgl_wapansek==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($isi->tgl_wapansek)).")"; } ?></td>
                                    <td class="hidden"><?php if($isi->tgl_panmud=="0000-00-00 00:00:00" || $isi->tgl_panmud==""){ }else{ echo "(Update ".date("d/m/y g:i A", strtotime($isi->tgl_panmud)).")"; } ?></td>
                                </tr>
                                <?php
                                $no++;
                                }
                                }
                                ?>
                                    </tbody>
                                </table>
                                <?php
                                if($disposisi!=false){
                                $no=0;
                                foreach($disposisi as $disp){
                                ?>
                                <form action="<?php echo base_url('dashboard') ?>" method="post">
                                <!-- Modal -->
                                <div class="modal fade" id="myModal<?php echo $no+1; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Perhatian</h4>
                                            </div>
                                            <div class="modal-body">

                                                Pilih tombol dibawah untuk melanjutkan

                                            </div>
                                            <div class="modal-footer">
                                                <a href="<?php echo base_url('dashboard/direct/'.$disp->id_disposisi) ?>" class="btn btn-success" role="button"><i class="fa fa-mail-forward text"></i> Beri Tanggapan</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- modal -->
                                </form>
                                <?php
                                $no++;
                                }
                                }
                                ?>
                            </div>
                        </div>
                  </section>
                </div>
              </div>


            </section>
          </section>
          <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
        </section>
        <aside class="bg-light lter b-l aside-md hide" id="notes">
          <div class="wrapper">Notification</div>
        </aside>
      </section>
    </section>

    <script type="text/javascript">
      /* Formating function for row details */
      function fnFormatDetails ( oTable, nTr )
      {
          var aData = oTable.fnGetData( nTr );
          var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
          sOut += '<tr><td>Perihal:</td><td>'+aData[7]+'</td></tr>';
          sOut += '<tr><td>Ringkasan Surat:</td><td>'+aData[8]+'</td></tr>';
          if (aData[9] != "") {
              sOut += '<tr><td>Tanggapan '+aData[9]+':</td><td>'+aData[13]+'. <i>'+aData[17]+'</i></td></tr>';
          }
          if (aData[10] != "") {
              sOut += '<tr><td>Tanggapan '+aData[10]+':</td><td>'+aData[14]+'. <i>'+aData[18]+'</i></td></tr>';
          }
          if (aData[11] != "") {
              sOut += '<tr><td>Tanggapan '+aData[11]+':</td><td>'+aData[15]+'. <i>'+aData[19]+'</i></td></tr>';
          }
          if (aData[12] != "") {
              sOut += '<tr><td>Tanggapan '+aData[12]+':</td><td>'+aData[16]+'. <i>'+aData[20]+'</i></td></tr>';
          }
          sOut += '</table>';
          return sOut;
      }

      $(document).ready(function() {
          /*
           * Insert a 'details' column to the table
           */
          var nCloneTh = document.createElement( 'th' );
          var nCloneTd = document.createElement( 'td' );
          nCloneTd.innerHTML = '<img src="<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_open.png">';
          nCloneTd.className = "center";

          $('#hidden-table-info thead tr').each( function () {
              this.insertBefore( nCloneTh, this.childNodes[0] );
          } );

          $('#hidden-table-info tbody tr').each( function () {
              this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
          } );

          /*
           * Initialse DataTables, with no sorting on the 'details' column
           */
          var oTable = $('#hidden-table-info').dataTable( {
              "aoColumnDefs": [
                  { "bSortable": false, "aTargets": [ 0 ] }
              ],
              "aaSorting": []
          });

          /* Add event listener for opening and closing details
           * Note that the indicator for showing which row is open is not controlled by DataTables,
           * rather it is done here
           */
          $('#hidden-table-info tbody td img').live('click', function () {
              var nTr = $(this).parents('tr')[0];
              if ( oTable.fnIsOpen(nTr) )
              {
                  /* This row is already open - close it */
                  this.src = "<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_open.png";
                  oTable.fnClose( nTr );
              }
              else
              {
                  /* Open this row */
                  this.src = "<?php echo base_url(); ?>assets/advanced-datatable/examples/examples_support/details_close.png";
                  oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
              }
          } );
      } );
  </script>