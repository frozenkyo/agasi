<!DOCTYPE html>
<html lang="en">
<head>
 <title>Cetak Disposisi</title>
</head>
<style>
    table{
        border: 1;
    }
    td{
        border: none;
    }
    @page {
    margin-top: 1cm;
    margin-bottom: 1cm;
    margin-left: 1cm;
    margin-right: 1cm;
    }
</style>
<body>
    <?php foreach ($contents as $content): ?>
    <table style="width:100%" align="center" cellspacing="10">
        <tr>
            <td width="22%" align="right"><img src="<?php echo base_url().'images/new_pns2.png'; ?>"/></td>
            <td align="center" style="margin-right: 5px;margin-left: -5px;">
                <p style="font-size:21px"><strong>PENGADILAN NEGERI / NIAGA / HAM</strong></p>
                <p style="font-size:21px;"><strong>HUBUNGAN INDUSTRIAL / TIPIKOR SURABAYA</strong></p>
                <p style="font-size:8px;color: #fff">&nbsp;j</p>
                <p style="font-size:15px">Jalan Raya Arjuno No. 16 - 18 Surabaya</p>
                <p style="font-size:15px">Telepon 031-5311523 Faximile 031-5343907</p>
                <p style="font-size:15px">Website : www.pn-surabayakota.go.id</p>
                <p style="font-size:15px">Email : mail@pn-surabayakota.go.id</p>
            </td>
            <td width="2%">&nbsp;</td>
        </tr>
    </table>
    <table style="width:100%;border-bottom: none; border-top: none;" align="center">
        <tr>
            <td style="padding-top: 10px;padding-bottom: 10px;" colspan="3" align="center"><p style="font-size:18px"><strong>LEMBAR DISPOSISI</strong></p></td>
        </tr>
    </table>
    <table style="width:100%;padding: 10px;border-bottom: none;" align="left">
        <tr>
            <td valign="center" width="15%"><p style="font-size:13px">No. Agenda</p></td>
            <td valign="center" width="35%"><p style="font-size:13px">: <?php echo $content->no_agenda ?></p></td>
            <td valign="center" width="20%"><p style="font-size:13px">Tk. Keamanan</p></td>
            <td valign="center" width="30%"><p style="font-size:13px">: <?php echo $content->keamanan ?></p></td>
        </tr>
        <tr>
            <td valign="center" ><p style="font-size:13px">Tgl. Terima</p></td>
            <td valign="center" ><p style="font-size:13px">: <?php $time = strtotime($content->tgl_penerimaan);echo tgl_indo2(date("Y-m-d", $time)); ?></p></td>
            <td valign="center" ><p style="font-size:13px">Tgl. Penyelesaian</p></td>
            <td valign="center" ><p style="font-size:13px">: </p></td>
        </tr>
    </table>
    <table style="width:100%;" cellpadding="10" align="left">
        <tr >
            <td valign="top" width="47%" height="80px;"><p style="font-size:13px">Tanggal dan Nomor Surat</p></td>
            <td valign="top" width="5%" style="padding-right: 5px;" ><p style="font-size:13px">:</p></td>
            <td valign="top" width="48%" style="margin-top: -10px;" >
                <p style="font-size:13px"><?php echo tgl_indo2($content->tgl_srt_masuk) ?></p><br/>
                <p style="font-size:13px"><?php echo $content->no_srt_masuk ?></p>
            </td>
        </tr>
        <tr>
            <td valign="top" width="47%" height="30px;" ><p style="font-size:13px">Dari</p></td>
            <td valign="top" width="5%" ><p style="font-size:13px">:</p></td>
            <td valign="top" width="48%" ><p style="font-size:13px"><?php echo $content->nama_pengirim ?></p></td>
        </tr>
        <tr>
            <td valign="top" width="47%" height="120px;" ><p style="font-size:13px">Ringkasan Isi</p></td>
            <td valign="top" width="5%" ><p style="font-size:13px">:</p></td>
            <td valign="top" width="48%" ><p style="font-size:13px"><?php echo $content->isi_ringkas ?></p></td>
        </tr>
        <tr>
            <td valign="top" width="47%" height="20px;" ><p style="font-size:13px">Lampiran</p></td>
            <td valign="top" width="5%" ><p style="font-size:13px">:</p></td>
            <td valign="top" width="48%" ><p style="font-size:13px"><?php echo ($content->lampiran == "") ? "-" : $content->lampiran; ?></p></td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left" cellpadding="15">
        <tr>
            <td align="center" width="49%"><p style="font-size:13px"><strong>DISPOSISI</strong></p></td>
            <td style="border-left: solid 1px black;" align="center" width="51%"><p style="font-size:13px"><strong>DITUJUKAN KEPADA</strong></p></td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left" cellpadding="10">
        <tr>
            <td valign="top" align="left" width="49%" height="80px;">
                <p style="font-size:13px">
                <strong>
                    <?php
                        $tes=$this->cetaksess->idJabatanToNama($content->tujuan_utama);
                        $time = strtotime($content->tgl_utama);
                        echo strtoupper($tes[0]->jabatan)." (".tgl_indo2(date("Y-m-d", $time)).") :";
                    ?>
                </strong>
                </p>
                <p style="font-size:13px"><?php echo $content->tanggapan_utama ?></p>
            </td>
            <td style="border-left: solid 1px black;" valign="top" align="left" width="51%">
                <p style="font-size:13px">
                    <strong>
                    <?php
                        if($content->tujuan_pansek!=0){
                            $tes=$this->cetaksess->idJabatanToNama($content->tujuan_pansek);
                            echo strtoupper($tes[0]->jabatan);
                        }
                    ?>
                    </strong>
                </p>
            </td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left" cellpadding="10">
        <tr>
            <?php $tes=$this->cetaksess->idJabatanToNama($content->tujuan_pansek); $time = strtotime($content->tgl_pansek); ?>
            <td valign="top" align="left" width="49%" height="80px;">
                <?php if ($content->tujuan_pansek != 0 && strtoupper($tes[0]->jabatan) != "SEKRETARIS"): ?>
                <p style="font-size:13px">
                    <strong>
                        <?php
                            echo strtoupper($tes[0]->jabatan)." (".tgl_indo2(date("Y-m-d", $time)).") :";
                        ?>
                    </strong>
                </p>
                <p style="font-size:13px"><?php echo $content->tanggapan_pansek; ?></p>
                <?php endif; ?>
            </td>
            <td style="border-left: solid 1px black;" valign="top" align="left" width="51%">
                <?php if ($content->tujuan_pansek != 0 && strtoupper($tes[0]->jabatan) == "SEKRETARIS"): ?>
                <p style="font-size:13px">
                    <strong>
                        <?php
                            echo strtoupper($tes[0]->jabatan)." (".tgl_indo2(date("Y-m-d", $time)).") :";
                        ?>
                    </strong>
                </p>
                <p style="font-size:13px"><?php echo $content->tanggapan_pansek; ?></p>
                <?php endif; ?>
            </td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left" cellpadding="15">
        <tr>
            <td align="center" width="49%"><p style="font-size:13px">DITERUSKAN KEPADA</p></td>
            <td style="border-left: solid 1px black;" align="center" width="51%"><p style="font-size:13px">DITERUSKAN KEPADA</p></td>
        </tr>
    </table>
    <table style="width:100%;border-top: none;" align="left" cellpadding="10">
        <tr>
            <!-- KOLOM PANITERA -->
            <?php $cetakStaffPanitera = false; $cetakSekretaris = true; ?>
            <td valign="top" align="left" width="49%" height="150px;">
                <?php if ($content->tujuan_wapansek != 0 && ($content->tujuan_wapansek == 3 || $content->tujuan_wapansek == 5)): ?>
                <p style="font-size:13px">
                    <strong>
                        <?php
                            $cetakStaffPanitera = true;
                            $cetakSekretaris = false;
                            $tes=$this->cetaksess->idJabatanToNama($content->tujuan_wapansek);
                            $time = strtotime($content->tgl_wapansek);
                            echo strtoupper($tes[0]->jabatan)." (".tgl_indo2(date("Y-m-d", $time)).") :";
                        ?>
                    </strong>
                </p>
                <p style="font-size:13px"><?php echo $content->tanggapan_pansek; ?></p>
                <?php endif; ?>

                <?php if ($content->tujuan_panmud != 0 && ($content->tujuan_panmud >= 7 && $content->tujuan_panmud <= 12) || $content->tujuan_panmud == 16): ?>
                <p style="font-size:13px">
                    <strong>
                        <?php
                            $tes=$this->cetaksess->idJabatanToNama($content->tujuan_panmud);
                            $time = strtotime($content->tgl_panmud);
                            echo strtoupper($tes[0]->jabatan)." (".tgl_indo2(date("Y-m-d", $time)).") :";
                            $cetakStaffPanitera = true;
                            $cetakSekretaris = false;
                        ?>
                    </strong>
                </p>
                <p style="font-size:13px"><?php echo $content->tanggapan_panmud; ?></p>
                <?php endif; ?>

                <?php if($cetakStaffPanitera): ?>
                <p style="font-size:13px">
                    <strong>
                        <?php
                            $time = strtotime($content->tgl_staff);
                            $cetakSekretaris = false;
                            echo "PELAKSANA : ".strtoupper($content->nama)." (".tgl_indo2(date("Y-m-d", $time)).")";
                        ?>
                    </strong>
                </p>
                <p style="font-size:13px"><?php echo $content->note; ?></p>
                <?php endif; ?>
            </td>
            <!-- KOLOM SEKRETARIS -->
            <td style="border-left: solid 1px black;" valign="top" align="left" width="51%">
                <?php if($cetakSekretaris): ?>
                <?php if ($content->tujuan_wapansek != 0): ?>
                <p style="font-size:13px">
                    <strong>
                        <?php
                            if ($content->tujuan_wapansek == 4 || $content->tujuan_wapansek == 6) {
                                $tes=$this->cetaksess->idJabatanToNama($content->tujuan_wapansek);
                                $time = strtotime($content->tgl_wapansek);
                                echo strtoupper($tes[0]->jabatan)." (".tgl_indo2(date("Y-m-d", $time)).") :";
                            }
                        ?>
                    </strong>
                </p>
                <p style="font-size:13px"><?php echo $content->tanggapan_pansek; ?></p>
                <?php endif; ?>
                <?php if ($content->tujuan_panmud != 0 && ($content->tujuan_panmud >= 13 && $content->tujuan_panmud <= 15)): ?>
                <p style="font-size:13px">
                    <strong>
                        <?php
                            $tes=$this->cetaksess->idJabatanToNama($content->tujuan_panmud);
                            $time = strtotime($content->tgl_panmud);
                            echo strtoupper($tes[0]->jabatan)." (".tgl_indo2(date("Y-m-d", $time)).") :";
                        ?>
                    </strong>
                </p>
                <p style="font-size:13px"><?php echo $content->tanggapan_panmud; ?></p>
                <?php endif; ?>

                <p style="font-size:13px">
                    <strong>
                        <?php
                            $time = strtotime($content->tgl_staff);
                            echo "PELAKSANA : ".strtoupper($content->nama)." (".tgl_indo2(date("Y-m-d", $time)).")";
                        ?>
                    </strong>
                </p>
                <p style="font-size:13px"><?php echo $content->note; ?></p>
                <?php endif;?>
            </td>
        </tr>
    </table>
    <table style="width:100%;border: none;" align="left" cellpadding="10">
        <tr>
            <td valign="top" align="left" width="100%" >
                <p style="font-size:13px">*&#41; Lembar disposisi ini sah tanpa paraf</p>
            </td>
        </tr>
    </table>
    <?php endforeach; ?>
</body>
</html>